from rpalpal_core import wsgi
import socket
from cheroot.wsgi import Server as WSGIServer

sn = socket.gethostname()

WSGIServer(
    ('0.0.0.0', 8060), wsgi.application, server_name=sn,
).start()
