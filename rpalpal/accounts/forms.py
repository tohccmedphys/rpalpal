
from django.contrib.auth import authenticate
from django.contrib.auth.forms import PasswordChangeForm, AuthenticationForm
from django.forms import ValidationError

from accounts.backends import auth_ldap


def check_password(user, old_password):
    user = authenticate(username=user.username, password=old_password)
    if user is None:
        return False
    return True


class CustomPasswordChangeForm(PasswordChangeForm):

    def clean_old_password(self):
        """
        Validates that the old_password field is correct.
        """
        old_password = self.cleaned_data["old_password"]
        if not check_password(self.user, old_password):
            raise ValidationError(self.error_messages['password_incorrect'], code='password_incorrect',)
        return old_password


class CustomAuthenticationForm(AuthenticationForm):

    def __init__(self, *args, **kwargs):
        super(CustomAuthenticationForm, self).__init__(*args, **kwargs)
        self.fields['username'].label = 'TOH Username'
        self.fields['password'].label = 'TOH Password'
        print('Yeah, here')

    def clean(self):
        # cleaned = super(CustomAuthenticationForm, self).clean()
        cleaned = self.cleaned_data
        if not auth_ldap(cleaned['username'], cleaned['password'], create=False):
            raise ValidationError('Not an active directory account username/password')

        return cleaned
