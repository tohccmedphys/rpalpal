
from django.conf import settings
from django.contrib.auth.backends import BaseBackend
from django.contrib.auth.models import User

from django.contrib.auth import login as auth_login
from django.contrib.auth import views as auth_views
from django.http import JsonResponse

if settings.USE_LDAP:
    import ldap3
    from ldap3.core.exceptions import LDAPBindError
    from ldap3 import Connection, Server


def auth_ldap(username=None, password=None, create=True):
    if not settings.USE_LDAP or not password or len(password) == 0:
        return None
    try:
        domain_username = '{}\\{}'.format(settings.LDAP_NT4_DOMAIN, username)

        if settings.LDAP_USE_SSL:
            server = Server(settings.LDAP_AUTH_URL, use_ssl=True)
            conn = Connection(server, user=domain_username, password=password, auto_bind=ldap3.AUTO_BIND_NO_TLS)
        else:
            conn = Connection(settings.LDAP_AUTH_URL, user=domain_username, password=password, auto_bind=ldap3.AUTO_BIND_NO_TLS)

        if conn.result.get("description") == 'success':
            if create:
                return get_or_create_ldap_user(username, conn)
            else:
                return User.objects.filter(username=username).first()
        return None

    except LDAPBindError:
        print("Unable to bind {}".format(username))
        return None


def get_or_create_ldap_user(username, conn):

    try:
        user = User.objects.get(username=username)
    except User.DoesNotExist:

        conn.search(
            settings.LDAP_AUTH_SEARCH_BASE,
            '({}={})'.format(settings.LDAP_USER_ID_FIELD, username),
            attributes=settings.LDAP_SEARCH_FIELDS
        )

        result = conn.entries[0]
        user = User(username=username, first_name=result.givenName, last_name=result.sn, email=result.mail)

        user.is_staff = False
        user.is_superuser = False
        user.set_unusable_password()
        user.save()

    return user


class ActiveDirectoryGroupMembershipSSLBackend(BaseBackend):

    def authenticate(self, request, username=None, password=None):
        if settings.USE_LDAP:
            return auth_ldap(username=username, password=password)
        return None

    def get_user(self, user_id):
        try:
            return User.objects.get(id=user_id)
        except User.DoesNotExist:
            return None


class LoginView(auth_views.LoginView):

    def form_valid(self, form):
        auth_login(self.request, form.get_user())
        return JsonResponse({'success': True, 'redirect_to': self.get_success_url()})

    def form_invalid(self, form):
        return JsonResponse({'success': False, 'errors': form.errors.get_json_data()})
