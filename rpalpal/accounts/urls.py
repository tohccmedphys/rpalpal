
from django.conf import settings
from django.urls import path

from . import views
from django.contrib.auth import views as auth_views

urlpatterns = [
    path(r'logout/', auth_views.LogoutView.as_view(), {'next_page': settings.LOGIN_URL}, name='logout'),
    path(r'login/', auth_views.LoginView.as_view(), name='login'),
]
