
from django.contrib.auth import authenticate, update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.http import HttpResponse, HttpResponseRedirect
from django.template.response import TemplateResponse
from django.urls import reverse
from django.views.decorators.csrf import csrf_protect

from . import forms as a_forms
from rpal.models import PERMISSIONS as rp_perms
from rpalpal_core.models import PERMISSIONS as rppc_perms


def authorize(request):

    username = request.POST.get("username", "")
    pwd = request.POST.get("password", "")

    user = authenticate(username=username, password=pwd)

    if user is None:
        return HttpResponse('Invalid Username/Password', status=401)

    elif not user.has_perm("patientplans.can_authorize_override"):
        return HttpResponse('User does not have required approval permission', status=403)

    return HttpResponse("OK", status=200)


def account_details(request):
    template_name = 'registration/account.html'
    print(rp_perms)
    context = {}
    all_perms = request.user.get_all_permissions()
    permissions = []
    for perm, title, desc in rp_perms:
        permissions.append((perm in all_perms, title, desc))
    for perm, title, desc in rppc_perms:
        permissions.append((perm in all_perms, title, desc))

    context["permissions"] = permissions

    return TemplateResponse(request, template_name, context=context)


@csrf_protect
@login_required
def password_reset_confirm(request):

    template_name = 'registration/password_reset_form.html'
    print('In view')
    if request.method == 'POST':
        form = a_forms.CustomAuthenticationForm(request=request, data=request.POST)
        if form.is_valid():
            request.user.set_password('ldap authenticated')
            request.user.save()
            update_session_auth_hash(request, request.user)
            messages.add_message(request, messages.SUCCESS, 'Password reset.')
            return HttpResponseRedirect(reverse('account_details'))
    else:
        form = a_forms.CustomAuthenticationForm(request=request)

    return TemplateResponse(request, template_name, context={'form': form})


@csrf_protect
@login_required
def password_change(request):
    template_name = 'registration/password_change_form.html'

    if request.method == 'POST':
        form = a_forms.CustomPasswordChangeForm(user=request.user, data=request.POST)
        if form.is_valid():
            form.save()
            update_session_auth_hash(request, form.user)
            messages.add_message(request, messages.SUCCESS, 'Password changed.')
            return HttpResponseRedirect(reverse('account_details'))
    else:
        form = a_forms.CustomPasswordChangeForm(user=request.user)

    return TemplateResponse(request, template_name, context={'form': form})
