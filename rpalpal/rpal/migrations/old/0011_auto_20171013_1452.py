# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-10-13 18:52
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('rpal', '0010_auto_20171013_1429'),
    ]

    operations = [
        migrations.CreateModel(
            name='Frailty',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('number', models.IntegerField(unique=True)),
            ],
            options={
                'verbose_name_plural': 'Frailty',
            },
        ),
        migrations.AddField(
            model_name='visit',
            name='frailty',
            field=models.CharField(blank=True, max_length=64, null=True),
        ),
        migrations.AlterField(
            model_name='ministryoftransportationstatus',
            name='name',
            field=models.CharField(max_length=64, unique=True),
        ),
    ]
