# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-09-27 16:39
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('rpal', '0007_auto_20170817_1430'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='diagnosis',
            name='datetime_created',
        ),
        migrations.AlterField(
            model_name='followup',
            name='treatment_course',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='rpal.TreatmentCourse'),
        ),
    ]
