# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-10-13 15:11
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('rpal', '0008_auto_20170927_1239'),
    ]

    operations = [
        migrations.AlterField(
            model_name='patientcareplan',
            name='care_plan',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='rpal.CarePlan'),
        ),
        migrations.AlterField(
            model_name='referral',
            name='referral_location',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='rpal.ReferralLocation', verbose_name='Location'),
        ),
        migrations.AlterField(
            model_name='referral',
            name='referral_reach',
            field=models.ForeignKey(blank=True, help_text='How did this referral reach RPAL?', null=True, on_delete=django.db.models.deletion.PROTECT, to='rpal.ReferralReach', verbose_name='Referral Via'),
        ),
        migrations.AlterField(
            model_name='referral',
            name='referral_reasons',
            field=models.ManyToManyField(blank=True, to='rpal.ReferralReason', verbose_name='Reasons'),
        ),
        migrations.AlterField(
            model_name='referral',
            name='referring_communication_mode',
            field=models.CharField(blank=True, choices=[('p', 'Phone'), ('t', 'Text'), ('e', 'Email'), ('f', 'Fax'), ('a', 'Paper')], max_length=1, null=True, verbose_name='Communication Mode'),
        ),
    ]
