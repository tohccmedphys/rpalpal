# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-08-17 18:30
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('rpal', '0006_auto_20170817_1224'),
    ]

    operations = [
        migrations.RenameField(
            model_name='followup',
            old_name='date_phone',
            new_name='date',
        ),
        migrations.RemoveField(
            model_name='followup',
            name='date_physician',
        ),
    ]
