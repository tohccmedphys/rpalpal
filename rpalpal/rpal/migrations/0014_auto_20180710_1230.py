# Generated by Django 2.0.6 on 2018-07-10 16:30

from django.db import migrations, models
import django.db.models.deletion


def split_structures_treated_into_multiple_tx(apps, schema):

    TreatmentCourse = apps.get_model('rpal', 'TreatmentCourse')

    for tc in TreatmentCourse.objects.all():
        count = 0
        for s in tc.structures_treated_override.all():
            if count == 0:
                tc.structure_treated = s
                tc.save()
            else:
                new_tc = TreatmentCourse.objects.create(
                    patient=tc.patient,
                    visit=tc.visit,
                    care_plan=tc.care_plan,
                    care_plan_num_fields=tc.care_plan_num_fields,
                    structure_treated=s,
                    dose_override=tc.dose_override,
                    fractions_override=tc.fractions_override,
                    type_override=tc.type_override,
                    complexity_override=tc.complexity_override,
                    pattern_override=tc.pattern_override,
                    date_start=tc.date_start,
                    date_end=tc.date_end,
                    ct_date=tc.ct_date,
                    bone_mets_involved=tc.bone_mets_involved,
                    is_prescribed_course_complete=tc.is_prescribed_course_complete,
                    same_day_start_date=tc.same_day_start_date,
                    explanation=tc.explanation,
                    transport_mode=tc.transport_mode,
                    location=tc.location,
                    coming_from=tc.coming_from,
                    duration_travel=tc.duration_travel
                )
                new_tc.bone_mets_complexity.add(*list(tc.bone_mets_complexity.all()))
                new_tc.save()
            count += 1


class Migration(migrations.Migration):

    dependencies = [
        ('rpal', '0013_auto_20180709_1444'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='careplan',
            name='structures_treated',
        ),
        migrations.AddField(
            model_name='treatmentcourse',
            name='structure_treated',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.PROTECT, to='rpal.Structure', related_name='treatment_courses'),
            preserve_default=False,
        ),
        migrations.RunPython(split_structures_treated_into_multiple_tx),
        migrations.RemoveField(
            model_name='treatmentcourse',
            name='structures_treated_override',
        ),
        migrations.AlterField(
            model_name='visit',
            name='in_out',
            field=models.CharField(choices=[('in', 'In'), ('out', 'Out')], default='out', max_length=3),
        ),
    ]
