# Generated by Django 2.0.6 on 2018-07-12 17:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('rpal', '0016_auto_20180711_1430-1'),
    ]

    operations = [
        migrations.AddField(
            model_name='diagnosis',
            name='relative_id',
            field=models.PositiveIntegerField(default=0, help_text='Number of this diagnosis relative to other diagnoses for a patient'),
        ),
        migrations.AlterField(
            model_name='referral',
            name='relative_id',
            field=models.PositiveIntegerField(default=0, help_text='Number of this referral relative to other referrals for a patient'),
        ),
        migrations.AlterField(
            model_name='treatmentcourse',
            name='relative_id',
            field=models.PositiveIntegerField(default=0, help_text='Number of this treatment course relative to other treatment courses for a patient'),
        ),
        migrations.AlterField(
            model_name='visit',
            name='relative_id',
            field=models.PositiveIntegerField(default=0, help_text='Number of this visit relative to other visits for a patient'),
        ),
    ]
