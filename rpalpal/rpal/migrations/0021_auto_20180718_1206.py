# Generated by Django 2.0.6 on 2018-07-18 16:06

from django.db import migrations, models, IntegrityError
import django.db.models.deletion


def migrate_through_collection(apps, schema):

    TreatmentCourse = apps.get_model('rpal', 'TreatmentCourse')
    TreatmentCourseBoneMetsComplexityCollection = apps.get_model('rpal', 'TreatmentCourseBoneMetsComplexityCollection')

    for tc in TreatmentCourse.objects.all():
        count = tc.bone_mets_complexity.count()
        for bmc in tc.bone_mets_complexity.all():
            TreatmentCourseBoneMetsComplexityCollection.objects.create(
                treatment_course=tc,
                bone_mets_complexity=bmc
            )

        if count != tc.bone_mets_complexity_2.count():
            raise IntegrityError('Failed migration. Different BMC counts')


class Migration(migrations.Migration):

    dependencies = [
        ('rpal', '0020_auto_20180718_1038'),
    ]

    operations = [
        migrations.CreateModel(
            name='TreatmentCourseBoneMetsComplexityCollection',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('bone_mets_complexity', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='rpal.BoneMetsComplexity')),
                ('treatment_course', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='rpal.TreatmentCourse')),
            ],
        ),
        migrations.AlterUniqueTogether(
            name='treatmentcoursebonemetscomplexitycollection',
            unique_together={('treatment_course', 'bone_mets_complexity')},
        ),
        migrations.AddField(
            model_name='treatmentcourse',
            name='bone_mets_complexity_2',
            field=models.ManyToManyField(blank=True, to='rpal.BoneMetsComplexity', through='rpal.TreatmentCourseBoneMetsComplexityCollection', related_name='bonemetscomplexity_set_2'),
        ),
        migrations.RunPython(migrate_through_collection),
        migrations.RemoveField(
            model_name='treatmentcourse',
            name='bone_mets_complexity'
        ),
        migrations.RenameField(
            model_name='treatmentcourse',
            old_name='bone_mets_complexity_2',
            new_name='bone_mets_complexity'
        ),
        migrations.AlterField(
            model_name='treatmentcourse',
            name='bone_mets_complexity',
            field=models.ManyToManyField(blank=True, to='rpal.BoneMetsComplexity', through='rpal.TreatmentCourseBoneMetsComplexityCollection', related_name='bonemetscomplexity_set'),
        ),
        migrations.AlterField(
            model_name='treatmentcourse',
            name='patient',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='treatmentcourse_set', to='rpal.Patient'),
        )
    ]
