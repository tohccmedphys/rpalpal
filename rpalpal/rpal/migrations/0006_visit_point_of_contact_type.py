# Generated by Django 2.0.6 on 2018-06-01 18:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('rpal', '0005_auto_20180601_1346'),
    ]

    operations = [
        migrations.AddField(
            model_name='visit',
            name='point_of_contact_type',
            field=models.CharField(choices=[('', '-----'), ('followup', 'Followup'), ('consult', 'Consult'), ('review', 'Review')], default='consult', max_length=10),
            preserve_default=False,
        ),
    ]
