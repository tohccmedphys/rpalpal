
from django.contrib.auth.decorators import login_required
from django.urls import path, re_path

from rpal import views, api

urlpatterns = [
    path('patients/new', views.CreatePatient.as_view(), name='p_new'),
    re_path('patients/edit(?:/(?P<pk>\d+))?/$', views.UpdatePatient.as_view(), name='p_edit'),
    path('patients/', views.PatientsList.as_view(), name='p_list'),
    path('patients/details', views.PatientDetails.as_view(), name='p_details'),
    path('patients/_mrn_searcher', login_required(api.patients_mrn_searcher), name='patients_mrn_searcher'),
    path('physicians/_searcher', login_required(api.physician_searcher), name='physician_searcher'),
    path('physicians/_active_physicians_searcher', login_required(api.active_physicians_searcher), name='active_physicians_searcher'),
    path('physicians/_active_rad_oncs_searcher', login_required(api.active_rad_oncs_searcher), name='active_rad_oncs_searcher'),
    path('referrals/_searcher', login_required(api.referral_searcher), name='referrals_searcher'),
    path('patient/_searcher', login_required(api.patients_searcher), name='patients_searcher'),
    path('diagnoses/_searcher', login_required(api.diagnoses_searcher), name='diagnoses_searcher'),
    path('diagnoses/_create_update', login_required(api.diagnosis_create_update_ajax), name='diagnosis_create_update_ajax'),
    path('visits/_searcher', login_required(api.visits_searcher), name='visits_searcher'),
    path('treatment_courses/_searcher', login_required(api.all_treatment_courses_searcher), name='all_treatment_courses_searcher'),
    path('careplan/_careplan_searcher', login_required(api.careplan_searcher), name='careplan_searcher'),
    path('rpal/_object_create_update', login_required(api.object_create_update_ajax), name='object_create_update_ajax'),
    path('rpal/_delete_object_ajax', login_required(api.delete_object_ajax), name='delete_object_ajax'),
    path('metastatic_cancer_sites/_searcher', login_required(api.metastatic_cancer_sites_searcher), name='metastatic_cancer_sites_searcher'),
    path('careplans/new', views.CreateCareplan.as_view(), name='cp_new'),
    re_path('careplans/edit(?:/(?P<pk>\d+))?/$', views.UpdateCareplan.as_view(), name='cp_edit'),
    path('careplans/', views.CareplansList.as_view(), name='cp_list'),
    path('pocs/', views.VisitsList.as_view(), name='pocs'),
    path('treatments/', views.TreatmentCourseList.as_view(), name='treatments'),
    path('stats/', views.Stats.as_view(), name='stats'),
    path('stats/_get_stat_data', login_required(views.get_stat_data), name='get_stat_data'),
    path('stats/poc_csv', login_required(views.export_visit_csv), name='visit_csv'),
    path('stats/tc_csv', login_required(views.export_treatment_course_csv), name='treatment_course_csv'),
    path('stats/r_csv', login_required(views.export_referral_csv), name='referral_csv'),
    path('stats/d_csv', login_required(views.export_diagnoses_csv), name='diagnosis_csv'),
    path('stats/p_csv', login_required(views.export_patients_csv), name='patients_csv'),
]
