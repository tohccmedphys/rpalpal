
from collections import OrderedDict
from django.conf import settings
from django.contrib import admin

from rpal import models as rp_models


ADMIN_FIELDS = {
    'Patients': [
        # 'Patient',
        'Facility',
        'Allergy',
        'Medication',
    ],
    'Physicians and Rad Oncs': [
        'Physician',
        'PhysicianSpecialty',
        'RadOnc',
    ],
    'Referrals': [
        # 'Referral',
        'ReferralReach',
        'ReferralReason',
        'ReferralLocation',
    ],
    'Diagnosis': [
        'PrimaryCancerSite',
        'PrimaryDiagnosisGroup',
        'MetaStaticCancerSite',
    ],
    'Points of Contact': [
        'TransportMode',
        'AccompanyingPerson',
        'PersonConsent',
        'Location',
        'Service',
        'CodeStatus',
        'MentalStatus',
        'MinistryOfTransportationStatus',
    ],
    'CarePlans': [
        'CarePlan',
        'Structure',
        'BoneMetsComplexity',
        'CarePlanNumFields',
    ],

}


class LocationAdmin(admin.ModelAdmin):

    change_form_template = 'admin/location_change.html'


class MinistryOfTransportationStatusAdmin(admin.ModelAdmin):

    prepopulated_fields = {'value': ('name',)}


basic_modles = [
    # rp_models.Patient,
    rp_models.Facility,
    rp_models.Allergy,

    rp_models.ReferralReach,
    rp_models.ReferralReason,
    rp_models.ReferralLocation,

    rp_models.Physician,
    rp_models.PhysicianSpecialty,
    rp_models.RadOnc,

    rp_models.PrimaryCancerSite,
    rp_models.MetaStaticCancerSite,
    rp_models.PrimaryDiagnosisGroup,

    rp_models.AccompanyingPerson,
    rp_models.PersonConsent,
    rp_models.Medication,
    rp_models.Service,
    rp_models.CodeStatus,
    rp_models.MentalStatus,
    rp_models.MinistryOfTransportationStatus,

    rp_models.CarePlan,
    rp_models.BoneMetsComplexity,
    rp_models.CarePlanNumFields,
    rp_models.TransportMode,
    rp_models.Structure,
    rp_models.PreviousRTCT,
]

admin.site.register([rp_models.Location], LocationAdmin)
admin.site.register(basic_modles, admin.ModelAdmin)
# admin.site.register([rp_models.Referral], admin.ModelAdmin)
