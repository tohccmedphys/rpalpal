
import googlemaps

from django.conf import settings
from django.contrib.auth.models import User, Group
from django.core.validators import RegexValidator
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext as _

gmaps = googlemaps.Client(key=settings.GOOGLE_API_KEY, timeout=5)

MALE = 'm'
FEMALE = 'f'
GENDER_CHOICES = (
    (MALE, _('Male')),
    (FEMALE, _('Female ')),
)

WALK = 'w'
BIKE = 'b'
PUBLIC_TRANSPORT = 'pt'
ROAD_VEHICLE = 'rv'
BASE_TRANSPORT_MODES = (
    (WALK, _('Walking')),
    (BIKE, _('Biking')),
    (PUBLIC_TRANSPORT, _('Public Transport')),
    (ROAD_VEHICLE, _('Road Vehicle'))
)

PHONE = 'p'
TEXT = 't'
EMAIL = 'e'
FAX = 'f'
IN_PERSON = 'i'
TELE_MED = 'm'
PAPER = 'a'
COMMUNICATION_MODES = (
    (PHONE, _('Phone')),
    (TEXT, _('Text')),
    (EMAIL, _('Email')),
    (FAX, _('Fax')),
    (PAPER, _('Paper')),
)
PHYSICIAN_COMMUNICATION_MODES = (
    (PHONE, _('Phone')),
    (TEXT, _('Text')),
    (EMAIL, _('Email')),
    (FAX, _('Fax')),
)

ELECTRONS = 'e'
PHOTONS = 'p'
RT_TYPES = (
    (ELECTRONS, _('Electrons')),
    (PHOTONS, _('Photons'))
)

MODULATED = 'm'
NON_MODULATED = 'nm'
PLACED_FIELDS = 'pf'
VMAT = 'vm'
RT_COMPLEXITIES = (
    (MODULATED, _('Modulated')),
    (NON_MODULATED, _('Non-Modulated')),
    (PLACED_FIELDS, _('Placed Fields')),
    (VMAT, _('VMAT'))
)

DAILY = 'daily'
WEEKLY = 'weekly'
_0721 = '0,7,21'
PATTERNS = (
    (DAILY, _('Daily')),
    (WEEKLY, _('Weekly')),
    (_0721, _('0,7,21'))
)

POC_IN_PERSON = 'in_person'
POC_TELEMED = 'telemed'
POC_TELEPHONE = 'telephone'
POINT_OF_CONTACT_MODES = (
    ('', '-----'),
    (POC_IN_PERSON, 'In Person'),
    (POC_TELEMED, 'Telemed'),
    (POC_TELEPHONE, 'Telephone'),
)

FOLLOWUP = 'followup'
CONSULT = 'consult'
REVIEW = 'review'
POINT_OF_CONTACT_TYPES = (
    ('', '-----'),
    (FOLLOWUP, 'Followup'),
    (CONSULT, 'Consult'),
    (REVIEW, 'Review')
)

ECOGS = (
    (0, '0 - Fully active, able to carry on all pre-disease performance without restriction'),
    (1, '1 - Restricted in physically strenuous activity but ambulatory and able to carry out '
        'work of a light or sedentary nature, e.g., light house work, office work'),
    (2, '2 - Ambulatory and capable of all selfcare but unable to carry out any work '
        'activities. Up and about more than 50% of waking hours'),
    (3, '3 - Capable of only limited selfcare, confined to bed or chair more than 50% of waking hours'),
    (4, '4 - Completely disabled. Cannot carry on any selfcare. Totally confined to bed or chair'),
    (5, '5 - Dead'),
)
PPSS = (
    ('100%', '100%'),
    ('90%', '90%'),
    ('80%', '80%'),
    ('70%', '70%'),
    ('60%', '60%'),
    ('50%', '50%'),
    ('40%', '40%'),
    ('30%', '30%'),
    ('20%', '20%'),
    ('10%', '10%'),
    ('0%', '0%'),
)
FRAILITIES = (
    ('very_fit', 'Very fit'),
    ('well', 'Well'),
    ('managing_well', 'Managing Well'),
    ('vulnerable', 'Vulnerable'),
    ('mildly_frail', 'Mildly Frail'),
    ('moderately_frail', 'Moderately Frail'),
    ('severely_frail', 'Severely Frail'),
    ('very_severely_frail', 'Very Severely Frail'),
    ('terminally_ill', 'Terminally Ill'),
)

IN = 'in'
OUT = 'out'
IN_OUT = (
    (IN, 'In'),
    (OUT, 'Out')
)

PERMISSIONS = (
    ('rpal.change_careplan', 'Can Change Careplans', 'Allows user to add/edit careplans'),
    ('rpal.change_patient', 'Can Delete Patient Plan', 'Allows user to add/edit patient information'),
)

phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$', message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")


def get_choice_text(val, obj):
    found = False
    to_return = None
    for oc in obj:
        for o in oc:
            if o == val:
                found = True
            elif found:
                to_return = o
        if found:
            break
    return to_return


class BaseNameObject(models.Model):

    name = models.CharField(max_length=64, unique=True)

    class Meta:
        abstract = True

    def __str__(self):
        return self.name


class BaseNumObject(models.Model):

    number = models.IntegerField(unique=True)

    class Meta:
        abstract = True

    def __str__(self):
        return str(self.number)

    @classmethod
    def get_choices(cls):
        return ((bno.id, bno.number) for bno in cls.objects.all())

    @classmethod
    def get_choices_values(cls):
        return ((bno.number, bno.number) for bno in cls.objects.all())


class Facility(BaseNameObject):

    class Meta:
        verbose_name_plural = 'Facilities'


class Allergy(BaseNameObject):

    class Meta:
        verbose_name_plural = 'Allergies'


class PreviousRTCT(BaseNameObject):
    pass


class Person(models.Model):

    first_name = models.CharField(verbose_name=_('First Name'), max_length=30)
    middle_name = models.CharField(verbose_name=_('Middle Name'), max_length=30, null=True, blank=True)
    last_name = models.CharField(verbose_name=_('Last Name'), max_length=30)
    phone_number = models.CharField(null=True, blank=True, validators=[phone_regex], max_length=16)
    datetime_entered = models.DateTimeField(blank=True, editable=False)

    class Meta:
        abstract = True

    def __str__(self):
        return '%s %s' % (self.first_name, self.last_name)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if not self.pk:
            self.datetime_entered = timezone.now()
        super().save(force_insert=force_insert, force_update=force_update, using=using, update_fields=update_fields)

    def get_full_name(self):
        return '{} {}{}'.format(self.first_name, self.middle_name + ' ' if self.middle_name else '', self.last_name)


class Patient(Person):

    facilities = models.ManyToManyField(Facility, blank=True)
    allergies = models.ManyToManyField(Allergy, blank=True)

    mrn = models.CharField(max_length=8, unique=True, help_text=_('Patients MRN'))
    date_birth = models.DateField(blank=True, null=True, verbose_name=_('Date of birth'))
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES)
    home_address = models.CharField(max_length=255, blank=True, null=True)
    deceased = models.BooleanField(default=False)
    date_deceased = models.DateField(blank=True, null=True, verbose_name=_('Date of death'))

    def __str__(self):
        return 'Patient %s' % self.mrn

    def first_visit(self):
        return self.visit_set.first() if self.visit_set.exists() else None

    def get_verbose_dict(self):

        return {
            'id': self.id,
            'first_name': self.first_name,
            'middle_name': self.middle_name if self.middle_name else '',
            'last_name': self.last_name,
            'phone_number': self.phone_number if self.phone_number else '',
            'facilities': [{'name': f.name, 'id': f.id} for f in self.facilities.all()],
            'allergies': [{'name': a.name, 'id': a.id} for a in self.allergies.all()],
            'mrn': self.mrn,
            'date_birth': self.date_birth.strftime(settings.SIMPLE_DATE_FORMAT) if self.date_birth else '',
            'gender': {'value': self.gender, 'name': get_choice_text(self.gender, GENDER_CHOICES)},
            'home_address': self.home_address if self.home_address else '',
            'deceased': self.deceased,
            'date_deceased': self.date_deceased.strftime(settings.SIMPLE_DATE_FORMAT) if self.date_deceased else '',
        }


class PrimaryCancerSite(BaseNameObject):
    pass


class MetaStaticCancerSite(BaseNameObject):
    pass


class PrimaryDiagnosisGroup(BaseNameObject):
    pass


class MetaStaticCancerSiteDiagnosisCollection(models.Model):

    date = models.DateField(null=True, blank=True)
    metastatic_cancer_site = models.ForeignKey(MetaStaticCancerSite, on_delete=models.PROTECT)
    diagnosis = models.ForeignKey('Diagnosis', on_delete=models.CASCADE)


class DiagnosisManager(models.Manager):

    def get_queryset(self):
        return super().get_queryset().select_related('patient')

    def get_verbose_queryset(self):
        return self.get_queryset().select_related('primary_cancer_site').prefetch_related('cancer_sites_metastatic')


class Diagnosis(models.Model):

    patient = models.ForeignKey(Patient, on_delete=models.CASCADE)
    primary_cancer_site = models.ForeignKey(PrimaryCancerSite, on_delete=models.PROTECT)
    cancer_sites_metastatic = models.ManyToManyField(
        MetaStaticCancerSite,
        through=MetaStaticCancerSiteDiagnosisCollection,
        blank=True,
        related_name='diagnoses'
    )
    primary_diagnosis_group = models.ForeignKey(PrimaryDiagnosisGroup, blank=True, null=True, on_delete=models.PROTECT)

    relative_id = models.PositiveIntegerField(default=0, help_text=_('Number of this diagnosis relative to other diagnoses for a patient'))
    primary_cancer_dx_description = models.TextField(null=True, blank=True)
    pathologic_diagnosis = models.BooleanField(default=True)
    date_diagnosis = models.DateField(null=True, blank=True)
    radiographic_diagnosis_explanation = models.TextField(null=True, blank=True)

    objects = DiagnosisManager()

    def __str__(self):
        return 'Dianosis for patient %s' % self.patient.mrn

    def save(self, *args, reorder=True, **kwargs):
        super().save(*args, **kwargs)
        if reorder:
            patient = self.patient
            diagnoses = patient.diagnosis_set.order_by('date_diagnosis').all()
            first_d = diagnoses[0]
            first_d.relative_id = 1
            first_d.save(reorder=False)
            count = 1
            for d in diagnoses.exclude(id=first_d.id):
                count += 1
                d.relative_id = count
                d.save(reorder=False)

    def get_verbose_dict(self):
        return {
            'id': self.id,
            'relative_id': self.relative_id,
            'patient': {'id': self.patient_id, 'mrn': self.patient.mrn},
            'primary_cancer_site': {'id': self.primary_cancer_site_id, 'name': self.primary_cancer_site.name},
            'primary_diagnosis_group': {
                'id': self.primary_diagnosis_group_id,
                'name': self.primary_diagnosis_group.name
            } if self.primary_diagnosis_group else '',
            'cancer_sites_metastatic': [{
                'id': mscsdc.id,
                'cancer_site_metastatic_id': mscsdc.metastatic_cancer_site.id,
                'cancer_site_metastatic_name': mscsdc.metastatic_cancer_site.name,
                'date': mscsdc.date
            } for mscsdc in self.metastaticcancersitediagnosiscollection_set.all()],
            'pathologic_diagnosis': self.pathologic_diagnosis,
            'date_diagnosis': self.date_diagnosis,
            'radiographic_diagnosis_explanation': self.radiographic_diagnosis_explanation,
            'primary_cancer_dx_description': self.primary_cancer_dx_description
        }


class PhysicianSpecialty(BaseNameObject):

    class Meta:
        verbose_name_plural = 'Physician specialties'


class Physician(Person):

    physician_specialty = models.ForeignKey(PhysicianSpecialty, on_delete=models.PROTECT)

    text_number = models.CharField(null=True, blank=True, validators=[phone_regex], max_length=16)
    fax_number = models.CharField(null=True, blank=True, validators=[phone_regex], max_length=16)
    email = models.EmailField(verbose_name=_('Email Address'), blank=True, null=True)
    active = models.BooleanField(default=True)

    def get_contact_info(self):
        return {ct: getattr(self, ct, None) for ct in ['phone_number', 'email', 'text_number', 'fax_number']}

    def get_verbose_dict(self):
        return {
            'id': self.id,
            'first_name': self.first_name,
            'last_name': self.last_name,
            'contact_info': self.get_contact_info(),
            'physician_specialty': {'id': self.physician_specialty_id, 'name': self.physician_specialty.name},
            'active': self.active,
        }


class RadOnc(Person):

    active = models.BooleanField(default=True)
    email = models.EmailField(verbose_name=_('Email Address'), blank=True, null=True)

    def get_verbose_dict(self):
        return {
            'id': self.id,
            'first_name': self.first_name,
            'last_name': self.last_name,
            'email': self.email,
            'phone_number': self.phone_number,
            'active': self.active,
        }


class ReferralReach(BaseNameObject):

    help_text = 'Ways the referral can reach RPal'

    class Meta:
        ordering = ('name',)
        verbose_name_plural = 'Referrals via'
        verbose_name = 'Referral via'


class ReferralReason(BaseNameObject):

    class Meta:
        verbose_name_plural = 'Referral reasons'


class ReferralLocation(BaseNameObject):

    help_text = 'Locations patient can be at time of referral'


class ReferralManager(models.Manager):

    def get_queryset(self):
        return super().get_queryset().select_related('patient')

    def get_verbose_queryset(self):
        return super().get_queryset().select_related(
            'patient', 'referring_physician', 'referring_physician__physician_specialty', 'referral_reach',
        ).prefetch_related('referral_reasons')


class Referral(models.Model):

    patient = models.ForeignKey(Patient, on_delete=models.CASCADE)
    referring_physician = models.ForeignKey(Physician, null=True, on_delete=models.PROTECT)
    referral_reach = models.ForeignKey(
        ReferralReach, blank=True, null=True, help_text=_('How did this referral reach RPAL?'),
        on_delete=models.PROTECT, verbose_name=_('Referral Via')
    )
    referral_reasons = models.ManyToManyField(ReferralReason, blank=True, verbose_name=_('Reasons'))
    referral_location = models.ForeignKey(
        ReferralLocation, blank=True, null=True, verbose_name=_('Location'), on_delete=models.PROTECT
    )

    relative_id = models.PositiveIntegerField(
        default=0, help_text=_('Number of this referral relative to other referrals for a patient')
    )
    physician_contacted_by = models.CharField(
        choices=PHYSICIAN_COMMUNICATION_MODES, null=True, blank=True, max_length=1
    )
    referring_communication_mode = models.CharField(
        choices=COMMUNICATION_MODES, blank=True, null=True, max_length=1, verbose_name=_('Communication Mode')
    )
    date_referred = models.DateField()

    objects = ReferralManager()

    def __str__(self):
        return 'Referral %s for patient %s' % ('by %s' % get_choice_text(self.referring_communication_mode, COMMUNICATION_MODES) if self.referring_communication_mode else '', self.patient)

    def save(self, *args, reorder=True, **kwargs):
        super().save(*args, **kwargs)
        if reorder:
            patient = self.patient
            rs = patient.referral_set.order_by('date_referred').all()
            first_r = rs[0]
            first_r.relative_id = 1
            first_r.save(reorder=False)
            count = 1
            for r in rs.exclude(id=first_r.id):
                count += 1
                r.relative_id = count
                r.save(reorder=False)

    def get_verbose_dict(self):
        return {
            'id': self.id,
            'relative_id': self.relative_id,
            'patient': {'mrn': self.patient.mrn, 'id': self.patient_id},
            'referring_physician': self.referring_physician.get_verbose_dict(),
            'physician_contacted_by': self.physician_contacted_by,
            'date_referred': self.date_referred,
            'referral_reach': {'id': self.referral_reach_id, 'name': self.referral_reach.name} if self.referral_reach else '',
            'referral_reasons': [{'name': rr.name, 'id': rr.id} for rr in self.referral_reasons.all()],
            'referral_location': {'id': self.referral_location_id, 'name': self.referral_location.name} if self.referral_location else '',
            'referring_communication_mode': {'value': self.referring_communication_mode, 'name': get_choice_text(self.referring_communication_mode, COMMUNICATION_MODES)},
        }


class TransportMode(BaseNameObject):

    base_transport_mode = models.CharField(max_length=2, choices=BASE_TRANSPORT_MODES)


class Location(BaseNameObject):
    address = models.CharField(max_length=255)

    help_text = 'Locations of visits and treatments'

    class Meta:
        verbose_name_plural = 'Locations'


class AccompanyingPerson(BaseNameObject):

    help_text = 'People that could accompany the patient'

    class Meta:
        verbose_name_plural = 'Accompanying People'


class Medication(BaseNameObject):
    pass


class Service(BaseNameObject):

    help_text = 'Referrals made to other services at time of visit'


class CodeStatus(BaseNameObject):

    class Meta:
        verbose_name_plural = 'Code statuses'


class MentalStatus(BaseNameObject):

    class Meta:
        verbose_name_plural = 'Mental statuses'


class PersonConsent(BaseNameObject):

    help_text = 'People that could consent for patient'

    class Meta:
        verbose_name_plural = 'Consenting People'


class MinistryOfTransportationStatus(BaseNameObject):

    class Meta:
        verbose_name_plural = 'Ministry Of Transportation Statuses'


class VisitManager(models.Manager):

    def get_queryset(self):
        return super().get_queryset().select_related('patient')

    def get_verbose_queryset(self):
        return self.get_queryset().select_related(
            'transport_mode', 'location', 'person_consent', 'code_status', 'mental_status'
        ).prefetch_related('medications', 'medications_modified', 'services_referred', 'accompanying_people')


class Visit(models.Model):

    patient = models.ForeignKey(Patient, on_delete=models.CASCADE, related_name='visit_set')

    transport_mode = models.ForeignKey(
        TransportMode, null=True, blank=True, help_text=_('Mode of transport to visit'), on_delete=models.PROTECT
    )
    accompanying_people = models.ManyToManyField(
        AccompanyingPerson, blank=True, help_text=_('Persons accompanying patient')
    )
    location = models.ForeignKey(Location, on_delete=models.PROTECT, null=True, blank=True)

    coming_from_lat_lng = models.CharField(max_length=20, null=True, blank=True)
    coming_from = models.CharField(
        max_length=255, blank=True, null=True, help_text=_('Address patient is travelling from')
    )
    duration_travel = models.DurationField(blank=True, null=True, help_text=_('Time to travel to visit'))  # Consider linking with above via map click.

    person_consent = models.ForeignKey(
        PersonConsent, blank=True, null=True, help_text=_('Person giving consent for treatment'),
        related_name='visits', on_delete=models.PROTECT, verbose_name=_('Consenting person')
    )
    medications = models.ManyToManyField(
        Medication, blank=True, help_text=_('Current cancer-related medications at time of visit')
    )
    medications_modified = models.ManyToManyField(
        Medication, blank=True, help_text=_('Supportive care medication prescribed/modified at time of visit'),
        related_name='visits'
    )
    treatment_course = models.ForeignKey(
        'TreatmentCourse', null=True, blank=True, related_name='visit_rev_or_fol',
        help_text=_('If this is a follow up, what treatment course is it for'), on_delete=models.SET_NULL
    )
    referral = models.ForeignKey(
        'Referral', null=True, blank=True,
        help_text=_('If this is a consult, what referral is it for'), on_delete=models.SET_NULL
    )
    rad_onc = models.ForeignKey(
        RadOnc, null=True, blank=True, verbose_name=_('Rad Onc'), on_delete=models.PROTECT
    )

    point_of_contact_mode = models.CharField(max_length=10, choices=POINT_OF_CONTACT_MODES)
    point_of_contact_type = models.CharField(max_length=10, choices=POINT_OF_CONTACT_TYPES)
    stage = models.IntegerField(
        choices=[(1, 1), (2, 2), (3, 3), (4, 4)], null=True, blank=True, help_text=_('Stage at time of visit')
    )
    services_referred = models.ManyToManyField(
        Service, help_text=_('Referrals made to other services at time of visit'), blank=True
    )
    code_status = models.ForeignKey(
        CodeStatus, blank=True, null=True, help_text=_('Code status at time of visit '), on_delete=models.PROTECT
    )
    life_expectancy = models.PositiveIntegerField(
        blank=True, null=True, help_text=_('Life expectancy at time of visit ')
    )
    mental_status = models.ForeignKey(
        MentalStatus, blank=True, null=True, help_text=_('Mental status at time of visit'), on_delete=models.PROTECT
    )
    ministry_of_transportation_status = models.CharField(max_length=64, null=True, blank=True)
    frailty = models.CharField(max_length=20, choices=FRAILITIES, null=True, blank=True)
    in_out = models.CharField(max_length=3, choices=IN_OUT, default=OUT, help_text=_(''))

    is_on_chemo = models.BooleanField(default=False, help_text=_('On chemotherapy at time of visit'))
    is_treatment_decided_on_visit = models.BooleanField(
        default=False, help_text=_('Is treatment decided at time of visit')
    )
    is_first_visit = models.BooleanField(default=False)
    relative_id = models.PositiveIntegerField(default=0, help_text=_('Number of this visit relative to other visits for a patient'))
    date = models.DateField(verbose_name=_('Date'))
    ecog = models.IntegerField(choices=ECOGS, blank=True, null=True)
    pps = models.CharField(choices=PPSS, max_length=4, blank=True, null=True)

    objects = VisitManager()

    class Meta:
        get_latest_by = 'date'
        permissions = (('can_view_poc', 'Can view points of contact lists'),)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__original_coming_from = self.coming_from

    def __str__(self):
        return 'Visit on %s for patient %s' % (self.date, self.patient)

    def detail_string(self):
        return '{}, {}'.format(self.id, self.date.strftime('%d-%m-%Y'))

    def display(self):
        return '{} - {} - {}'.format(
            get_choice_text(self.point_of_contact_type, POINT_OF_CONTACT_TYPES),
            get_choice_text(self.point_of_contact_mode, POINT_OF_CONTACT_MODES),
            self.date.strftime('%b %d %Y')
        )

    def get_poc_type(self):
        return get_choice_text(self.point_of_contact_type, POINT_OF_CONTACT_TYPES)

    def save(self, *args, reorder=True, **kwargs):

        if self.coming_from and self.coming_from != self.__original_coming_from:
            try:
                g_response = gmaps.geocode(self.coming_from)
                self.coming_from_lat_lng = '{},{}'.format(
                    str(g_response[0]['geometry']['location']['lat'])[:8],
                    str(g_response[0]['geometry']['location']['lng'])[:8]
                )
            except googlemaps.exceptions.Timeout:
                pass
        elif not self.coming_from:
            self.coming_from_lat_lng = None

        super().save(*args, **kwargs)
        self.__original_coming_from_lat_lng = self.coming_from_lat_lng

        if reorder:
            patient = self.patient
            visits = patient.visit_set.order_by('date').all()
            first_visit = visits[0]
            first_visit.is_first_visit = True
            first_visit.relative_id = 1
            first_visit.save(reorder=False)
            count = 1
            for v in visits.exclude(id=first_visit.id):
                v.is_first_visit = False
                count += 1
                v.relative_id = count
                v.save(reorder=False)

    def get_verbose_dict(self):
        return {
            'id': self.id,
            'relative_id': self.relative_id,
            'patient': {'mrn': self.patient.mrn, 'id': self.patient_id},
            'date': self.date,
            'coming_from': self.coming_from if self.coming_from else '',
            'coming_from_lat_lng': self.coming_from_lat_lng if self.coming_from_lat_lng else '',
            'duration_travel': self.duration_travel.total_seconds() if self.duration_travel else '',
            # 'is_initial_visit': self.is_initial_visit,
            'is_treatment_decided_on_visit': self.is_treatment_decided_on_visit,
            'is_on_chemo': self.is_on_chemo,
            'is_first_visit': self.is_first_visit,
            'stage': {'value': self.stage, 'name': self.stage} if self.stage else '',

            'point_of_contact_mode': {
                'name': get_choice_text(self.point_of_contact_mode, POINT_OF_CONTACT_MODES),
                'value': self.point_of_contact_mode
            },
            'point_of_contact_type': {
                'name': get_choice_text(self.point_of_contact_type, POINT_OF_CONTACT_TYPES),
                'value': self.point_of_contact_type
            },
            'transport_mode': {
                'id': self.transport_mode.id,
                'name': self.transport_mode.name
            } if self.transport_mode else '',
            'person_consent': {
                'id': self.person_consent.id,
                'name': self.person_consent.name
            } if self.person_consent else '',
            'location': {
                'id': self.location.id,
                'name': self.location.name,
                'address': self.location.address
            } if self.location else '',
            'code_status': {'id': self.code_status.id, 'name': self.code_status.name} if self.code_status else '',
            'life_expectancy': self.life_expectancy,
            'mental_status': {
                'id': self.mental_status.id,
                'name': self.mental_status.name
            } if self.mental_status else '',
            'ecog': self.ecog,
            'pps': self.pps,
            'ministry_of_transportation_status': {
                'name': self.ministry_of_transportation_status,
                'value': self.ministry_of_transportation_status
            } if self.ministry_of_transportation_status else '',
            'frailty': {
                'name': get_choice_text(self.frailty, FRAILITIES),
                'value': self.frailty
            } if self.frailty else '',
            'in_out': {
                'name': get_choice_text(self.in_out, IN_OUT),
                'value': self.in_out
            },
            'rad_onc': self.rad_onc.get_verbose_dict() if self.rad_onc else '',
            'medications': [{'id': m.id, 'name': m.name} for m in self.medications.all()],
            'medications_modified': [{'id': mm.id, 'name': mm.name} for mm in self.medications_modified.all()],
            'services_referred': [{'id': sr.id, 'name': sr.name} for sr in self.services_referred.all()],
            'accompanying_people': [{'id': ap.id, 'name': ap.name} for ap in self.accompanying_people.all()],

            'treatment_course_id': self.treatment_course.id if self.treatment_course else '',
            'referral_id': self.referral.id if self.referral else ''
        }


class Structure(BaseNameObject):
    pass


class CarePlan(models.Model):

    dose = models.IntegerField(null=True, blank=True)
    fractions = models.IntegerField(null=True, blank=True)
    type = models.CharField(choices=RT_TYPES, max_length=1, null=True, blank=True)
    complexity = models.CharField(choices=RT_COMPLEXITIES, max_length=2, null=True, blank=True)
    name = models.CharField(max_length=255, help_text=_("A Unique name for this careplan"))
    pattern = models.CharField(max_length=8, choices=PATTERNS, null=True, blank=True)

    def __str__(self):
        return self.name

    def get_verbose_dict(self):
        return {
            'id': self.id,
            'name': self.name,
            'dose': self.dose,
            'fractions': self.fractions,
            'type': {'value': self.type, 'name': get_choice_text(self.type, RT_TYPES)},
            'complexity': {'value': self.complexity, 'name': get_choice_text(self.complexity, RT_COMPLEXITIES)},
            'pattern': {'value': self.pattern, 'name': get_choice_text(self.pattern, PATTERNS)},

        }


class BoneMetsComplexity(BaseNameObject):

    class Meta:
        verbose_name_plural = 'Bone mets complexities'


class CarePlanNumFields(BaseNameObject):

    help_text = 'Number of fields for non-modulated care plans'

    class Meta:
        verbose_name_plural = 'Care plan number fields'


class TreatmentCourseManager(models.Manager):

    def get_queryset(self):
        return super().get_queryset().select_related('patient')

    def get_verbose_queryset(self):
        return self.get_queryset().select_related(
            'patient', 'visit', 'care_plan', 'care_plan_num_fields', 'structure_treated'
        ).prefetch_related(
            'bone_mets_complexity'
        )


class TreatmentCourseBoneMetsComplexityCollection(models.Model):

    treatment_course = models.ForeignKey('TreatmentCourse', on_delete=models.CASCADE)
    bone_mets_complexity = models.ForeignKey(BoneMetsComplexity, on_delete=models.CASCADE)

    class Meta:
        unique_together = ('treatment_course', 'bone_mets_complexity')


class TreatmentCourse(models.Model):

    patient = models.ForeignKey(Patient, on_delete=models.CASCADE, related_name='treatmentcourse_set')
    visit = models.ForeignKey(Visit, on_delete=models.SET_NULL, null=True, blank=True)
    structure_treated = models.ForeignKey(Structure, on_delete=models.PROTECT, related_name='treatment_courses')
    care_plan = models.ForeignKey(CarePlan, on_delete=models.PROTECT)
    care_plan_num_fields = models.ForeignKey(CarePlanNumFields, blank=True, null=True, on_delete=models.PROTECT)
    bone_mets_complexity = models.ManyToManyField(
        BoneMetsComplexity, blank=True, through=TreatmentCourseBoneMetsComplexityCollection,
        related_name='bonemetscomplexity_set'
    )

    dose_override = models.IntegerField(null=True, blank=True)
    fractions_override = models.IntegerField(null=True, blank=True)
    type_override = models.CharField(choices=RT_TYPES, max_length=1, null=True, blank=True)
    complexity_override = models.CharField(choices=RT_COMPLEXITIES, max_length=2, null=True, blank=True)
    pattern_override = models.CharField(max_length=8, choices=PATTERNS, null=True, blank=True)

    # treatment course fields:
    relative_id = models.PositiveIntegerField(
        default=0, help_text=_('Number of this treatment course relative to other treatment courses for a patient')
    )
    date_start = models.DateField()
    date_end = models.DateField()
    ct_date = models.DateField(null=True, blank=True)
    bone_mets_involved = models.BooleanField(default=False)
    is_prescribed_course_complete = models.BooleanField(default=True)
    same_day_start_date = models.BooleanField(default=False)
    explanation = models.TextField(null=True, blank=True)

    transport_mode = models.ForeignKey(
        TransportMode, null=True, blank=True, help_text=_('Mode of transport to visit'), on_delete=models.PROTECT
    )
    location = models.ForeignKey(Location, on_delete=models.PROTECT, null=True, blank=True)
    coming_from = models.CharField(
        max_length=255, blank=True, null=True, help_text=_('Address patient is travelling from')
    )
    coming_from_lat_lng = models.CharField(max_length=20, null=True, blank=True)
    duration_travel = models.DurationField(blank=True, null=True, help_text=_('Time to travel to visit'))  # Consider linking with above via map click.

    objects = TreatmentCourseManager()

    class Meta:
        default_permissions = ()
        permissions = (
            ('can_view_treatments', 'Can view list of treatments'),
            ('view_stats', 'Can view stats page')
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__original_coming_from = self.coming_from

    def __str__(self):
        return 'Treatment course for patient %s' % self.patient

    def save(self, *args, reorder=True, **kwargs):

        if self.coming_from and self.coming_from != self.__original_coming_from:
            try:
                g_response = gmaps.geocode(self.coming_from)
                self.coming_from_lat_lng = '{},{}'.format(
                    str(g_response[0]['geometry']['location']['lat'])[:8],
                    str(g_response[0]['geometry']['location']['lng'])[:8]
                )
            except googlemaps.exceptions.Timeout:
                pass
        elif not self.coming_from:
            self.coming_from_lat_lng = None

        super().save(*args, **kwargs)
        self.__original_coming_from_lat_lng = self.coming_from_lat_lng

        if reorder:
            patient = self.patient
            tcs = patient.treatmentcourse_set.order_by('date_start').all()
            first_tc = tcs[0]
            first_tc.relative_id = 1
            first_tc.save(reorder=False)
            count = 1
            for tc in tcs.exclude(id=first_tc.id):
                count += 1
                tc.relative_id = count
                tc.save(reorder=False)

    def get_verbose_dict(self):
        return {

            'id': self.id,
            'relative_id': self.relative_id,
            'patient': {'mrn': self.patient.mrn, 'id': self.patient_id},
            'dose_override': self.dose_override,
            'fractions_override': self.fractions_override,

            'type_override': {'value': self.type_override, 'name': get_choice_text(self.type_override, RT_TYPES)} if self.type_override else '',
            'complexity_override': {'value': self.complexity_override, 'name': get_choice_text(self.complexity_override, RT_COMPLEXITIES)} if self.complexity_override else '',
            'pattern_override': {'value': self.pattern_override, 'name': get_choice_text(self.pattern_override, PATTERNS)} if self.pattern_override else '',

            'visit': {'id': self.visit_id},
            'care_plan': {'id': self.care_plan_id, 'name': self.care_plan.name} if self.care_plan else None,
            'care_plan_num_fields': {'id': self.care_plan_num_fields_id, 'name': self.care_plan_num_fields.name} if self.care_plan_num_fields else '',
            'bone_mets_complexity': [{'id': bmc.id, 'name': bmc.name} for bmc in self.bone_mets_complexity.all()],
            # 'treatment_course': self.treatment_course.get_verbose_dict() if self.treatment_course else None,

            'structure_treated': {'id': self.structure_treated.id, 'name': self.structure_treated.name},

            'date_start': self.date_start,
            'date_end': self.date_end,
            'ct_date': self.ct_date if self.ct_date else '',
            'is_prescribed_course_complete': self.is_prescribed_course_complete,
            'same_day_start_date': self.same_day_start_date,
            'bone_mets_involved': self.bone_mets_involved,
            'explanation': self.explanation,
            'coming_from': self.coming_from if self.coming_from else '',
            'coming_from_lat_lng': self.coming_from_lat_lng if self.coming_from_lat_lng else '',
            'duration_travel': self.duration_travel.total_seconds() if self.duration_travel else '',
            'transport_mode': {'id': self.transport_mode.id, 'name': self.transport_mode.name} if self.transport_mode else '',
            'location': {'id': self.location.id, 'name': self.location.name, 'address': self.location.address} if self.location else '',

        }
