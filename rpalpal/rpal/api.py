
import traceback

from django.db.models import Q
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_protect

from rpal import models as rp_models
from rpal import forms as rp_forms


def patients_mrn_searcher(request):
    q = request.GET['q']
    name_arr = q.split()
    patients = rp_models.Patient.objects.all()
    for n in name_arr:
        patients = patients.filter(Q(mrn__icontains=n) | Q(first_name__icontains=n) | Q(last_name__icontains=n))
    patients = patients.order_by('mrn')[0:50].values('id', 'mrn', 'first_name', 'last_name')

    return JsonResponse({'patients': list(patients)})


def get_form(obj):
    form_dict = {
        'patient': rp_forms.PatientForm,
        'referral': rp_forms.ReferralForm,
        'diagnosis': rp_forms.DiagnosisForm,
        'visit': rp_forms.VisitForm,
        'treatment_course': rp_forms.TreatmentCourseForm,
        'physician': rp_forms.PhysicianForm,
    }
    return form_dict[obj]


def physician_searcher(request):
    physician_id = request.GET['id']
    physician = rp_models.Physician.objects.select_related('physician_specialty').get(pk=physician_id)
    return JsonResponse({'physician': physician.get_verbose_dict()})


def active_physicians_searcher(request):
    active_physicians = rp_models.Physician.objects.select_related('physician_specialty').filter(active=True)
    return JsonResponse({'active_physicians': [ap.get_verbose_dict() for ap in active_physicians]})


def active_rad_oncs_searcher(request):
    active_rad_oncs = rp_models.RadOnc.objects.filter(active=True)
    return JsonResponse({'active_rad_oncs': [aro.get_verbose_dict() for aro in active_rad_oncs]})


def patients_searcher(request):
    patient_id = request.GET['patient_id']
    patient = rp_models.Patient.objects.prefetch_related('facilities', 'allergies').get(pk=patient_id)
    return JsonResponse({'patient': patient.get_verbose_dict()})


def metastatic_cancer_sites_searcher(request):
    return JsonResponse({
        'metastatic_cancer_sites': [
            {'id': mcs.id, 'name': mcs.name} for mcs in rp_models.MetaStaticCancerSite.objects.all()
        ]
    })


@csrf_protect
def delete_object_ajax(request):

    form_class = get_form(request.POST.get('obj_type'))
    model_class = form_class.Meta.model

    object_id = request.POST.get('id')
    if object_id:
        try:
            objekt = model_class.objects.get(id=object_id)
            objekt.delete()
            return JsonResponse({'success': True})
        except Exception as e:
            print(e)
            print(traceback.format_exc())
            return JsonResponse({'error': str(e), 'error_trace': str(traceback.format_exc())}, status=500)

    return JsonResponse({
        'error': 'Error deleting {}'.format(request.POST.get('obj_type')),
        'success': False
    })


@csrf_protect
def object_create_update_ajax(request):

    form_class = get_form(request.POST.get('obj_type'))
    model_class = form_class.Meta.model
    if request.POST.get('id'):
        is_new = False
        form = form_class(request.POST, instance=model_class.objects.get(pk=request.POST.get('id')))
    else:
        is_new = True
        form = form_class(request.POST)

    if form.is_valid():
        try:
            objekt = form.save()
        except Exception as e:
            print(e)
            print(traceback.format_exc())
            return JsonResponse({'error': str(e), 'error_trace': str(traceback.format_exc())}, status=500)
    else:
        return JsonResponse({'form_errors': form.errors, 'success': False})

    return JsonResponse({'object': objekt.get_verbose_dict(), 'success': True, 'is_new': is_new})


@csrf_protect
def diagnosis_create_update_ajax(request):
    form_class = rp_forms.DiagnosisForm
    model_class = rp_models.Diagnosis
    if request.POST.get('id'):
        is_new = False
        form = form_class(request.POST, instance=model_class.objects.get(pk=request.POST.get('id')))
    else:
        is_new = True
        form = form_class(request.POST)

    formset = rp_forms.MetaStaticCancerSiteDiagnosisCollectioFormset(
        request.POST,
        instance=form.instance
    )
    formset_okay = formset.is_valid()

    if form.is_valid() and formset_okay:
        try:
            diagnosis = form.save()
            formset.save()
        except Exception as e:
            print(e)
            return JsonResponse({'error': str(e)}, status=500)
    else:
        return JsonResponse({'form_errors': form.errors, 'success': False, 'formset_errors': formset.errors})

    return JsonResponse({'object': diagnosis.get_verbose_dict(), 'success': True, 'is_new': is_new})


def referral_searcher(request):
    patient_id = request.GET['patient_id']
    referral_qs = rp_models.Referral.objects.get_verbose_queryset().filter(patient_id=patient_id).order_by(
        'date_referred'
    )
    referrals = [r.get_verbose_dict() for r in referral_qs]

    return JsonResponse({'referrals': referrals})


def diagnoses_searcher(request):
    patient_id = request.GET['patient_id']
    diagnoses_qs = rp_models.Diagnosis.objects.get_verbose_queryset().filter(patient_id=patient_id).order_by(
        'date_diagnosis'
    )
    diagnoses = [r.get_verbose_dict() for r in diagnoses_qs]

    return JsonResponse({'diagnoses': diagnoses})


def visits_searcher(request):

    patient_id = request.GET['patient_id']
    visits_qs = rp_models.Visit.objects.get_verbose_queryset().filter(patient_id=patient_id).order_by(
        'date'
    )
    visits = [r.get_verbose_dict() for r in visits_qs]

    return JsonResponse({'visits': visits})


def all_treatment_courses_searcher(request):

    patient_id = request.GET.get('patient_id')
    tc_qs = rp_models.TreatmentCourse.objects.get_verbose_queryset().filter(patient_id=patient_id)
    tcs = [tc.get_verbose_dict() for tc in tc_qs]

    return JsonResponse({'all_treatment_courses': tcs})


def careplan_searcher(request):

    careplan_id = request.GET['careplan_id']
    careplan = rp_models.CarePlan.objects.get(pk=careplan_id)

    return JsonResponse({'careplan': careplan.get_verbose_dict()})

