define(['jquery', 'lodash', 'knockout', 'rpal_models', 'moment', 'autosize', 'select2', 'async!https://maps.googleapis.com/maps/api/js?key=' + SiteConfig.google_api_key + '&libraries=places'], function($, _, ko, RPal, moment, autosize) {

    return function (patient_details_vm) {

        var self = this;

        self.patient_details_vm = patient_details_vm;
        patient_details_vm.patient_vm = self;

        var $patient_form = $('#patient_form'),
            $add_patient_go = $('#add-patient-go'),
            $date_birth = $('#id_date_birth'),
            $date_deceased  = $('#id_date_deceased'),
            $home_address = $('#id_home_address'),
            $date_deceased_div = $('#date-deceased-div'),
            $patient_selector = $('#patient-selector');

        self.$patient_modal = $('#patient-modal');

        self.patient = ko.observable();
        self.id = ko.observable();

        self.first_name = ko.observable();
        self.middle_name = ko.observable();
        self.last_name = ko.observable();
        self.phone_number = ko.observable();
        self.facilities = ko.observable();
        self.allergies = ko.observable();
        self.mrn = ko.observable();
        self.date_birth = ko.observable();
        self.date_birth_verbose = ko.observable();
        self.gender = ko.observable();
        self.home_address = ko.observable();
        self.deceased = ko.observable(false);
        self.date_deceased = ko.observable();
        self.date_deceased_verbose = ko.observable();

        self.first_name_error = ko.observable();
        self.middle_name_error = ko.observable();
        self.last_name_error = ko.observable();
        self.phone_number_error = ko.observable();
        self.facilities_error = ko.observable();
        self.allergies_error = ko.observable();
        self.mrn_error = ko.observable();
        self.date_birth_error = ko.observable();
        self.gender_error = ko.observable();
        self.home_address_error = ko.observable();
        self.date_deceased_error = ko.observable();

        ko.computed(function() {
            if (self.deceased()) {
                $date_deceased_div.fadeIn('fast');
            } else {
                $date_deceased_div.fadeOut('fast');
            }
        });

        self.date_birth_verbose.subscribe(function() {
            if (self.date_birth_verbose()) {
                self.date_birth(moment(self.date_birth_verbose(), 'DD-MM-YYYY'))
            }
        });

        self.date_deceased_verbose.subscribe(function() {
            if (self.date_deceased_verbose()) {
                self.date_deceased(moment(self.date_deceased_verbose(), 'DD-MM-YYYY'))
            }
        });

        self.get_submit_text = ko.computed(function() {
            return self.id() ? 'Update' : 'Add'
        });

        $date_birth.dateline({
            mainDivClass: 'row',
            monthDivClass: 'col-md-6 padding-right-5',
            dayDivClass: 'col-md-2 padding-right-5 padding-left-5',
            yearDivClass: 'col-md-4 padding-left-5',
            monthClass: 'form-control select2',
            dayClass: 'form-control padding-right-5',
            yearClass: 'form-control padding-right-5'
        });
        $date_deceased.dateline({
            mainDivClass: 'row',
            monthDivClass: 'col-md-5 padding-left-5 padding-right-5',
            dayDivClass: 'col-md-3 padding-right-5 padding-left-5',
            yearDivClass: 'col-md-4 padding-left-5',
            monthClass: 'form-control select2',
            dayClass: 'form-control padding-right-5',
            yearClass: 'form-control padding-right-5'
        });

        autosize($('textarea.autosize'));

        var home_address_autocomplete;
        function initAutocomplete() {
            // Create the autocomplete object, restricting the search to geographical
            // location types.
            var center = new google.maps.LatLng(45.4, -75.6);
            var circle = new google.maps.Circle({
                center: center,
                radius: 100000
            });
            home_address_autocomplete = new google.maps.places.Autocomplete(
                $home_address[0],
                {
                    types: ['geocode'],
                }
            );
            home_address_autocomplete.setBounds(circle.getBounds());

            google.maps.event.addListener(home_address_autocomplete, 'place_changed', function () {
                var new_address = home_address_autocomplete.getPlace().formatted_address;
                if (new_address !== '') {
                    self.home_address(home_address_autocomplete.getPlace().formatted_address);
                }
            })
        }

        initAutocomplete();

        function serialize_patient_form() {

            var inputs = [
                'csrfmiddlewaretoken=' + $('input[name=csrfmiddlewaretoken]').val(),

                'first_name=' + self.first_name(),
                'middle_name=' + self.middle_name(),
                'last_name=' + self.last_name(),
                'phone_number=' + self.phone_number(),
                'mrn=' + self.mrn(),
                'date_birth=' + (self.date_birth() ? self.date_birth().format('DD-MM-YYYY') : ''),
                'gender=' + self.gender(),
                'home_address=' + (self.home_address() ? self.home_address() : ''),
                'deceased=' + self.deceased(),
                'obj_type=patient'
            ];
            if (self.deceased()) {
                inputs.push('date_deceased=' + (self.date_deceased() ? self.date_deceased().format('DD-MM-YYYY') : ''));
            }
            $.each(self.facilities(), function(i, v) {
                inputs.push('facilities=' + v);
            });
            $.each(self.allergies(), function(i, v) {
                inputs.push('allergies=' + v);
            });
            if (self.id()) {
                inputs.push('id=' + self.id());
            }
            return inputs.join('&');
        }

        $patient_form.submit(function() {

            var url = RPP_URLs.object_create_update_ajax,
                data = serialize_patient_form();

            self.clear_errors();

            $.ajax({
                url: url,
                data: data,
                method: 'POST',
                success: function(res) {
                    if (!res.success) {
                        $.each(res.form_errors, function (k, v) {
                            self[k + '_error'](v);
                        });
                    } else {
                        if (res.is_new) {
                            // patient_details_vm.patient(new RPal.models.Patient(res.object));
                            $patient_selector.select2('val', '');
                            self.patient_details_vm.patient_id(res.object.id);
                        } else {
                            self.patient_details_vm.patient_id.valueHasMutated();
                        }
                        self.$patient_modal.modal('hide');
                        self.clear();
                    }
                    self.patient_details_vm.disable_submits(false);
                },
                error: function(res) {
                    console.log('failed patient creation');
                    console.log(res);
                    // TODO add error message somewhere
                    self.patient_details_vm.disable_submits(false);
                }
            });
            return false;
        });

        $add_patient_go.click(function() {
            if (!self.patient_details_vm.disable_submits()) {
                self.patient_details_vm.disable_submits(true);
                $patient_form.submit();
            }
        });

        self.$patient_modal.on('show.bs.modal', function (e) {
            self.clear();
        });

        $('#delete-patient').click(function() {
            self.patient_details_vm.active_object(self.patient());
        });

        self.clear_errors = function() {
            self.first_name_error('');
            self.middle_name_error('');
            self.last_name_error('');
            self.phone_number_error('');
            self.facilities_error('');
            self.allergies_error('');
            self.mrn_error('');
            self.date_birth_error('');
            self.gender_error('');
            self.home_address_error('');
            self.date_deceased_error('');
        };

        self.check_vals = function() {
            if (self.patient_details_vm.patient()) {
                return self.patient_details_vm.patient().id;
            }
        };

        self.clear = function () {
            self.id('');
            self.patient('');
            self.first_name('');
            self.middle_name('');
            self.last_name('');
            self.phone_number('');
            self.facilities([]);
            self.allergies([]);
            self.mrn('');
            self.date_birth(null);
            self.date_birth_verbose('');
            self.gender('');
            self.home_address('');
            self.deceased(false);
            self.date_deceased(null);
            self.date_deceased_verbose('');
            self.clear_errors();
            self.$patient_modal.modal('hide');
        };

        self.set_patient = function(patient) {
            self.id(patient.id);
            self.patient(patient);
            self.first_name(patient.first_name);
            self.middle_name(patient.middle_name);
            self.last_name(patient.last_name);
            self.phone_number(patient.phone_number);
            self.facilities(_.map(patient.facilities, function(f) {
                return f.id.toString();
            }));
            self.allergies(_.map(patient.allergies, function(a) {
                return a.id.toString();
            }));
            self.mrn(patient.mrn);
            self.date_birth(patient.date_birth);
            self.date_birth_verbose(patient.date_birth_verbose);
            self.gender(patient.gender.get_value());
            self.home_address(patient.home_address);
            self.deceased(patient.deceased);
            self.date_deceased(patient.date_deceased);
            self.date_deceased_verbose(patient.date_deceased_verbose);
            $date_birth.change();
            $date_deceased.change();
        }

    }

});