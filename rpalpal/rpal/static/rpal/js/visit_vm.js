define([
    'jquery', 'lodash', 'knockout', 'rpal_models', 'moment', 'autosize', 'select2', 'flatpickr', 'daterangepicker',
    'inputmask', 'async!https://maps.googleapis.com/maps/api/js?key=' + SiteConfig.google_api_key + '&libraries=places'
], function($, _, ko, RPal, moment, autosize) {

    ko.subscribable.fn.subscribeChanged = function(callback) {
        var previousValue;
        this.subscribe(function(_previousValue) {
            previousValue = _previousValue;
        }, undefined, 'beforeChange');
        this.subscribe(function(latestValue) {
            callback(latestValue, previousValue );
        });
    };

    return function (patient_details_vm) {

        var self = this;

        self.patient_details_vm = patient_details_vm;
        patient_details_vm.visit_vm = self;

        self.$visit_modal = $('#visit-modal');

        var $visit_form = $('#visit_form'),
            $add_visit_go = $('#add-visit-go'),
            $coming_from = $('#id_coming_from'),
            $duration_travel = $('#id_duration_travel'),
            $add_visit = $('#add-visit'),
            $visit_details = $('#visit_details'),
            $date = $('#id_date');

        self.id = ko.observable();

        self.modal_open = ko.observable(false);

        self.patient = ko.observable();
        self.visit = ko.observable();

        self.relative_id = ko.observable();
        self.date = ko.observable();
        self.point_of_contact_mode = ko.observable();
        self.point_of_contact_type = ko.observable();
        self.date_verbose = ko.observable();
        self.transport_mode = ko.observable();
        self.transport_mode_base = ko.observable();
        self.location = ko.observable();
        self.location_address = ko.observable();
        self.coming_from = ko.observable();
        self.duration_travel = ko.observable();
        self.duration_travel_verbose = ko.observable();
        self.person_consent = ko.observable();
        self.stage = ko.observable();
        self.code_status = ko.observable();
        self.mental_status = ko.observable();

        self.life_expectancy = ko.observable();
        self.life_expectancy_verbose = ko.observable();
        self.le_years = ko.observable();
        self.le_months = ko.observable();
        self.le_days = ko.observable();

        self.ecog = ko.observable();
        self.pps = ko.observable();
        self.ministry_of_transportation_status = ko.observable();
        self.frailty = ko.observable();
        self.in_out = ko.observable();

        self.is_treatment_decided_on_visit = ko.observable(false);
        self.is_on_chemo = ko.observable(false);
        self.is_first_visit = ko.observable(false);

        self.accompanying_people = ko.observableArray([]);
        self.medications = ko.observableArray([]);
        self.medications_modified = ko.observableArray([]);
        self.services_referred = ko.observableArray([]);

        self.treatment_course_id = ko.observable();
        self.treatment_course_display = ko.observable();
        self.treatment_course_options = ko.observableArray([]);
        self.referral_id = ko.observable();
        self.referral_display = ko.observable();
        self.referral_options = ko.observableArray([]);
        
        self.rad_onc = ko.observable();
        self.rad_onc_object = ko.observable(null);
        self.selectable_rad_oncs = ko.observableArray([]);
        self.active_rad_oncs = ko.observableArray([]);

        self.treatment_course_label = ko.observable('');

        self.date_error = ko.observable('');
        self.point_of_contact_mode_error = ko.observable('');
        self.point_of_contact_type_error = ko.observable('');
        self.coming_from_error = ko.observable('');
        self.duration_travel_error = ko.observable('');
        self.transport_mode_error = ko.observable('');
        self.location_error = ko.observable('');
        self.person_consent_error = ko.observable('');
        self.stage_error = ko.observable('');
        self.code_status_error = ko.observable('');
        self.mental_status_error = ko.observable('');
        self.life_expectancy_error = ko.observable('');
        self.accompanying_people_error = ko.observable('');
        self.medications_error = ko.observable('');
        self.medications_modified_error = ko.observable('');
        self.services_referred_error = ko.observable('');
        self.ecog_error = ko.observable('');
        self.pps_error = ko.observable('');
        self.ministry_of_transportation_status_error = ko.observable('');
        self.treatment_course_error = ko.observable('');
        self.referral_error = ko.observable('');
        self.frailty_error = ko.observable('');
        self.rad_onc_error = ko.observable('');

        self.form_error = ko.observable('');

        self.calulating_travel_time = ko.observable('');

        self.edit_visit = function(vis_vm) {
            self.$visit_modal.modal('show');
            self.set_visit(self.visit(), true);
            self.modal_open(true);
        };

        self.visit_details = function(vis) {
            if (self.visit()) {
                $visit_details.fadeTo('fast', 0.01, function () {
                    // self.patient_careplan_vm.clear();
                    self.set_visit(vis, false);
                    $visit_details.fadeTo('fast', 1);
                });
            } else {
                self.set_visit(vis, false);
            }
        };

        self.coming_from_home = function() {
            self.coming_from(patient_details_vm.patient().home_address);
        };

        ko.computed(function() {
            self.patient(patient_details_vm.patient());
        });

        ko.computed(function() {
            self.date(moment(self.date_verbose(), 'MMM DD YYYY'));
        });

        ko.computed(function() {
            var days = (self.le_years() ? self.le_years() : 0) * 365 + (self.le_months() ? self.le_months() : 0) * 30 + (self.le_days() ? self.le_days() : 0);
            if (days === 0) {
                self.life_expectancy_verbose('');
            } else {
                self.life_expectancy_verbose(moment.duration(days, 'days').humanize());
            }
        });

        self.rad_onc_object.subscribe(function(roo) {
            var sros = [{display: '---------', id: ''}].concat(_.map(
                self.patient_details_vm.active_rad_oncs(), function(aro) { return { display: aro.display, id: aro.id }}
            ));
            if (roo) {
                sros.push({ display: roo.display, id: roo.id });
            }
            self.selectable_rad_oncs(sros);
        });

        self.point_of_contact_type.subscribe(function(v) {
            if (v) {
                self.treatment_course_label(v[0].toUpperCase() + v.substr(1) + ' for treatment course');
                if (v === '' || v === 'consult') {
                    self.treatment_course_id('');
                } else {
                    self.referral_id('');
                }
            } else {
                self.treatment_course_id('');
                self.referral_id('');
            }
        });

        self.point_of_contact_mode.subscribe(function(v) {
            if (v && self.point_of_contact_type()) {
                self.prefill_from_last_visit();
            }
        });

        self.location.subscribe(function(v) {
            if (v) { self.location_address(patient_details_vm.locations[self.location()].address); }
        });

        self.transport_mode.subscribe(function(v) {
            if (v) { self.transport_mode_base(patient_details_vm.transport_modes[self.transport_mode()].base); }
        });

        self.duration_travel.subscribe(function(v) {
            if (v) {
                self.duration_travel_verbose(self.format_duration(self.duration_travel()));
            } else {
                self.duration_travel_verbose('');
            }
        });

        self.location_address.subscribe(function(val) {
            if (val && self.transport_mode() && self.coming_from() && self.modal_open()) {
                self.calulating_travel_time('Calculating...');
                self.calculate_travel_duration();
            }
        });
        self.transport_mode_base.subscribe(function(val) {
            if (val && self.location_address() && self.coming_from() && self.modal_open()) {
                self.calulating_travel_time('Calculating...');
                self.calculate_travel_duration();
            }
        });
        self.coming_from.subscribe(function(val) {
            if (val && self.location_address() && self.transport_mode_base() && self.modal_open()) {
                self.calulating_travel_time('Calculating...');
                self.calculate_travel_duration();
            }
        });

        ko.computed(function() {
            if (self.point_of_contact_mode() === 'telephone') {
                self.coming_from('');
                self.location('');
                self.duration_travel('');
                self.transport_mode('');
            }
            if (self.point_of_contact_mode() !== 'in_person') {
                self.accompanying_people([]);
            }
        });

        self.patient_details_vm.treatment_courses.subscribe(function(tcs) {
            var options = [{display: '---------', id: ''}];
            $.each(tcs, function(i, v) {
                options.push({display: v.get_display(), id: v.id});
            });
            self.treatment_course_options(options);
        });

        self.treatment_course_id.subscribe(function(tc_id) {
            if (tc_id) {
                $.each(self.patient_details_vm.treatment_courses(), function(i, v) {
                    if (v.id === tc_id) {
                        self.treatment_course_display(v.get_display());
                        return true;
                    }
                })
            } else {
                self.treatment_course_display('');
            }
        });

        self.patient_details_vm.referrals.subscribe(function(r) {
            var options = [{display: '---------', id: ''}];
            $.each(r, function(i, v) {
                options.push({display: v.get_display(), id: v.id});
            });
            self.referral_options(options);
        });

        self.referral_id.subscribe(function(r_id) {
            if (r_id) {
                $.each(self.patient_details_vm.referrals(), function(i, v) {
                    if (v.id === r_id) {
                        self.referral_display(v.get_display());
                        return true;
                    }
                })
            } else {
                self.referral_display('');
            }
        });

        self.get_submit_text = ko.computed(function() {
            if (self.patient_details_vm.disable_submits()) {
                return '...'
            }
            return self.id() ? 'Update' : 'Add'
        });

        self.$visit_modal.on('show.bs.modal', function (e) {
            self.patient_details_vm.previous_active_visit(self.patient_details_vm.active_visit());
            self.modal_open(true);
        });

        self.$visit_modal.on('hide.bs.modal', function() {
            if (self.patient_details_vm.previous_active_visit()) {
                $.each(self.patient_details_vm.visits(), function (v) {
                    if (self.patient_details_vm.previous_active_visit().id === v.id) {
                        self.set_visit(self.patient_details_vm.previous_active_visit(), false);
                        return true;
                    }
                })
            }
        });

        $add_visit.click(function() {
            self.clear();
            self.prefill_from_last_visit();
        });

        $date.flatpickr({
            dateFormat: 'M d Y',
            allowInput: true,
            maxDate: moment()._d,
        });

        $duration_travel.inputmask('99:99', {numericInput: true, placeholder: "_", removeMaskOnSubmit: true});

        var coming_from_autocomplete;
        function initAutocomplete() {
            // Create the autocomplete object, restricting the search to geographical
            // location types.
            var center = new google.maps.LatLng(45.4, -75.6);
            var circle = new google.maps.Circle({
                center: center,
                radius: 100000
            });
            coming_from_autocomplete = new google.maps.places.Autocomplete(
                $coming_from[0],
                {
                    types: ['geocode'],
                }
            );
            coming_from_autocomplete.setBounds(circle.getBounds());
            // When the user selects an address from the dropdown, populate the address
            // fields in the form.

            google.maps.event.addListener(coming_from_autocomplete, 'place_changed', function () {
                var new_address = coming_from_autocomplete.getPlace().formatted_address;
                if (new_address !== '') {
                    self.coming_from(coming_from_autocomplete.getPlace().formatted_address);
                    if (self.transport_mode_base() && self.location_address() && self.modal_open()) {
                        self.calulating_travel_time('Calculating...');
                        self.calculate_travel_duration();
                    }
                }
            })
        }

        initAutocomplete();

        var distanceService = new google.maps.DistanceMatrixService();

        self.calculate_travel_duration = function() {
            self.patient_details_vm.disable_submits(true);
            distanceService.getDistanceMatrix({
                origins: [self.coming_from()],
                destinations: [self.location_address()],
                travelMode: patient_details_vm.google_transport_codes[patient_details_vm.transport_modes[self.transport_mode()].base],
                unitSystem: google.maps.UnitSystem.METRIC,
                durationInTraffic: true,
                avoidHighways: false,
                avoidTolls: false,
                drivingOptions: {
                    departureTime: new Date(Date.now()),  // for the time N milliseconds from now.
                    trafficModel: 'pessimistic'
                }
            },
            function (response, status) {
                self.patient_details_vm.disable_submits(false);
                if (status !== google.maps.DistanceMatrixStatus.OK) {
                    self.duration_travel('');
                } else {
                    var element = response.rows[0].elements[0];
                    if (element.status === 'OK') {
                        self.duration_travel(moment.duration(element.duration.value, 'seconds'));
                    } else {
                        self.duration_travel('');
                    }
                }
                self.calulating_travel_time('');
            });
        };

        self.format_duration = function(moment_duration) {
            if (moment_duration.asMilliseconds() === 0) {
                return ''
            }
            var days = moment_duration.days(),
                hours = moment_duration.hours(),
                minutes = moment_duration.minutes();

            hours = hours + days * 24;
            if (hours > 99) {
                return '9999';
            }
            if (hours <= 9) {
                hours = '0' + hours;
            }
            if (minutes <= 9) {
                minutes = '0' + minutes;
            }

            return '' + hours + '' + minutes;
        };

        function serialize_visit_form() {
            var inputs = [
                'csrfmiddlewaretoken=' + $('input[name=csrfmiddlewaretoken]').val(),
                'patient=' + self.patient().id,
                'point_of_contact_mode=' + self.point_of_contact_mode(),
                'point_of_contact_type=' + self.point_of_contact_type(),
                'date=' + self.date().format('DD-MM-YYYY'),
                'transport_mode=' + self.transport_mode(),
                'location=' + self.location(),
                'coming_from=' + self.coming_from(),
                'duration_travel=' + self.duration_travel_verbose(),
                'person_consent=' + self.person_consent(),
                'stage=' + self.stage(),
                'code_status=' + self.code_status(),
                'mental_status=' + self.mental_status(),
                // 'life_expectancy=' + (self.life_expectancy() ? self.life_expectancy().asDays() : ''),
                // 'is_initial_visit=' + self.is_initial_visit(),
                'is_treatment_decided_on_visit=' + self.is_treatment_decided_on_visit(),
                'is_on_chemo=' + self.is_on_chemo(),
                'ecog=' + self.ecog(),
                'pps=' + self.pps(),
                'ministry_of_transportation_status=' + self.ministry_of_transportation_status(),
                'frailty=' + self.frailty(),
                'in_out=' + self.in_out(),
                'treatment_course=' + self.treatment_course_id(),
                'referral=' + self.referral_id(),
                'rad_onc=' + self.rad_onc(),
                'obj_type=visit'
            ];
            $.each(self.accompanying_people(), function(i, v) {
                inputs.push('accompanying_people=' + v);
            });
            $.each(self.medications(), function(i, v) {
                inputs.push('medications=' + v);
            });
            $.each(self.medications_modified(), function(i, v) {
                inputs.push('medications_modified=' + v);
            });
            $.each(self.services_referred(), function(i, v) {
                inputs.push('services_referred=' + v);
            });

            if (self.id()) {
                inputs.push('id=' + self.id());
            }
            var days = (self.le_years() || 0) * 365 + (self.le_months() || 0) * 30 + (self.le_days() || 0);
            inputs.push('life_expectancy=' + days);

            return inputs.join('&');
        }

        $visit_form.submit(function() {

            self.form_error('');

            var url = RPP_URLs.object_create_update_ajax,
                data = serialize_visit_form();

            self.clear_errors();

            $.ajax({
                url: url,
                data: data,
                method: 'POST',
                success: function(res) {
                    if (!res.success) {
                        console.log(res);
                        $.each(res.form_errors, function(k, v) {
                            self[k + '_error'](v);
                        });
                    } else {
                        RPal.api.getVisits(
                            function (visits) {
                                console.log(visits);
                                self.patient_details_vm.visits(visits);
                                self.set_visit(_.find(self.patient_details_vm.visits(), {id: res.object.id}), false);

                                self.$visit_modal.modal('hide');
                            },
                            function(res) {console.log(res);},
                            self.patient_details_vm.patient_id()
                        );
                        // self.set_visit(new_or_edited_visit, false);
                    }
                    self.patient_details_vm.disable_submits(false);
                },
                error: function(res) {
                    console.log('failed point of contact creation');
                    console.log(res);
                    self.form_error('Failed point of contact creation.');
                    self.patient_details_vm.disable_submits(false);
                }
            });
            return false;
        });

        $add_visit_go.click(function() {
            if (!self.patient_details_vm.disable_submits()) {
                self.patient_details_vm.disable_submits(true);
                $visit_form.submit();
            }
        });

        self.clear = function () {
            self.id('');
            self.relative_id('');
            self.visit('');
            self.point_of_contact_mode('');
            self.point_of_contact_type('');
            self.date_verbose(moment().format('MMM DD YYYY'));
            self.transport_mode('');
            self.transport_mode_base('');
            self.location('');
            self.location_address('');
            self.coming_from('');
            self.duration_travel('');
            self.person_consent('');
            self.stage('');
            self.code_status('');
            self.mental_status('');
            self.le_years('');
            self.le_months('');
            self.le_days('');
            self.ecog(null);
            self.pps(null);
            self.ministry_of_transportation_status('');
            self.frailty('');
            self.in_out('');
            self.is_treatment_decided_on_visit(false);
            self.is_on_chemo(false);
            self.is_first_visit(false);
            self.accompanying_people([]);
            self.medications([]);
            self.medications_modified([]);
            self.services_referred([]);
            self.treatment_course_id('');
            self.referral_id('');
            self.rad_onc('');
            self.rad_onc_object(null);
            self.calulating_travel_time('');
            self.clear_errors();
            self.patient_details_vm.active_object('');
            self.patient_details_vm.active_visit('');
            self.$visit_modal.modal('hide');
        };

        self.set_visit = function(visit, open) {
            self.modal_open(open);

            if (self.visit().id !== visit.id) {
                self.patient_details_vm.previous_active_visit(self.visit());
            }

            if (visit) {
                self.visit(visit);
                self.id(visit.id);
                self.relative_id(visit.relative_id);
                self.date_verbose(visit.date_verbose);
                self.coming_from(visit.coming_from);
                if (visit.duration_travel) {
                    self.duration_travel(moment.duration(visit.duration_travel, 'seconds'));
                } else {
                    self.duration_travel('');
                }
                self.is_treatment_decided_on_visit(visit.is_treatment_decided_on_visit);
                self.is_on_chemo(visit.is_on_chemo);
                self.is_first_visit(visit.is_first_visit);
                self.ecog(visit.ecog);
                self.pps(visit.pps);

                self.transport_mode(visit.transport_mode.get_id());
                self.location(visit.location.get_id());
                self.person_consent(visit.person_consent.get_id());
                self.stage(visit.stage.get_value());
                self.code_status(visit.code_status.get_id());
                self.mental_status(visit.mental_status.get_id());
                if (visit.life_expectancy) {
                    var total_days = visit.life_expectancy.asDays(),
                        years = Math.floor(total_days / 365),
                        months = Math.floor((total_days - years * 365) / 30),
                        days = Math.floor((total_days - years * 365 - months * 30));

                    self.le_years(years === 0 ? '' : years);
                    self.le_months(months === 0 ? '' : months);
                    self.le_days(days === 0 ? '' : days);
                }
                self.point_of_contact_mode(visit.point_of_contact_mode.get_value());
                self.point_of_contact_type(visit.point_of_contact_type.get_value());
                self.ministry_of_transportation_status(visit.ministry_of_transportation_status.get_value());
                self.frailty(visit.frailty.get_value());
                self.in_out(visit.in_out.get_value());
                self.treatment_course_id(visit.treatment_course_id);
                self.referral_id(visit.referral_id);
                self.rad_onc_object(visit.rad_onc);

                self.accompanying_people(_.map(visit.accompanying_people, function (ap) {
                    return ap.id.toString();
                }));
                self.medications(_.map(visit.medications, function (m) {
                    return m.id.toString();
                }));
                self.medications_modified(_.map(visit.medications_modified, function (mm) {
                    return mm.id.toString();
                }));
                self.services_referred(_.map(visit.services_referred, function (sr) {
                    return sr.id.toString();
                }));

                self.clear_errors();
                self.patient_details_vm.active_object(self.visit());
                self.patient_details_vm.active_visit(self.visit());
            }
        };

        self.clear_errors = function() {
            self.date_error('');
            self.point_of_contact_mode_error('');
            self.point_of_contact_type_error('');
            self.coming_from_error('');
            self.duration_travel_error('');
            self.transport_mode_error('');
            self.location_error('');
            self.person_consent_error('');
            self.stage_error('');
            self.code_status_error('');
            self.mental_status_error('');
            self.life_expectancy_error('');
            self.accompanying_people_error('');
            self.medications_error('');
            self.medications_modified_error('');
            self.services_referred_error('');
            self.ecog_error('');
            self.pps_error('');
            self.ministry_of_transportation_status_error('');
            self.frailty_error('');
            self.treatment_course_error('');
            self.referral_error('');
            self.form_error('');
            self.rad_onc_error('');
        };

        self.prefill_from_last_visit = function() {
            var visits_length = self.patient_details_vm.visits().length;
            if (visits_length > 0) {
                var visit = self.patient_details_vm.visits()[visits_length - 1];
                self.transport_mode(visit.transport_mode.get_id());
                self.location(visit.location.get_id());
                self.coming_from(visit.coming_from);
                self.duration_travel(visit.duration_travel);
                self.frailty(visit.frailty.get_value());
                self.in_out(visit.in_out.get_value());
                self.is_on_chemo(visit.is_on_chemo);
                self.is_first_visit(false);
                self.stage(visit.stage.get_value());
                self.code_status(visit.code_status.get_id());
                self.mental_status(visit.mental_status.get_id());
                if (visit.life_expectancy) {
                    var total_days = visit.life_expectancy.asDays(),
                        years = Math.floor(total_days / 365),
                        months = Math.floor((total_days - years * 365) / 30),
                        days = Math.floor((total_days - years * 365 - months * 30));

                    self.le_years(years === 0 ? '' : years);
                    self.le_months(months === 0 ? '' : months);
                    self.le_days(days === 0 ? '' : days);
                }
                self.medications(_.map(visit.medications.concat(visit.medications_modified), function (m) {
                    return m.id.toString();
                }));
                self.services_referred(_.map(visit.services_referred, function (sr) {
                    return sr.id.toString();
                }));
                self.ecog(visit.ecog);
                self.pps(visit.pps);
                self.ministry_of_transportation_status(visit.ministry_of_transportation_status.get_value());
            }
        }

    };

});