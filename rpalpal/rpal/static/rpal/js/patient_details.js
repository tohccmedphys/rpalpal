require(['jquery', 'knockout', 'moment', 'base', 'dateline', 'select2'], function($, ko, moment) {

    ko.bindingHandlers.invisible = {
        init: function(element, valueAccessor) {
            var value = ko.utils.unwrapObservable(valueAccessor());
            if (!value || typeof value == 'undefined') {
                $(element).addClass('invisible');
            } else {
                $(element).addClass('visible');
            }
        },
        update: function(element, valueAccessor) {
            var value = ko.utils.unwrapObservable(valueAccessor());
            // Here we just update the "disabled" state, but you could update other properties too
            if (!value || typeof value == 'undefined') {
                $(element).removeClass('visible');
                $(element).addClass('invisible');
            } else {
                $(element).removeClass('invisible');
                $(element).addClass('visible');
            }

        }
    };

    ko.utils.setValue = function (property, newValue) {
        if (ko.isObservable(property))
            property(newValue);
        else
            property = newValue;
    };

    ko.observableArray.fn.refresh_item = function (item) {
        var index = this['indexOf'](item);
        if (index >= 0) {
            this.splice(index, 1);
            this.splice(index, 0, item);
        }
    };

    ko.bindingHandlers.daterangepicker_mcs = {

        init: function(el, valueAccessor, allBindings, data, context) {
            $(el).flatpickr({
                dateFormat: 'M d Y',
                allowInput: true,
                maxDate: moment()._d,
                onChange: function(chosen_dates) {
                    var moment_date = moment(chosen_dates[0]);
                    data.date(moment_date);
                    var formatted_date = moment_date.format('MMM DD YYYY');
                    data.date_verbose(formatted_date);
                    $(this.element).val(formatted_date);
                }
            });
            $(el).removeClass('not-dated');
        }
        // $date.flatpickr({
        //     dateFormat: 'M d Y',
        //     allowInput: true,
        //     maxDate: moment()._d,
        // });

    };

    ko.bindingHandlers.select2 = {
        init: function (el, valueAccessor, allBindingsAccessor, viewModel) {
            ko.utils.domNodeDisposal.addDisposeCallback(el, function () {
                $(el).select2('destroy');
            });

            var allBindings = allBindingsAccessor(),
                select2 = ko.utils.unwrapObservable(allBindings.select2);
            select2.dropdownParent = $(el).parent();
            if ($(el).prop('multiple')) {
                $(el).select2(select2);
            } else {
                $(el).select2(select2).overrideSelect2Keys();
            }
        },
        update: function (el, valueAccessor, allBindingsAccessor, viewModel) {
            var allBindings = allBindingsAccessor();
            if ("value" in allBindings) {
                if ((allBindings.select2.multiple || el.multiple) && allBindings.value().constructor != Array) {
                    $(el).val(allBindings.value().split(',')).trigger('change');
                }
                else {
                    $(el).val(allBindings.value()).trigger('change');
                }
            } else if ("selectedOptions" in allBindings) {
                var converted = [];
                var textAccessor = function (value) {
                    return value;
                };
                if ("optionsText" in allBindings) {
                    textAccessor = function (value) {
                        var valueAccessor = function (item) {
                            return item;
                        };
                        if ("optionsValue" in allBindings) {
                            valueAccessor = function (item) {
                                return item[allBindings.optionsValue];
                            }
                        }
                        var items = $.grep(allBindings.options(), function (e) {
                            return valueAccessor(e) == value
                        });
                        if (items.length == 0 || items.length > 1) {
                            return "UNKNOWN";
                        }
                        return items[0][allBindings.optionsText];
                    }
                }
                $.each(allBindings.selectedOptions(), function (key, value) {
                    converted.push({id: value, text: textAccessor(value)});
                });
                $(el).select2("data", converted);
            }
            $(el).trigger("change");
        }
    };

    $(document).ready(function () {

        require([
            'patient_details_vm',
            'referral_vm',
            'diagnosis_vm',
            'visit_vm',
            'treatment_course_vm',
            'patient_vm',
            'physician_vm'
        ], function (
            PatientDetailsVM, ReferralVM, DiagnosisVM, VisitVM, TreatmentCourseVM, PatientVM, PhysicianVM
        ) {
            var patient_details_vm = new PatientDetailsVM(),
                patient_vm = new PatientVM(patient_details_vm),
                referral_vm = new ReferralVM(patient_details_vm),
                physician_vm = new PhysicianVM(referral_vm),
                diagnosis_vm = new DiagnosisVM(patient_details_vm),
                visit_vm = new VisitVM(patient_details_vm),
                treatment_course_vm = new TreatmentCourseVM(patient_details_vm);
            ko.applyBindings(patient_details_vm, document.getElementById('patient-details'));
            ko.applyBindings(referral_vm, document.getElementById('referral-modal'));
            ko.applyBindings(diagnosis_vm, document.getElementById('diagnosis-modal'));
            ko.applyBindings(visit_vm, document.getElementById('visit-modal'));
            ko.applyBindings(treatment_course_vm, document.getElementById('treatment-course-modal'));
            ko.applyBindings(patient_vm, document.getElementById('patient-modal'));
            ko.applyBindings(physician_vm, document.getElementById('physician-modal'));
        });

        var $patient_selector = $('#patient-selector');

        /* patient selector ------------------------------------------------------------------ */
        $patient_selector.select2({
            ajax: {
                url: RPP_URLs.patients_mrn_searcher,
                dataType: 'json',
                delay: '500',
                data: function (params) {
                    return {
                        q: params.term, // search term
                        page: params.page
                    }
                },
                processResults: function (data) {
                    return {
                        results: $.map(data.patients, function (item) {
                            return {
                                text: item.mrn + ' - ' + item.first_name + ' ' + item.last_name,
                                id: item.id
                            }
                        })
                    };
                },
                cache: true
            },
            width: '100%',
            minimumInputLength: 1,
            placeholder: 'Patient MRN'
        });

        if (patient) {
            $patient_selector.append('<option value="' + patient.id + '">' + patient.mrn + ' - ' + patient.first_name + ' ' + patient.last_name + '</option>')
        }

    });
});