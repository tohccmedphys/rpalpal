define(['jquery', 'knockout', 'lodash', 'moment'], function($, ko, _, moment) {

    var RPal = RPal || {};

    var inheritsFrom = function (child, parent) {
        child.prototype = Object.create(parent.prototype);
    };

    RPal.models = (function () {

        var models = {};

        models.SimpleModel = function(data) {
            _.extend(this, data);
        };
        models.SimpleModel.prototype.get_name = function() { return this.name; };
        models.SimpleModel.prototype.get_value = function() { return this.value; };
        models.SimpleModel.prototype.get_id = function() { return this.id; };

        models.Patient = function(data) {
            _.extend(this, data);
            this.class = 'patient';
            this.date_birth = moment(data.date_birth, 'DD-MM-YYYY');
            this.date_deceased = moment(data.date_deceased, 'DD-MM-YYYY');
            if (data.deceased) {
                this.age = moment(this.date_deceased).diff(this.date_birth, 'years')
            } else {
                this.age = moment().diff(this.date_birth, 'years');
            }
            this.date_birth_verbose = data.date_birth;
            this.date_deceased_verbose = data.date_deceased;

            this.facilities = _.map(data.facilities, function(a) {
                return new models.SimpleModel(a);
            });
            this.allergies = _.map(data.allergies, function(f) {
                return new models.SimpleModel(f);
            });

            this.phone_number = this.phone_number ? this.phone_number : '';
            this.gender = new models.SimpleModel(data.gender);

            this.referrals = [];
            this.diagnoses = [];
            this.visits = [];
            this.treatment_courses = [];

            this.display = this.first_name + ' ' + (this.middle_name ? this.middle_name + ' ' : '') + this.last_name;
        };
        models.Patient.prototype.get_referral = function(r_id) {
            return _.find(this.referrals, function(r) { return r.id === r_id; })
        };
        models.Patient.prototype.get_diagnosis = function(d_id) {
            return _.find(this.diagnoses, function(d) { return d.id === d_id; })
        };
        models.Patient.prototype.get_visit = function(v_id) {
            return _.find(this.visits, function(v) { return v.id === v_id; })
        };
        models.Patient.prototype.get_careplan = function(c_id) {
            return _.find(this.patient_careplans, function(c) { return c.id === c_id; })
        };
        models.Patient.prototype.get_multi_names = function(field) {
            return _.map(this[field], function(f) { return f.name.toString(); }).join(' | ')
        };

        models.Referral = function (data) {
            this.class = 'referral';
            this.id = data.id;
            this.relative_id = data.relative_id;
            this.patient_id = data.patient_id;
            this.referring_communication_mode = new models.SimpleModel(data.referring_communication_mode);
            this.date_referred = data.date_referred;
            this.date_referred_verbose = moment(data.date_referred, 'YYYY-MM-DD').format('MMM DD YYYY');
            this.referral_reach = new models.SimpleModel(data.referral_reach);
            this.referral_reasons = _.map(data.referral_reasons, function(rr) {
                return new models.SimpleModel(rr);
            });
            this.referral_location = new models.SimpleModel(data.referral_location);
            this.referring_physician = new models.Physician(data.referring_physician);
        };

        models.Referral.prototype.set_patient = function(patient) {
            this.patient = patient;
        };
        models.Referral.prototype.get_display = function() {
            return '' + this.relative_id + ': ' + this.date_referred_verbose + ', ' + this.referring_physician.display;
        };
        models.Referral.prototype.get_referral_reasons_verbose = function() {
            return _.map(this.referral_reasons, function(rr) {
                return rr.name;
            }).join(' | ')
        };

        models.Physician = function(data) {
            this.class = 'physician';
            this.id = data.id;
            this.first_name = data.first_name;
            this.last_name = data.last_name;
            this.contact_info = {};
            this.contact_info.email = data.contact_info.email ? data.contact_info.email : '';
            this.contact_info.phone_number = data.contact_info.phone_number ? data.contact_info.phone_number : '';
            this.contact_info.text_number = data.contact_info.text_number ? data.contact_info.text_number : '';
            this.contact_info.fax_number = data.contact_info.fax_number ? data.contact_info.fax_number : '';
            this.active = data.active;

            this.physician_specialty = new models.SimpleModel(data.physician_specialty);

            this.display = '' + this.first_name + ' ' + this.last_name;
        };

        models.RadOnc = function(data) {
            this.class = 'rad_onc';
            this.id = data.id;
            this.first_name = data.first_name;
            this.last_name = data.last_name;
            this.email = data.email;
            this.phone_number = data.phone_number;
            this.active = data.active;
            this.contact_display = '';
            if (this.email || this.phone_number){
                if (this.email && this.phone_number){
                    this.contact_display = '(' + this.email + ', ' + this.phone_number + ')';
                } else if (this.email) {
                    this.contact_display = '(' + this.email + ')';
                } else {
                    this.contact_display = '(' + this.phone_number + ')';
                }
            }

            this.display = '' + (this.first_name ? this.first_name : '') + ' ' + (this.last_name ? this.last_name : '');
        };

        models.Diagnosis = function(data) {
            this.class = 'diagnosis';
            this.id = data.id;
            this.relative_id = data.relative_id;
            this.patient_id = data.patient.id;
            this.primary_cancer_site = new models.SimpleModel(data.primary_cancer_site);
            this.primary_diagnosis_group = new models.SimpleModel(data.primary_diagnosis_group);

            this.pathologic_diagnosis = data.pathologic_diagnosis;
            this.date_diagnosis = data.date_diagnosis ? moment(data.date_diagnosis, 'YYYY-MM-DD') : '';
            this.date_diagnosis_verbose = this.date_diagnosis ? this.date_diagnosis.format('MMM DD YYYY') : '';
            this.radiographic_diagnosis_explanation = data.radiographic_diagnosis_explanation;
            this.primary_cancer_dx_description = data.primary_cancer_dx_description;

            this.cancer_sites_metastatic = _.map(data.cancer_sites_metastatic, function(csm) {
                return csm;
            });
        };
        models.Diagnosis.prototype.cancer_sites_metastatic_verbose = function() {
            return _.map(this.cancer_sites_metastatic, function(csm) {
                return csm.cancer_site_metastatic_name;
            }).join(' | ')
        };
        models.Diagnosis.prototype.set_patient = function(patient) {
            this.patient = patient;
        };

        models.Visit = function(data) {
            this.class = 'visit';
            this.id = data.id;
            this.relative_id = data.relative_id;
            this.date = moment(data.date, 'YYYY-MM-DD');
            this.date_verbose = this.date.format('MMM DD YYYY');
            this.coming_from = data.coming_from;
            this.coming_from_lat_lng = data.coming_from_lat_lng;
            this.duration_travel = data.duration_travel ? moment.duration(data.duration_travel, 'seconds') : '';
            this.duration_travel_verbose = this.duration_travel ? this.duration_travel.hours() + ' hours, ' + this.duration_travel.minutes() + ' minutes' : '';
            // this.is_initial_visit = data.is_initial_visit;
            this.is_treatment_decided_on_visit = data.is_treatment_decided_on_visit;
            this.is_on_chemo = data.is_on_chemo;
            this.is_first_visit = data.is_first_visit;
            this.pps = data.pps !== null ? data.pps.toString() : '';
            this.ecog = data.ecog !== null ? data.ecog.toString() : '';
            this.life_expectancy = moment.duration(data.life_expectancy, 'days');
            if (data.life_expectancy) {
                this.life_expectancy_verbose = moment.duration(data.life_expectancy, 'days').humanize();
            } else {
                this.life_expectancy_verbose = '';
            }
            this.point_of_contact_mode = new models.SimpleModel(data.point_of_contact_mode);
            this.point_of_contact_type = new models.SimpleModel(data.point_of_contact_type);
            this.transport_mode = new models.SimpleModel(data.transport_mode);
            this.person_consent = new models.SimpleModel(data.person_consent);
            this.location = new models.SimpleModel(data.location);
            this.stage = new models.SimpleModel(data.stage);
            this.code_status = new models.SimpleModel(data.code_status);
            this.mental_status = new models.SimpleModel(data.mental_status);
            this.ministry_of_transportation_status = new models.SimpleModel(data.ministry_of_transportation_status);
            this.frailty = new models.SimpleModel(data.frailty);
            this.in_out = new models.SimpleModel(data.in_out);
            this.rad_onc = new models.RadOnc(data.rad_onc);

            this.treatment_course_id = data.treatment_course_id;
            this.referral_id = data.referral_id;

            this.medications = _.map(data.medications, function (m) { return new models.SimpleModel(m); });
            this.medications_modified = _.map(data.medications_modified, function (mm) { return new models.SimpleModel(mm); });
            this.services_referred = _.map(data.services_referred, function (sr) { return new models.SimpleModel(sr); });
            this.accompanying_people = _.map(data.accompanying_people, function (ap) { return new models.SimpleModel(ap); });
        };
        models.Visit.prototype.set_patient = function(patient) {
            this.patient = patient;
        };
        models.Visit.prototype.get_multi_names = function(field) {
            return _.map(this[field], function(f) { return f.name.toString(); }).join(' | ')
        };
        models.Visit.prototype.get_display = function() {
            return '' + this.relative_id + ': ' + this.point_of_contact_type.get_name() + ' - ' + this.point_of_contact_mode.get_name() + ' - ' + this.date_verbose;
        };

        models.TreatmentCourse = function(data) {
            this.class = 'treatment_course';
            this.id = data.id;
            this.relative_id = data.relative_id;
            this.dose_override = data.dose_override;
            this.fractions_override = data.fractions_override;

            this.type_override = new models.SimpleModel(data.type_override);
            this.complexity_override = new models.SimpleModel(data.complexity_override);
            this.pattern_override = new models.SimpleModel(data.pattern_override);

            this.visit_id = data.visit ? data.visit.id : null;
            this.care_plan = new models.SimpleModel(data.care_plan);
            this.care_plan_num_fields = new models.SimpleModel(data.care_plan_num_fields);
            // this.bone_mets_complexity = new models.SimpleModel(data.bone_mets_complexity);

            this.bone_mets_complexity = _.map(data.bone_mets_complexity, function(bmc) { return new models.SimpleModel(bmc);});
            this.structure_treated = new models.SimpleModel(data.structure_treated);

            this.date_end = moment(data.date_end, 'YYYY-MM-DD');
            this.date_end_verbose = this.date_end.format('MMM DD YYYY');
            this.date_start = moment(data.date_start, 'YYYY-MM-DD');
            this.date_start_verbose = this.date_start.format('MMM DD YYYY');
            this.ct_date = data.ct_date ? moment(data.ct_date, 'YYYY-MM-DD') : '';
            this.ct_date_verbose = data.ct_date ? this.ct_date.format('MMM DD YYYY') : '';
            this.date_range_treatment = this.date_start.format('MMM DD YYYY') + ' - ' + this.date_end.format('MMM DD YYYY');
            this.is_prescribed_course_complete = data.is_prescribed_course_complete;
            this.same_day_start_date = data.same_day_start_date;
            this.bone_mets_involved = data.bone_mets_involved;
            this.explanation = data.explanation;
            this.coming_from = data.coming_from;
            this.coming_from_lat_lng = data.coming_from_lat_lng;
            this.duration_travel = data.duration_travel ? moment.duration(data.duration_travel, 'seconds') : null;

            this.transport_mode = new models.SimpleModel(data.transport_mode);
            this.location = new models.SimpleModel(data.location);

        };
        models.TreatmentCourse.prototype.get_multi_names = function(field) {
            return _.map(this[field], function(f) { return f.name.toString(); }).join(' | ')
        };
        models.TreatmentCourse.prototype.get_display = function() {
            return '' + this.relative_id + ' - ' + this.date_start_verbose + ' - ' + this.structure_treated.get_name();
        };

        return models;

    })();

    RPal.api = (function () {

        var api = {};

        api.getPatient = function (success, error, patient_id) {

            var url = RPP_URLs.patients_searcher;

            $.ajax({
                url: url,
                data: {patient_id: patient_id},
                success: function(data) {
                    var patient = new RPal.models.Patient(data.patient);
                    success(patient);
                },
                error: error
            })
        };

        api.getReferrals = function (success, error, patient_id) {

            var url = RPP_URLs.referrals_searcher;

            $.ajax({
                url: url,
                data: {patient_id: patient_id},
                success: function (data) {
                    var referrals = _.map(data.referrals, function (r) {
                        return new RPal.models.Referral(r);
                    });
                    success(referrals);
                },
                error: error
            });
        };

        api.getDiagnoses = function (success, error, patient_id) {

            var url = RPP_URLs.diagnoses_searcher;

            $.ajax({
                url: url,
                data: {patient_id: patient_id},
                success: function (data) {
                    var diagnoses = _.map(data.diagnoses, function (d) {
                        return new RPal.models.Diagnosis(d);
                    });
                    success(diagnoses);
                },
                error: error
            });
        };

        api.getVisits = function (success, error, patient_id) {

            var url = RPP_URLs.visits_searcher;

            $.ajax({
                url: url,
                data: {patient_id: patient_id},
                success: function (data) {
                    var visits = _.map(data.visits, function (v) {
                        return new RPal.models.Visit(v);
                    });
                    success(visits);
                },
                error: error
            });
        };

        api.getTreatmentCourses = function (success, error, patient_id) {

            var url = RPP_URLs.all_treatment_courses_searcher;

            $.ajax({
                url: url,
                data: {patient_id: patient_id},
                success: function (data) {
                    var treatment_courses = _.map(data.all_treatment_courses, function (tc) {
                        return new RPal.models.TreatmentCourse(tc);
                    });
                    success(treatment_courses);
                },
                error: error
            });
        };

        api.getActivePhysicians = function (success, error) {

            var url = RPP_URLs.active_physicians_searcher;

            $.ajax({
                url: url,
                success: function (data) {
                    var active_physicians = _.map(data.active_physicians, function (ap) {
                        return new RPal.models.Physician(ap);
                    });
                    success(active_physicians);
                },
                error: error
            });
        };

        api.getActiveRadOncs = function (success, error) {

            var url = RPP_URLs.active_rad_oncs_searcher;

            $.ajax({
                url: url,
                success: function (data) {
                    var active_rad_oncs = _.map(data.active_rad_oncs, function (aro) {
                        return new RPal.models.RadOnc(aro);
                    });
                    success(active_rad_oncs);
                },
                error: error
            });
        };

        api.getMetastaticCancerSites = function(success, error) {
            var url = RPP_URLs.metastatic_cancer_sites_searcher;

            $.ajax({
                url: url,
                success: success
            })
        };

        api.deleteObject = function(success, error, obj_type, obj_id) {
            var url = RPP_URLs.delete_object_ajax,
                data = {
                    'obj_type': obj_type,
                    'id': obj_id,
                    'csrfmiddlewaretoken': $('input[name=csrfmiddlewaretoken]').val()
                };

            $.ajax({
                method: 'POST',
                url: url,
                data: data,
                success: success,
                error: error
            })
        };


        return api;

    })();

    return RPal;

});