define(['jquery', 'lodash', 'knockout', 'rpal_models', 'moment', 'select2'], function($, _, ko, RPal, moment) {

    return function (referral_vm) {

        var self = this;

        self.referral_vm = referral_vm;
        referral_vm.physician_vm = self;

        var $form = $('#physician_form'),
            $add_go = $('#add-physician-go');

        self.$modal = $('#physician-modal');

        self.id = ko.observable();

        self.first_name = ko.observable();
        self.last_name = ko.observable();
        self.email = ko.observable();
        self.phone_number = ko.observable('');
        self.physician_specialty = ko.observable(null);
        self.text_number = ko.observable('');
        self.fax_number = ko.observable('');
        self.active = ko.observable(true);

        self.first_name_error = ko.observable('');
        self.last_name_error = ko.observable('');
        self.email_error = ko.observable('');
        self.phone_number_error = ko.observable('');
        self.physician_specialty_error = ko.observable('');
        self.text_number_error = ko.observable('');
        self.fax_number_error = ko.observable('');

        self.form_error = ko.observable('');

        self.get_submit_text = ko.computed(function() {
            return self.id() ? 'Update' : 'Add'
        });

        function serialize_form() {

            var inputs = [
                'csrfmiddlewaretoken=' + $('input[name=csrfmiddlewaretoken]').val(),

                'first_name=' + self.first_name(),
                'last_name=' + self.last_name(),
                'email=' + self.email(),
                'phone_number=' + self.phone_number(),
                'physician_specialty=' + self.physician_specialty(),
                'text_number=' + self.text_number(),
                'fax_number=' + self.fax_number(),
                'active=' + self.active(),
                'obj_type=physician'
            ];

            if (self.id()) {
                inputs.push('id=' + self.id());
            }
            return inputs.join('&');
        }

        self.search_and_set_physician = function(id) {
            $.ajax({
                url: RPP_URLs.physician_searcher,
                data: {'id': id},
                success: function(res) {
                    self.set_physician(new RPal.models.Physician(res.physician));
                },
                error: function(res) {
                    console.log(res);
                }
            })
        };

        $form.submit(function() {

            var url = RPP_URLs.object_create_update_ajax,
                data = serialize_form();

            self.clear_errors();

            $.ajax({
                url: url,
                data: data,
                method: 'POST',
                success: function(res) {
                    if (!res.success) {
                        $.each(res.form_errors, function (k, v) {
                            self[k + '_error'](v);
                        });
                    } else {
                        var physician = new RPal.models.Physician(res.object);
                        if (res.is_new) {
                            self.referral_vm.$physician.append('<option value="' + physician.id + '">' + physician.first_name + ' ' + physician.last_name + '</option>');
                            self.referral_vm.$physician.val(physician.id).change();
                        } else {
                            $.each(referral_vm.patient_details_vm.referrals(), function(i, v) {
                                if (v.referring_physician.id === physician.id) {
                                    v.referring_physician = physician;
                                    referral_vm.patient_details_vm.referrals.refresh_item(v);
                                }
                            });
                            referral_vm.physician_object(physician);
                            referral_vm.referring_physician(physician.id);

                            RPal.api.getActivePhysicians(
                                function (active_physicians) {
                                    self.referral_vm.patient_details_vm.active_physicians(active_physicians);
                                },
                                self.referral_vm.patient_details_vm.error
                            );
                        }
                        self.$modal.modal('hide');
                        self.clear();
                    }
                    self.referral_vm.patient_details_vm.disable_submits(false);
                },
                error: function(res) {
                    console.log('failed physician creation');
                    console.log(res);
                    // TODO add error message somewhere
                    self.form_error('Failed physician creation.');
                    self.referral_vm.patient_details_vm.disable_submits(false);
                }
            });
            return false;
        });

        $add_go.click(function() {
            if (!self.referral_vm.patient_details_vm.disable_submits()) {
                self.referral_vm.patient_details_vm.disable_submits(true);
                $form.submit();
            }
        });

        self.clear_errors = function() {
            self.first_name_error('');
            self.last_name_error('');
            self.email_error('');
            self.phone_number_error('');
            self.physician_specialty_error('');
            self.text_number_error('');
            self.fax_number_error('');
            self.form_error('');
        };

        self.clear = function () {

            self.id('');

            self.first_name('');
            self.last_name('');
            self.email('');
            self.phone_number('');
            self.physician_specialty(null);
            self.text_number('');
            self.fax_number('');
            self.active(true);

            self.clear_errors();

        };

        self.set_physician = function(physician) {
            self.id(physician.id);

            self.first_name(physician.first_name);
            self.last_name(physician.last_name);
            self.email(physician.contact_info.email);
            self.phone_number(physician.contact_info.phone_number);
            self.physician_specialty(physician.physician_specialty.get_id());
            self.text_number(physician.contact_info.text_number);
            self.fax_number(physician.contact_info.fax_number);
            self.active(physician.active);

        }

    }

});