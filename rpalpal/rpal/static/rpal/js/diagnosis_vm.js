define(['jquery', 'lodash', 'knockout', 'rpal_models', 'moment', 'autosize', 'select2'], function($, _, ko, RPal, moment, autosize) {

    return function (patient_details_vm) {

        var self = this;

        self.patient_details_vm = patient_details_vm;
        patient_details_vm.diagnosis_vm = self;

        var $diagnosis_form = $('#diagnosis_form'),
            $add_diagnosis_go = $('#add-diagnosis-go'),
            $date_diagnosis = $('#id_date_diagnosis');

        self.$diagnosis_modal = $('#diagnosis-modal');

        // self.diagnosis = ko.observable();

        self.id = ko.observable();
        self.relative_id = ko.observable();
        self.diagnosis = ko.observable();
        self.patient = ko.observable();
        // self.patient_id = ko.observable();
        self.primary_cancer_site = ko.observable();
        self.primary_diagnosis_group = ko.observable();
        self.cancer_sites_metastatic = ko.observableArray([]);
        self.pathologic_diagnosis = ko.observable(true);
        self.date_diagnosis = ko.observable();
        self.date_diagnosis_verbose = ko.observable();
        self.radiographic_diagnosis_explanation = ko.observable();
        self.primary_cancer_dx_description = ko.observable();
        self.datetime_created = ko.observable();

        self.primary_cancer_site_error = ko.observable();
        self.primary_diagnosis_group_error = ko.observable();
        self.cancer_sites_metastatic_error = ko.observable();
        self.date_diagnosis_error = ko.observable();
        self.radiographic_diagnosis_explanation_error = ko.observable();
        self.primary_cancer_dx_description_error = ko.observable();
        self.form_error = ko.observable();

        self.cancer_sites_metastatic_TOTAL_FORMS = ko.observable(0);
        self.cancer_sites_metastatic_INITIAL_FORMS = ko.observable(0);
        self.cancer_sites_metastatic_MIN_NUM_FORMS = ko.observable(0);
        self.cancer_sites_metastatic_MAX_NUM_FORMS = ko.observable(100);

        self.cancer_sites_metastatic.subscribe(function(v) {
            self.cancer_sites_metastatic_TOTAL_FORMS(v.length);
        });
        self.cancer_sites_metastatic_options = ko.observableArray([]);

        ko.computed(function() {
            self.patient(patient_details_vm.patient());
        });

        ko.computed(function() {
            self.date_diagnosis(moment(self.date_diagnosis_verbose(), 'MMM DD YYYY'));
        });

        ko.computed(function() {
            if (self.pathologic_diagnosis()) {
                self.radiographic_diagnosis_explanation('');
            }
        });

        self.get_submit_text = ko.computed(function() {
            return self.id() ? 'Update' : 'Add'
        });

        autosize($('textarea.autosize'));

        $date_diagnosis.flatpickr({
            dateFormat: 'M d Y',
            allowInput: true,
            maxDate: moment()._d
        });

        function serialize_diagnosis_form() {

            var inputs = [
                'csrfmiddlewaretoken=' + $('input[name=csrfmiddlewaretoken]').val(),
                'patient=' + self.patient().id,
                'pathologic_diagnosis=' + self.pathologic_diagnosis(),
                'primary_cancer_site=' + self.primary_cancer_site(),
                'primary_diagnosis_group=' + self.primary_diagnosis_group(),
                'radiographic_diagnosis_explanation=' + self.radiographic_diagnosis_explanation(),
                'primary_cancer_dx_description=' + self.primary_cancer_dx_description(),
                'metastaticcancersitediagnosiscollection_set-TOTAL_FORMS=' + self.cancer_sites_metastatic_TOTAL_FORMS(),
                'metastaticcancersitediagnosiscollection_set-INITIAL_FORMS=' + self.cancer_sites_metastatic_INITIAL_FORMS(),
                'metastaticcancersitediagnosiscollection_set-MIN_NUM_FORMS=' + self.cancer_sites_metastatic_MIN_NUM_FORMS(),
                'metastaticcancersitediagnosiscollection_set-MAX_NUM_FORMS=' + self.cancer_sites_metastatic_MAX_NUM_FORMS(),
                'obj_type=diagnosis'
            ];
            $.each(self.cancer_sites_metastatic(), function(i, v) {

                if (v.cancer_site_metastatic_id()) {
                    inputs.push('metastaticcancersitediagnosiscollection_set-' + v.__prefix__ + '-metastatic_cancer_site=' + v.cancer_site_metastatic_id());
                }
                if (v.date_verbose()) {
                    inputs.push('metastaticcancersitediagnosiscollection_set-' + v.__prefix__ + '-date=' + v.date().format('DD-MM-YYYY'));
                }
                if (v.id()) {
                    inputs.push('metastaticcancersitediagnosiscollection_set-' + v.__prefix__ + '-id=' + v.id());
                }
                if (v.DELETE()) {
                    inputs.push('metastaticcancersitediagnosiscollection_set-' + v.__prefix__ + '-DELETE=' + v.DELETE());
                }
                if (self.id()) {
                    inputs.push('metastaticcancersitediagnosiscollection_set-' + v.__prefix__ + '-diagnosis=' + self.id());
                }
            });

            if (self.date_diagnosis()) {
                inputs.push('date_diagnosis=' + self.date_diagnosis().format('DD-MM-YYYY'));
            }

            if (self.id()) {
                inputs.push('id=' + self.id());
            }

            return inputs.join('&');
        }

        self.add_empty_metastatic_cancer_site_form = function() {

            var id_obs = ko.observable(),
                date_obs = ko.observable(),
                date_verbose_obs = ko.observable(),
                cancer_site_metastatic_id_obs = ko.observable(),
                delete_obs = ko.observable(false);

            self.cancer_sites_metastatic.push({
                id: id_obs,
                date: date_obs,
                date_verbose: date_verbose_obs,
                cancer_site_metastatic_id: cancer_site_metastatic_id_obs,
                cancer_site_metastatic_name: '---------',
                DELETE: delete_obs,
                __prefix__: self.cancer_sites_metastatic_TOTAL_FORMS(),
                id_error: ko.observable(''),
                date_error: ko.observable(''),
                metastatic_cancer_site_error: ko.observable('')
            });

            $('.csm-select2.not-select2').select2({
                autoclose: true, width: '100%', minimumResultsForSearch: 10
            }).removeClass('not-select2');

        };

        $diagnosis_form.submit(function() {

            var url = RPP_URLs.diagnosis_create_update_ajax,
                data = serialize_diagnosis_form();

            // $('.diagnosis-error').remove();
            $('.has-error').removeClass('has-error');

            $.ajax({
                url: url,
                data: data,
                method: 'POST',
                success: function(res) {
                    if (!res.success) {
                        $.each(res.form_errors, function(k, v) {
                            self[k + '_error'](v);
                        });
                        $.each(res.formset_errors, function(i, v1) {
                            $.each(v1, function(k, v2) {
                                self.cancer_sites_metastatic()[i][k + '_error'](v2);
                            })
                        })
                    } else {
                        RPal.api.getDiagnoses(
                            function (diagnoses) {
                                console.log(diagnoses);
                                self.patient_details_vm.diagnoses(diagnoses);
                                self.clear();
                                self.$diagnosis_modal.modal('hide');
                            },
                            function(res) {console.log(res);},
                            self.patient_details_vm.patient_id()
                        );
                    }
                    self.patient_details_vm.disable_submits(false);
                },
                error: function(res) {
                    console.log('failed diagnosis creation');
                    console.log(res);
                    self.form_error('Failed diagnosis creation.');
                    // TODO add error message somewhere
                    self.patient_details_vm.disable_submits(false);
                }
            });
            return false;
        });

        $add_diagnosis_go.click(function() {
            if (!self.patient_details_vm.disable_submits()) {
                self.patient_details_vm.disable_submits(true);
                $diagnosis_form.submit();
            }
        });

        self.$diagnosis_modal.on('show.bs.modal', function (e) {
            self.clear();
        });

        self.clear = function () {

            self.id('');
            self.relative_id('');
            self.diagnosis('');

            self.primary_cancer_site('');
            self.primary_diagnosis_group('');
            self.primary_cancer_dx_description('');
            self.cancer_sites_metastatic([]);
            self.pathologic_diagnosis(true);
            self.date_diagnosis('');
            self.date_diagnosis_verbose('');
            self.radiographic_diagnosis_explanation('');
            self.datetime_created('');

            self.patient_details_vm.active_object('');

            self.clear_errors();

            self.$diagnosis_modal.modal('hide');

        };

        self.clear_errors = function() {
            self.primary_cancer_site_error('');
            self.primary_diagnosis_group_error('');
            self.primary_cancer_dx_description_error('');
            self.cancer_sites_metastatic_error('');
            self.date_diagnosis_error('');
            self.radiographic_diagnosis_explanation_error('');
            self.form_error('');
            $.each(self.cancer_sites_metastatic(), function(i, v) {
                v.id_error('');
                v.date_error('');
                v.metastatic_cancer_site_error('');
            })
        };

        self.set_diagnosis = function(diagnosis) {

            self.id(diagnosis.id);
            self.relative_id(diagnosis.relative_id);
            self.diagnosis(diagnosis);
            // self.patient_id(diagnosis.patient_id);
            self.date_diagnosis_verbose(diagnosis.date_diagnosis_verbose);
            self.primary_cancer_site(diagnosis.primary_cancer_site.get_id());
            self.primary_diagnosis_group(diagnosis.primary_diagnosis_group.get_id());
            self.cancer_sites_metastatic(_.map(diagnosis.cancer_sites_metastatic, function(csm, i) {

                var date = ko.observable(csm.date ? moment(csm.date, 'YYYY-MM-DD') : ''),
                    date_verbose = ko.observable(date() ? date().format('MMM DD YYYY') : '');

                return {
                    id: ko.observable(csm.id),
                    date: date,
                    date_verbose: date_verbose,
                    cancer_site_metastatic_id: ko.observable(csm.cancer_site_metastatic_id),
                    cancer_site_metastatic_name: csm.cancer_site_metastatic_name,
                    DELETE: ko.observable(false),
                    __prefix__: i,
                    id_error: ko.observable(''),
                    date_error: ko.observable(''),
                    metastatic_cancer_site_error: ko.observable('')
                }
            }));
            self.pathologic_diagnosis(diagnosis.pathologic_diagnosis);
            self.radiographic_diagnosis_explanation(diagnosis.radiographic_diagnosis_explanation);
            self.primary_cancer_dx_description(diagnosis.primary_cancer_dx_description);
            self.cancer_sites_metastatic_TOTAL_FORMS(self.cancer_sites_metastatic().length);
            self.cancer_sites_metastatic_INITIAL_FORMS(self.cancer_sites_metastatic().length);

            self.patient_details_vm.active_object(self.diagnosis());
        }

    }

});