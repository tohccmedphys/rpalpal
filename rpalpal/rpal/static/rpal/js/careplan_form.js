require(['jquery', 'base', 'select2'], function($) {
    $(document).ready(function () {

        var $structures_treated = $('#id_cp_structures_treated');

        function format_placeholder(option_data) {
            if (option_data.id === '') {
                return $('<span style="opacity: 0.8;">' + option_data.text + '</span>');
            } else {
                return option_data.text;
            }
        }
        $('.select2').select2({
            autoclose: true,
            width: '100%',
            minimumResultsForSearch: 10,
            templateSelection: format_placeholder,
            templateResult: format_placeholder
        });
        $('select.select2').overrideSelect2Keys();
        $structures_treated.select2({
            autoclose: true,
            width: '100%',
            minimumResultsForSearch: 10,
            templateSelection: format_placeholder,
            templateResult: format_placeholder
        });

    });
});