define(['jquery', 'lodash', 'knockout', 'rpal_models', 'moment', 'autosize', 'flatpickr', 'select2', 'daterangepicker', 'inputmask', 'async!https://maps.googleapis.com/maps/api/js?key=' + SiteConfig.google_api_key + '&libraries=places'], function($, _, ko, RPal, moment, autosize) {

    return function (patient_details_vm) {
        var self = this;
        self.patient_details_vm = patient_details_vm;
        patient_details_vm.treatment_course_vm = self;

        var duration_travel_error_msg = 'Could not calculate duration. Enter manually.';

        // self.treatment_course_vm;

        self.$treatment_course_modal = $('#treatment-course-modal');
        self.$poc_select_modal = $('#poc-select-modal');

        var $treatment_course_form = $('#treatment-course_form'),
            $add_treatment_course_go = $('#add-treatment-course-go'),
            $date_range_treatment = $('#date_range_treatment'),
            $ct_date = $('#id_tc_ct_date'),
            $coming_from = $('#id_tc_coming_from'),
            $duration_travel = $('#id_tc_duration_travel'),
            $explanation = $('#id_tc_explanation'),
            $treatment_course_details = $('#treatment_course_details'),
            $add_treatment_course = $('#add-treatment-course');

        self.modal_open = ko.observable(false);
        self.id = ko.observable();

        self.visit = ko.observable();
        self.visit_id = ko.observable();
        self.visit_options = ko.observableArray();
        self.patient = ko.observable();
        self.treatment_course = ko.observable();
        self.previous_treatment_course = ko.observable();

        self.dose_override = ko.observable();
        self.fractions_override = ko.observable();
        self.type_override = ko.observable();
        self.complexity_override = ko.observable();
        self.pattern_override = ko.observable();
        self.care_plan = ko.observable();
        self.care_plan_num_fields = ko.observable();
        // self.bone_mets_complexity = ko.observable();

        self.structure_treated = ko.observable();
        self.bone_mets_complexity = ko.observableArray([]);

        self.date_start = ko.observable();
        self.date_end = ko.observable();
        self.ct_date = ko.observable();
        self.ct_date_verbose = ko.observable();
        self.date_range_treatment = ko.observable();
        self.is_prescribed_course_complete = ko.observable(true);
        self.same_day_start_date = ko.observable(false);
        self.bone_mets_involved = ko.observable(false);
        self.explanation = ko.observable();

        self.transport_mode = ko.observable();
        self.transport_mode_base = ko.observable();
        self.location = ko.observable();
        self.location_address = ko.observable();
        self.coming_from = ko.observable();
        self.duration_travel = ko.observable();
        self.duration_travel_verbose = ko.observable();
        self.calulating_travel_time = ko.observable('');

        self.dose_override_error = ko.observable('');
        self.fractions_override_error = ko.observable('');
        self.type_override_error = ko.observable('');
        self.complexity_override_error = ko.observable('');
        self.pattern_override_error = ko.observable('');
        self.visit_error = ko.observable('');
        self.care_plan_error = ko.observable('');
        self.care_plan_num_fields_error = ko.observable('');
        self.bone_mets_complexity_error = ko.observable('');
        self.structure_treated_error = ko.observable('');
        self.date_range_treatment_error = ko.observable('');
        self.ct_date_error = ko.observable('');
        self.transport_mode_error = ko.observable('');
        self.location_error = ko.observable('');
        self.coming_from_error = ko.observable('');
        self.duration_travel_error = ko.observable('');
        self.explanation_error = ko.observable('');
        self.form_error = ko.observable('');

        ko.computed(function() {
            self.patient(patient_details_vm.patient());
        });

        self.visit_id.subscribe(function(id) {
            if (id) {
                $.each(patient_details_vm.visits(), function (i, v) {
                    if (v.id === id) {
                        self.visit(v);
                        return true;
                    }
                })
            } else {
                self.visit('');
            }
        });

        self.care_plan.subscribe(function(v) {

            var url = RPP_URLs.careplan_searcher,
                data = {'careplan_id': v};

            if (v && self.modal_open()) {
                $.ajax({
                    url: url,
                    data: data,
                    method: 'GET',
                    success: function(res) {
                        self.dose_override(res.careplan.dose);
                        self.fractions_override(res.careplan.fractions);
                        self.type_override(res.careplan.type.value);
                        self.complexity_override(res.careplan.complexity.value);
                        self.pattern_override(res.careplan.pattern.value);
                    },
                    error: function(res) {
                        console.log('failed diagnosis creation');
                        console.log(res);
                        // TODO add error message somewhere
                    }
                })
            } else if (!v) {
                self.dose_override('');
                self.fractions_override('');
                self.type_override('');
                self.complexity_override('');
                self.pattern_override('');
            }
        });

        self.show_num_fields = ko.computed(function() {
            return _.includes(rt_complexity_without_number_of_fields, self.complexity_override()) && self.complexity_override();
        });

        self.get_submit_text = ko.computed(function() {
            return self.id() ? 'Update' : 'Add'
        });

        $date_range_treatment.daterangepicker({
            autoClose: true,
            autoApply: true,
            opens: 'left',
            locale: {'format': 'MMM DD YYYY'}
        });

        $ct_date.flatpickr({
            dateFormat: 'M d Y',
            allowInput: true
        });

        autosize($explanation);

        self.date_range_treatment.subscribe(function(v) {
            self.date_start($date_range_treatment.data('daterangepicker').startDate);
            self.date_end($date_range_treatment.data('daterangepicker').endDate);
        });
        ko.computed(function() {
            if (self.ct_date_verbose()) {
                self.ct_date(moment(self.ct_date_verbose(), 'MMM DD YYYY'));
            } else {
                self.ct_date('');
            }
        });

        self.$treatment_course_modal.on('show.bs.modal', function (e) {
            self.modal_open(true);
            self.patient_details_vm.previous_active_treatment_course(self.patient_details_vm.active_treatment_course());
        });

        self.$treatment_course_modal.on('hide.bs.modal', function() {
            if (self.patient_details_vm.previous_active_treatment_course()) {
                $.each(self.patient_details_vm.treatment_courses(), function (v) {
                    console.log(v.id, self.patient_details_vm.previous_active_treatment_course().id);
                    if (self.patient_details_vm.previous_active_treatment_course().id === v.id) {
                        self.set_treatment_course(self.patient_details_vm.previous_active_treatment_course(), false, false);
                        return true;
                    }
                })
            }
        });

        self.coming_from_home = function() {
            self.coming_from(patient_details_vm.patient().home_address);
        };

        $add_treatment_course.click(function() {
            self.previous_treatment_course(self.treatment_course());
            self.clear();
            // self.prefill_from_last_treatment_course();
        });

        self.patient_details_vm.visits.subscribe(function(vs) {
            var options = [{display: '---------', id: ''}];
            $.each(vs, function(i, v) {
                options.push({display: v.get_display(), id: v.id});
            });
            self.visit_options(options);
        });

        self.format_duration = function(moment_duration) {
            var days = moment_duration.days(),
                hours = moment_duration.hours(),
                minutes = moment_duration.minutes();

            hours = hours + days * 24;
            if (hours > 99) {
                return '9999';
            }
            if (hours <= 9) {
                hours = '0' + hours;
            }
            if (minutes <= 9) {
                minutes = '0' + minutes;
            }
            return '' + hours + ':' + minutes;
        };

        self.select_poc = function(v) {
            self.visit_id(v.id);
            self.$poc_select_modal.modal('hide');
        };

        self.bone_mets_involved.subscribe(function(v) {
            if (!v) {
                self.bone_mets_complexity([]);
            }
        });

        self.location.subscribe(function(v) {
            if (v) { self.location_address(patient_details_vm.locations[self.location()].address); }
        });

        self.transport_mode.subscribe(function(v) {
            if (v) { self.transport_mode_base(patient_details_vm.transport_modes[self.transport_mode()].base); }
        });

        self.duration_travel.subscribe(function(v) {
            if (v) {
                self.duration_travel_verbose(self.format_duration(self.duration_travel()));
            } else {
                self.duration_travel_verbose('');
            }
        });

        self.location_address.subscribe(function(val) {
            if (val && self.transport_mode() && self.coming_from() && self.modal_open()) {
                self.calulating_travel_time('Calculating...');
                self.calculate_travel_duration();
            }
        });
        self.transport_mode_base.subscribeChanged(function(val, prev_val) {
            if (val && self.location_address() && self.coming_from() && self.modal_open()) {
                self.calulating_travel_time('Calculating...');
                self.calculate_travel_duration();
            }
        });
        self.coming_from.subscribe(function(val) {
            if (val && self.location_address() && self.transport_mode_base() && self.modal_open()) {
                self.calulating_travel_time('Calculating...');
                self.calculate_travel_duration();
            }
        });

        $duration_travel.inputmask('99:99', {numericInput: true, placeholder: "_", removeMaskOnSubmit: true});

        var coming_from_autocomplete;
        function initAutocomplete() {
            // Create the autocomplete object, restricting the search to geographical
            // location types.
            var center = new google.maps.LatLng(45.4, -75.6);
            var circle = new google.maps.Circle({
                center: center,
                radius: 100000
            });
            coming_from_autocomplete = new google.maps.places.Autocomplete(
                $coming_from[0],
                {
                    types: ['geocode'],
                }
            );
            coming_from_autocomplete.setBounds(circle.getBounds());
            // When the user selects an address from the dropdown, populate the address
            // fields in the form.

            google.maps.event.addListener(coming_from_autocomplete, 'place_changed', function () {
                var new_address = coming_from_autocomplete.getPlace().formatted_address;
                if (new_address !== '') {
                    self.coming_from(coming_from_autocomplete.getPlace().formatted_address);
                    if (self.transport_mode_base() && self.location_address() && self.modal_open()) {
                        self.calulating_travel_time('Calculating...');
                        self.calculate_travel_duration();
                    }
                }
            })
        }

        initAutocomplete();

        var distanceService = new google.maps.DistanceMatrixService();

        self.calculate_travel_duration = function() {
            self.patient_details_vm.disable_submits(true);
            distanceService.getDistanceMatrix({
                origins: [self.coming_from()],
                destinations: [self.location_address()],
                travelMode: patient_details_vm.google_transport_codes[patient_details_vm.transport_modes[self.transport_mode()].base],
                unitSystem: google.maps.UnitSystem.METRIC,
                durationInTraffic: true,
                avoidHighways: false,
                avoidTolls: false,
                drivingOptions: {
                    departureTime: new Date(Date.now()),  // for the time N milliseconds from now.
                    trafficModel: 'pessimistic'
                }
            },
            function (response, status) {
                self.patient_details_vm.disable_submits(false);
                if (status !== google.maps.DistanceMatrixStatus.OK) {
                    console.log('Error:', status);
                    self.duration_travel('');
                    self.duration_travel_error(duration_travel_error_msg);
                    self.calulating_travel_time('');
                } else {
                    var element = response.rows[0].elements[0];
                    if (element.status === 'OK') {
                        self.duration_travel(moment.duration(element.duration.value, 'seconds'));
                    } else {
                        console.log('Error:', status);
                        self.duration_travel('');
                        self.duration_travel_error(duration_travel_error_msg);
                        self.calulating_travel_time('');
                    }
                }
                self.calulating_travel_time('');
            });
        };

        function serialize_treatment_course_form() {

            var inputs = [
                'csrfmiddlewaretoken=' + $('input[name=csrfmiddlewaretoken]').val(),
                'patient=' + self.patient().id,
                'dose_override=' + self.dose_override(),
                'fractions_override=' + self.fractions_override(),
                'type_override=' + self.type_override(),
                'complexity_override=' + self.complexity_override(),
                'pattern_override=' + self.pattern_override(),
                'care_plan=' + self.care_plan(),
                'care_plan_num_fields=' + self.care_plan_num_fields(),
                'structure_treated=' + self.structure_treated(),
                // 'bone_mets_complexity=' + self.bone_mets_complexity(),

                'date_start=' + self.date_start().format('DD-MM-YYYY'),
                'date_end=' + self.date_end().format('DD-MM-YYYY'),
                'is_prescribed_course_complete=' + self.is_prescribed_course_complete(),
                'same_day_start_date=' + self.same_day_start_date(),
                'bone_mets_involved=' + self.bone_mets_involved(),
                'explanation=' + self.explanation(),
                'transport_mode=' + self.transport_mode(),
                'location=' + self.location(),
                'coming_from=' + self.coming_from(),
                'duration_travel=' + self.duration_travel_verbose(),

                'obj_type=treatment_course'
            ];
            if (self.ct_date()) {
                console.log(self.ct_date());
                inputs.push('ct_date=' + self.ct_date().format('DD-MM-YYYY'));
            }
            $.each(self.bone_mets_complexity(), function(i, v) {
                inputs.push('bone_mets_complexity=' + v);
            });

            if (self.id()) {
                inputs.push('id=' + self.id());
            }
            if (self.visit()) {
                inputs.push('visit=' + self.visit().id);
            }

            return inputs.join('&');
        }

        $treatment_course_form.submit(function() {

            var url = RPP_URLs.object_create_update_ajax,
                data = serialize_treatment_course_form();

            // $('.diagnosis-error').remove();
            $('.has-error').removeClass('has-error');
            if (self.duration_travel_error() === duration_travel_error_msg) {
                self.duration_travel_error('');
            }

            $.ajax({
                url: url,
                data: data,
                method: 'POST',
                success: function(res) {
                    self.patient_details_vm.disable_submits(false);
                    if (!res.success) {
                        $.each(res.form_errors, function(k, v) {
                            if (k === 'date_end' || k === 'date_start') {
                                self.date_range_treatment_error(v);
                            } else {
                                self[k + '_error'](v);
                            }
                        });
                    } else {
                        RPal.api.getTreatmentCourses(
                            function (treatment_courses) {
                                console.log(treatment_courses);
                                self.patient_details_vm.treatment_courses(treatment_courses);
                                self.set_treatment_course(_.find(self.patient_details_vm.treatment_courses(), {id: res.object.id}), false);

                                self.$treatment_course_modal.modal('hide');
                            },
                            function(res) {console.log(res);},
                            self.patient_details_vm.patient_id()
                        );
                    }
                },
                error: function(res) {
                    $add_treatment_course_go.removeAttr('disabled');
                    console.log('failed treatment course creation');
                    console.log(res);
                    self.form_error('Failed treatment course creation.')
                    self.patient_details_vm.disable_submits(false);
                    // TODO add error message somewhere
                }
            });
            return false;
        });

        $add_treatment_course_go.click(function() {
            console.log(self.patient_details_vm.disable_submits());
            if (!self.patient_details_vm.disable_submits()) {
                self.patient_details_vm.disable_submits(true);
                $treatment_course_form.submit();
            }
        });

        self.date_sort = function (arr, param) {
            return arr.sort(function(l, r) { return moment(l[param]) > moment(r[param]) ? 1 : -1 });
        };

        self.edit_treatment_course = function() {
            self.$treatment_course_modal.modal('show');
            // self.set_treatment_course(self.treatment_course(), true);
            self.modal_open(true);
        };

        self.treatment_course_details = function(tc) {
            if (self.treatment_course()) {
                $treatment_course_details.fadeTo('fast', 0.01, function () {
                    self.set_treatment_course(tc, false, false);
                    $treatment_course_details.fadeTo('fast', 1);
                });
            } else {
                self.set_treatment_course(tc, false, false);
            }
        };

        self.set_treatment_course = function(treatment_course, open, prefill) {
            self.modal_open(open);

            if (self.patient_details_vm.active_treatment_course().id !== treatment_course.id) {
                self.patient_details_vm.previous_active_treatment_course(self.treatment_course());
            }

            if (treatment_course) {

                if (!prefill) {
                    self.id(treatment_course.id);
                    self.treatment_course(treatment_course);
                    self.structure_treated(treatment_course.structure_treated.get_id());
                    self.patient_details_vm.active_treatment_course(self.treatment_course());
                    self.patient_details_vm.active_object(self.treatment_course());
                    self.clear_errors();
                }

                self.visit_id(treatment_course.visit_id);
                self.care_plan(treatment_course.care_plan ? treatment_course.care_plan.id : '');
                self.dose_override(treatment_course.dose_override);
                self.fractions_override(treatment_course.fractions_override);
                self.type_override(treatment_course.type_override.get_value());
                self.complexity_override(treatment_course.complexity_override.get_value());
                self.pattern_override(treatment_course.pattern_override.get_value());

                self.care_plan_num_fields(treatment_course.care_plan_num_fields.get_id());
                // self.bone_mets_complexity(treatment_course.bone_mets_complexity);
                self.bone_mets_complexity(_.map(treatment_course.bone_mets_complexity, function (bmc) {
                    return bmc.id.toString();
                }));

                self.is_prescribed_course_complete(treatment_course.is_prescribed_course_complete);
                self.same_day_start_date(treatment_course.same_day_start_date);
                self.bone_mets_involved(treatment_course.bone_mets_involved);
                self.explanation(treatment_course.explanation);
                self.date_range_treatment(moment(treatment_course.date_start, 'YYYY-MM-DD').format('MMM DD YYYY') + ' - ' + moment(treatment_course.date_end, 'YYYY-MM-DD').format('MMM DD YYYY'));
                self.ct_date_verbose(treatment_course.ct_date_verbose);

                self.coming_from(treatment_course.coming_from);
                self.duration_travel(treatment_course.duration_travel);
                self.calulating_travel_time = ko.observable('');
                self.transport_mode(treatment_course.transport_mode.get_id());
                self.location(treatment_course.location.get_id());
            }

        };

        self.clear = function() {
            self.id('');
            self.treatment_course(null);
            self.modal_open(false);
            self.visit(null);
            self.visit_id('');
            self.dose_override('');
            self.fractions_override('');
            self.type_override('');
            self.complexity_override('');
            self.pattern_override('');
            self.care_plan('');
            self.care_plan_num_fields('');
            self.bone_mets_complexity([]);
            // self.treatment_course('');
            self.structure_treated('');
            self.date_range_treatment('');
            self.ct_date_verbose('');
            self.is_prescribed_course_complete(true);
            self.bone_mets_involved(false);
            self.same_day_start_date(false);
            self.explanation('');
            self.transport_mode('');
            self.transport_mode_base('');
            self.location('');
            self.location_address('');
            self.coming_from('');
            self.duration_travel('');

            self.patient_details_vm.active_object('');
            self.patient_details_vm.active_treatment_course('');

            self.clear_errors();

            self.$treatment_course_modal.modal('hide');
        };

        self.clear_errors = function() {
            self.date_range_treatment_error('');
            self.ct_date_error('');
            self.coming_from_error('');
            self.duration_travel_error('');
            self.transport_mode_error('');
            self.location_error('');
            self.explanation_error('');
            self.dose_override_error('');
            self.fractions_override_error('');
            self.type_override_error('');
            self.complexity_override_error('');
            self.pattern_override_error('');
            self.visit_error('');
            self.care_plan_error('');
            self.care_plan_num_fields_error('');
            self.bone_mets_complexity_error('');
            self.structure_treated_error('');
            self.form_error('');
        };

        self.prefill_from_last_treatment_course = function() {
            var treatment_courses_length = self.patient_details_vm.treatment_courses().length;
            if (treatment_courses_length > 0) {
                var treatment_course = self.patient_details_vm.treatment_courses()[treatment_courses_length - 1];
                // self.transport_mode(treatment_course.transport_mode.get_id());
                // self.location(treatment_course.location.get_id());
                // self.coming_from(treatment_course.coming_from);
                // self.duration_travel(treatment_course.duration_travel);
                // self.care_plan(treatment_course.care_plan);
                self.set_treatment_course(treatment_course, false, true);

            }
        }

    }

});