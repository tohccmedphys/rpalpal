define(['jquery', 'lodash', 'knockout', 'rpal_models', 'moment', 'autosize', 'select2', 'daterangepicker', 'inputmask', 'async!https://maps.googleapis.com/maps/api/js?key=' + SiteConfig.google_api_key + '&libraries=places'], function($, _, ko, RPal, moment, autosize) {

    return function (treatment_course_vm) {
        var self = this;
        self.treatment_course_vm = treatment_course_vm;
        treatment_course_vm.followup_vm = self;
        self.$followup_modal = $('#followup-modal');

        var $followup_form = $('#followup_form'),
            $add_followup_go = $('#add-followup-go'),
            $add_followup = $('#add-followup'),
            $date = $('#id_fu_date');

        self.id = ko.observable();

        self.followup = ko.observable();

        self.date = ko.observable();
        self.date_verbose = ko.observable();
        self.is_further_treatment_required = ko.observable(false);
        self.medications = ko.observableArray([]);
        self.communication_mode_physician = ko.observable();

        self.date_error = ko.observable('');
        self.medications_error = ko.observable('');
        self.communication_mode_physician_error = ko.observable('');

        self.form_error = ko.observable('');
        self.modal_open = ko.observable(false);

        ko.computed(function() {
            self.date(moment(self.date_verbose(), 'MMM DD YYYY'));
        });

        self.get_submit_text = ko.computed(function() {
            return self.id() ? 'Update' : 'Add'
        });

        $date.daterangepicker({
            singleDatePicker: true,
            autoClose: true,
            autoApply: true,
            maxDate: moment(),
            locale: {'format': 'MMM DD YYYY'}
        });

        function serialize_followup_form() {

            var inputs = [
                'csrfmiddlewaretoken=' + $('input[name=csrfmiddlewaretoken]').val(),
                'date=' + self.date().format('DD-MM-YYYY'),
                'is_further_treatment_required=' + self.is_further_treatment_required(),
                'communication_mode_physician=' + self.communication_mode_physician(),
                'treatment_course=' + self.treatment_course_vm.treatment_course().id,
                'obj_type=followup'
            ];
            $.each(self.medications(), function(i, v) {
                inputs.push('medications=' + v);
            });

            if (self.id()) {
                inputs.push('id=' + self.id());
            }

            return inputs.join('&');
        }

        self.edit_followup = function(fu) {
            console.log(fu);
            self.set_followup(fu, false);
            self.$followup_modal.modal('show');
        };
        self.treatment_course_vm.patient_careplan_vm.patient_details_vm.edit_followup = self.edit_followup;

        $followup_form.submit(function() {

            self.form_error('');

            var url = RPP_URLs.object_create_update_ajax,
                data = serialize_followup_form();

            // $('.diagnosis-error').remove();
            $('.has-error').removeClass('has-error');

            $.ajax({
                url: url,
                data: data,
                method: 'POST',
                success: function(res) {
                    console.log(res);

                    if (!res.success) {
                        $.each(res.form_errors, function(k, v) {
                            if (k === 'treatment_course') {
                                self.form_error('Treatment Course: ' + v);
                            } else {
                                self[k + '_error'](v);
                            }
                        });
                    } else {
                        if (res.is_new) {
                            treatment_course_vm.patient_careplan_vm.followups.push(new RPal.models.FollowUp(res.object));
                        } else {
                            var new_version = new RPal.models.FollowUp(res.object);
                            $.each(treatment_course_vm.patient_careplan_vm.followups(), function(i, v) {
                                if (v.id === new_version.id) {
                                    treatment_course_vm.patient_careplan_vm.followups.replace(
                                        treatment_course_vm.patient_careplan_vm.followups()[i], new_version
                                    );
                                    return false;
                                }
                            })
                        }
                        self.$followup_modal.modal('hide');
                        self.clear();
                    }
                },
                error: function(res) {
                    console.log('failed followup creation');
                    console.log(res);
                    self.form_error(res.statusText)
                }
            });
            return false;
        });

        $add_followup_go.click(function() {
            $followup_form.submit();
        });

        self.clear = function() {

            self.id(null);

            self.followup(null);

            self.date('');

            self.date_verbose(moment().format('MMM DD YYYY'));

            self.is_further_treatment_required(false);
            self.medications([]);
            self.communication_mode_physician('');

            self.date_error('');
            self.medications_error('');
            self.communication_mode_physician_error('');

        };

        self.set_followup = function(followup, open) {

            self.modal_open(open);

            if (followup) {
                self.id(followup.id);

                self.followup(followup);

                self.date_verbose(followup.date_verbose);
                self.is_further_treatment_required(followup.is_further_treatment_required);
                self.medications(_.map(followup.medications, function(m) {
                    return m.id.toString();
                }));
                self.communication_mode_physician(followup.communication_mode_physician.get_value());
            }

        }


    }

});