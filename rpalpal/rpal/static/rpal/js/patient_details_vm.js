
define(['jquery', 'lodash', 'knockout', 'rpal_models', 'moment'], function($, _, ko, RPal, moment) {

    return function () {

        var self = this;

        self.patient_id = ko.observable();
        self.patient_id_searched = ko.observable();
        self.patient_mrn = ko.observable();
        self.patient = ko.observable();

        self.delete_error = ko.observable();
        self.delete_error.subscribe(function() {
            console.log(self.delete_error())
        })
        self.disable_submits = ko.observable(false);

        self.locations = locations;
        self.transport_modes = transport_modes;
        self.google_transport_codes = {
            'pt': 'TRANSIT',
            'rv': 'DRIVING',
            'w': 'WALKING',
            'b': 'BICYCLING'
        };

        self.active_object = ko.observable();
        self.active_object_warning = ko.observable();

        self.active_object.subscribe(function(v) {
            var klass = v.class === 'patient' ? '' : v.class + ' for ';
            self.active_object_warning('Delete ' + klass + 'patient ' + (self.patient() ? self.patient().mrn : '') + '?');
        });

        self.patient_id_searched.subscribe(function() {
            if (self.patient_id_searched()) {
                self.patient_id(self.patient_id_searched());
            }
        });

        function error(pfft) {
            console.log(pfft);
        }

        self.patient_id.subscribe(function(val) {

            if (val) {

                function error(pfft) {
                    console.log(pfft);
                }

                RPal.api.getPatient(
                    function(patient) {
                        self.patient(patient);
                        self.patient_mrn(patient.mrn);
                        // self.patient_vm.set_patient(self.patient());
                    },
                    error,
                    self.patient_id()
                );

            } else {
                return null;
            }
        });

        RPal.api.getMetastaticCancerSites(function (response) {
            var options = response.metastatic_cancer_sites;
            options.unshift({id: '', name: '---------'});
            self.diagnosis_vm.cancer_sites_metastatic_options(options);
        }, error);

        self.patient.subscribe(function(patient) {
            self.visit_vm.clear();
            self.referral_vm.clear();
            self.diagnosis_vm.clear();
            self.treatment_course_vm.clear();
            if (patient) {
                RPal.api.getReferrals(
                    function (referrals) {
                        self.referrals(referrals);
                    },
                    error,
                    self.patient_id()
                );
                RPal.api.getDiagnoses(
                    function (diagnoses) {
                        self.diagnoses(diagnoses);
                    },
                    error,
                    self.patient_id()
                );
                RPal.api.getVisits(
                    function (visits) {
                        self.visits(visits);
                    },
                    error,
                    self.patient_id()
                );
                RPal.api.getTreatmentCourses(
                    function (all_treatment_courses) {
                        self.treatment_courses(all_treatment_courses);
                    },
                    error,
                    self.patient_id()
                );
            }
        });

        self.edit_patient = function() {
            self.patient_vm.$patient_modal.modal('show');
            self.patient_vm.set_patient(self.patient());
        };

        self.referrals = ko.observableArray([]);
        self.referral_vm;
        self.referrals.subscribe(function(referrals) {
            self.patient().referrals = referrals;
        });

        self.edit_referral = function(ref) {
            self.referral_vm.$referral_modal.modal('show');
            self.referral_vm.set_referral(ref);
        };

        self.active_physicians = ko.observableArray([]);
        RPal.api.getActivePhysicians(
            function (active_physicians) {
                self.active_physicians(active_physicians);
            },
            error
        );

        self.active_rad_oncs = ko.observableArray([]);
        RPal.api.getActiveRadOncs(
            function (active_rad_oncs) {
                self.active_rad_oncs(active_rad_oncs);
            },
            error
        );

        self.diagnoses = ko.observableArray([]);
        self.diagnosis_vm;
        self.diagnoses.subscribe(function() {
            self.patient().diagnoses = self.diagnoses();
        });

        self.edit_diagnosis = function(dia) {
            self.diagnosis_vm.$diagnosis_modal.modal('show');
            self.diagnosis_vm.set_diagnosis(dia);
        };

        self.visits = ko.observableArray([]);
        self.visit_vm;
        self.visits.subscribe(function(visits) {
            self.patient().visits = visits;
        });

        self.active_visit = ko.observable();
        self.previous_active_visit = ko.observable();
        // self.active_visit.subscribe(function() {
        //     self.visit_vm.patient_careplans(self.active_visit().patient_careplans);
        // });
        self.active_visit_id = function() {
            if (self.active_visit()) {
                return self.active_visit().id;
            } else {
                return -1;
            }
        };

        self.treatment_courses = ko.observableArray([]);
        self.treatment_course_vm;
        self.treatment_courses.subscribe(function(treatment_courses) {
            self.patient().treatment_courses = treatment_courses;
        });
        self.active_treatment_course = ko.observable();
        self.previous_active_treatment_course = ko.observable();
        // self.edit_treatment_course = function(tc) {
        //     self.treatment_course_vm.$treatment_course_modal.modal('show');
        //     self.treatment_course_vm.set_treatment_course(tc);
        // };

        self.print_it = function(it) {
            console.log(it);
        };

        self.get_date = function(date, in_format, out_format) {
            if (date) {
                return moment(date, in_format).format(out_format);
            }
            return ''
        };

        self.date_sort = function (arr, param) {
            return arr.sort(function(l, r) { return moment(l[param]) > moment(r[param]) ? 1 : -1 });
        };
        self.number_sort = function(arr, param) {
            return arr.sort(function(l, r) { return l[param] > r[param] ? 1 : -1 })
        };

        self.get_visit_display = function(id) {
            var visit = _.find(self.visits(), {id:id});
            if (visit) {
                return visit.get_display();
            }
        };

        self.select_visit = function(obj) {
            console.log(obj);
            $.each(self.visits(), function(i, v) {
                if (v.id === obj.visit_id) {
                    self.visit_vm.visit_details(v, false);
                    $('#poc-tab').tab('show');
                    return true;
                }
            });
        };

        self.select_visit_with_vm = function(vm) {
            console.log(vm)
            $.each(self.visits(), function(i, v) {
                if (v.id === vm.visit_id()) {
                    self.visit_vm.visit_details(v, false);
                    console.log($('#poc-tab'))
                    $('#poc-tab').tab('show');

                    return true;
                }
            });
        };

        self.select_treatment_course = function(vis) {
            $.each(self.treatment_courses(), function(i, v) {
                if (v.id === vis.treatment_course_id()) {
                    self.treatment_course_vm.treatment_course_details(v);
                    $('#tc-tab').tab('show');
                    return true;
                }
            })
        };

        self.select_referral = function(vis) {
            var ref_id;
            if (vis instanceof RPal.models.Visit) {
                ref_id = vis.referral_id;
            } else {
                ref_id = vis.referral_id();
            }
            $.each(self.referrals(), function(i, v) {
                if (v.id === ref_id) {
                    self.referral_vm.set_referral(v);
                    $('#r-tab').tab('show');
                    return true;
                }
            })
        };

        self.referral_display_from_visit = function(vis) {
            var ref = _.find(self.referrals(), {id: vis.referral_id});
            if (ref) {
                return ref.get_display();
            }
            return '';
        };

        self.close_delete_modal = function() {
            $('#delete-modal').modal('hide');
        };

        $('#delete-modal').on('hide.bs.modal', function() {
            self.delete_error('');
        });

        var delete_functions = {
            'referral': function() {
                self.referrals(_.filter(self.referrals(), function(v) {
                    return v.id !== self.active_object().id;
                }));
                self.referral_vm.clear();
            },
            'diagnosis': function() {
                self.diagnoses(_.filter(self.diagnoses(), function(v) {
                    return v.id !== self.active_object().id;
                }));
                self.diagnosis_vm.clear();
            },
            'visit': function() {
                self.visits(_.filter(self.visits(), function(v) {
                    return v.id !== self.active_object().id;
                }));
                self.visit_vm.clear();
            },
            'treatment_course': function() {
                self.treatment_courses(_.filter(self.treatment_courses(), function(v) {
                    return v.id !== self.active_object().id;
                }));
                self.treatment_course_vm.clear();
            },
            'patient': function() {
                self.patient('');
                self.patient_vm.clear();
            }
        };

        self.delete_active_object = function() {
            if (!self.disable_submits()) {
                self.disable_submits(true);
                RPal.api.deleteObject(
                    function () {
                        delete_functions[self.active_object().class]();
                        self.active_object('');
                        self.close_delete_modal();
                        self.disable_submits(false);
                    },
                    function (res) {
                        console.log(res);
                        self.delete_error('Cannot delete ' + self.active_object().class)
                        self.disable_submits(false);
                    },
                    self.active_object().class,
                    self.active_object().id
                )
            }
        };

    };

});