define(['jquery', 'lodash', 'knockout', 'rpal_models', 'moment', 'select2'], function($, _, ko, RPal, moment) {

    return function (patient_details_vm) {

        var self = this;

        self.patient_details_vm = patient_details_vm;
        patient_details_vm.referral_vm = self;

        var $referral_form = $('#referral_form'),
            $add_referral_go = $('#add-referral-go'),
            $date_referred = $('#id_date_referred');
            // $patient_id = $('#id_patient'),
            // $referring_communication_mode = $('#id_referring_communication_mode'),
            // $referral_reach = $('#id_referral_reach'),
            // $referral_reasons = $('#id_referral_reasons'),
            // $referring_physician = $('#id_referring_physician');

        self.$physician = $('#id_referring_physician');
        self.$referral_modal = $('#referral-modal');

        self.$referral_modal.on('show.bs.modal', function (e) {
            self.clear();
        });

        self.referral = ko.observable();

        self.patient = ko.observable();
        self.date_referred = ko.observable();
        self.date_referred_verbose = ko.observable();
        self.referring_communication_mode = ko.observable();
        self.referral_reach = ko.observable();
        self.referring_physician = ko.observable();
        self.physician_object = ko.observable(null);
        self.selectable_physicians = ko.observableArray([]);
        self.referral_location = ko.observable();
        self.referral_reasons = ko.observableArray([]);

        self.id = ko.observable();
        self.relative_id = ko.observable();

        self.date_referred_error = ko.observable('');
        self.patient_error = ko.observable('');
        self.referring_physician_error = ko.observable('');
        self.referring_communication_mode_error = ko.observable('');
        self.referral_reach_error = ko.observable('');
        self.referral_reasons_error = ko.observable('');
        self.referral_location_error = ko.observable('');
        self.form_error = ko.observable('');

        $date_referred.flatpickr({
            dateFormat: 'M d Y',
            allowInput: true,
            maxDate: moment()._d,
        });

        self.referring_physician.subscribe(function(v) {
            if (v) {
                self.physician_vm.search_and_set_physician(v);
            } else {
                self.physician_vm.clear();
            }
        });

        self.patient_details_vm.active_physicians.subscribe(function(aps) {
            var sps = [{display: '---------', id: ''}].concat(_.map(aps, function(ap) { return { display: ap.display, id: ap.id }}));
            if (self.physician_object()) {
                sps.push({display: self.physician_object().display, id: self.physician_object().id});
            }
            self.selectable_physicians(sps);
        });
        self.physician_object.subscribe(function(po) {
            var sps = [{display: '---------', id: ''}].concat(_.map(
                self.patient_details_vm.active_physicians(), function(ap) { return { display: ap.display, id: ap.id }}
            ));
            if (po) {
                sps.push({ display: po.display, id: po.id });
            }
            self.selectable_physicians(sps);
        });

        ko.computed(function() {
            // self.patient_id(patient_details_vm.patient_id());
            self.patient(patient_details_vm.patient());
        });

        ko.computed(function() {
            self.date_referred(moment(self.date_referred_verbose(), 'MMM DD YYYY'));
        });

        self.get_submit_text = ko.computed(function() {
            return self.id() ? 'Update' : 'Add'
        });

        function serialize_referral_form(form) {
            console.log(self.date_referred());
            var inputs = [
                'csrfmiddlewaretoken=' + $('input[name=csrfmiddlewaretoken]').val(),
                'patient=' + self.patient().id,
                'date_referred=' + (self.date_referred() ? self.date_referred().format('DD-MM-YYYY') : ''),
                'referring_communication_mode=' + self.referring_communication_mode(),
                'referral_reach=' + self.referral_reach(),
                'referral_location=' + self.referral_location(),
                'referring_physician=' + self.referring_physician(),
                'obj_type=referral'
            ];

            $.each(self.referral_reasons(), function(i, v) {
                inputs.push('referral_reasons=' + v);
            });

            if (self.id()) {
                inputs.push('id=' + self.id());
            }

            return inputs.join('&');
        }

        $referral_form.submit(function() {

            var url = RPP_URLs.object_create_update_ajax,
                data = serialize_referral_form(this);

            $('.referral-error').remove();
            $('.has-error').removeClass('has-error');

            $.ajax({
                url: url,
                data: data,
                method: 'POST',
                success: function(res) {
                    if (!res.success) {
                        $.each(res.form_errors, function(k, v) {
                            self[k + '_error'](v);
                        });
                    } else {
                        RPal.api.getReferrals(
                            function (referrals) {
                                console.log(referrals);
                                self.patient_details_vm.referrals(referrals);
                                // self.set_referral(_.find(self.patient_details_vm.referrals(), {id: res.object.id}), false);

                                self.clear();
                                self.$referral_modal.modal('hide');
                            },
                            function(res) {console.log(res);},
                            self.patient_details_vm.patient_id()
                        );
                    }
                    self.patient_details_vm.disable_submits(false);
                },
                error: function(res) {
                    console.log('failed referral creation');
                    console.log(res);
                    self.form_error('Failed referral creation.')
                    // TODO add error message somewhere
                    self.patient_details_vm.disable_submits(false);
                }
            });
            return false;
        });

        $add_referral_go.click(function() {
            if (!self.patient_details_vm.disable_submits()) {
                self.patient_details_vm.disable_submits(true);
                $referral_form.submit();
            }
        });

        self.clear = function () {
            self.date_referred('');
            self.date_referred_verbose('');
            self.referring_communication_mode('');
            self.referral_reach('');
            self.referral_location('');
            self.referring_physician('');
            self.referral_reasons([]);

            self.physician_object(null);
            self.id('');
            self.relative_id('');
            self.referral('');
            self.patient_details_vm.active_object('');
            self.clear_errors();

            self.$referral_modal.modal('hide');
        };

        self.clear_errors = function () {
            self.date_referred_error('');
            self.patient_error('');
            self.referring_physician_error('');
            self.referring_communication_mode_error('');
            self.referral_reach_error('');
            self.referral_reasons_error('');
            self.referral_location_error('');
            self.form_error('');
        };

        self.set_referral = function(referral) {

            self.id(referral.id);
            self.relative_id(referral.relative_id);

            self.physician_object(referral.referring_physician);
            // self.patient_id(referral.patient_id);
            self.date_referred_verbose(referral.date_referred_verbose);
            self.referring_communication_mode(referral.referring_communication_mode.get_value());
            self.referral_reach(referral.referral_reach.get_id());
            self.referral_location(referral.referral_location.get_id());
            self.referring_physician(referral.referring_physician.id.toString());
            self.referral_reasons(_.map(referral.referral_reasons, function(rr) {
                return rr.id.toString();
            }));
            self.referral(referral);
            self.patient_details_vm.active_object(self.referral());

        }

    };
});