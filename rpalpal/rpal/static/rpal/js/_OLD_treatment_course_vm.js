define(['jquery', 'lodash', 'knockout', 'rpal_models', 'moment', 'autosize', 'select2', 'daterangepicker', 'inputmask', 'async!https://maps.googleapis.com/maps/api/js?key=' + SiteConfig.google_api_key + '&libraries=places'], function($, _, ko, RPal, moment, autosize) {

    return function (patient_careplan_vm) {
        var self = this;
        self.patient_careplan_vm = patient_careplan_vm;
        patient_careplan_vm.treatment_course_vm = self;
        self.$treatment_course_modal = $('#treatment-course-modal');

        var $treatment_course_form = $('#treatment-course_form'),
            $add_treatment_course_go = $('#add-treatment-course-go'),
            $add_treatment_course = $('#add-treatment-course'),
            $date_range_treatment = $('#date_range_treatment'),
            $coming_from = $('#id_tc_coming_from'),
            $duration_travel = $('#id_tc_duration_travel'),
            $explanation = $('#id_tc_explanation');

        self.id = ko.observable();

        self.treatment_course = ko.observable();

        self.date_start = ko.observable();
        self.date_end = ko.observable();
        self.date_range_treatment = ko.observable();
        self.is_prescribed_course_complete = ko.observable(true);
        self.explanation = ko.observable();

        self.transport_mode = ko.observable();
        self.transport_mode_base = ko.observable();
        self.location = ko.observable();
        self.location_address = ko.observable();
        self.coming_from = ko.observable();
        self.duration_travel = ko.observable();
        self.duration_travel_verbose = ko.observable();
        self.accompanying_people = ko.observableArray([]);
        self.followups = ko.observableArray([]);

        self.date_range_treatment_error = ko.observable('');
        self.transport_mode_error = ko.observable('');
        self.location_error = ko.observable('');
        self.coming_from_error = ko.observable('');
        self.duration_travel_error = ko.observable('');
        self.accompanying_people_error = ko.observable('');
        self.is_prescribed_course_complete_error = ko.observable('');
        self.explanation_error = ko.observable('');

        self.form_error = ko.observable('');

        self.calulating_travel_time = ko.observable('');
        self.modal_open = ko.observable(false);

        $date_range_treatment.daterangepicker({
            autoClose: true,
            autoApply: true,
            locale: {'format': 'MMM Do YYYY'}
        });

        autosize($explanation);

        self.date_range_treatment.subscribe(function(v) {
            self.date_start($date_range_treatment.data('daterangepicker').startDate);
            self.date_end($date_range_treatment.data('daterangepicker').endDate);
        });

        self.$treatment_course_modal.on('show.bs.modal', function (e) {
            self.modal_open(true);
        });

        self.$treatment_course_modal.on('hide.bs.modal', function() {
            self.modal_open(false);
            self.treatment_course(patient_careplan_vm.treatment_course());
        });

        self.location.subscribe(function(v) {
            if (v) { self.location_address(patient_careplan_vm.patient_details_vm.locations[self.location()].address); }
        });

        self.transport_mode.subscribe(function(v) {
            if (v) { self.transport_mode_base(patient_careplan_vm.patient_details_vm.transport_modes[self.transport_mode()].base); }
        });

        self.duration_travel.subscribe(function(v) {
            if (v) {
                self.duration_travel_verbose(self.format_duration(self.duration_travel()));
            } else {
                self.duration_travel_verbose('');
            }
        });

        self.location_address.subscribe(function(val) {
            if (val && self.transport_mode_base() && self.coming_from() && self.modal_open()) {
                self.calulating_travel_time('Calculating...');
                self.calculate_travel_duration();
            }
        });
        self.transport_mode_base.subscribeChanged(function(val, prev_val) {
            if (val && self.location_address() && self.coming_from() && self.modal_open()) {
                self.calulating_travel_time('Calculating...');
                self.calculate_travel_duration();
            }
        });

        ko.computed(function() {
            if (!self.coming_from() && patient_careplan_vm.patient_details_vm.patient()) {
                self.coming_from(patient_careplan_vm.patient_details_vm.patient().home_address)
            }
        });

        self.get_submit_text = ko.computed(function() {
            return self.id() ? 'Update' : 'Add'
        });

        $duration_travel.inputmask('99:99', {numericInput: true, placeholder: "_", removeMaskOnSubmit: true});

        var coming_from_autocomplete;
        function initAutocomplete() {
            // Create the autocomplete object, restricting the search to geographical
            // location types.
            var center = new google.maps.LatLng(45.4, -75.6);
            var circle = new google.maps.Circle({
                center: center,
                radius: 100000
            });
            coming_from_autocomplete = new google.maps.places.Autocomplete(
                $coming_from[0],
                {
                    types: ['geocode'],
                }
            );
            coming_from_autocomplete.setBounds(circle.getBounds());
            // When the user selects an address from the dropdown, populate the address
            // fields in the form.

            google.maps.event.addListener(coming_from_autocomplete, 'place_changed', function () {
                var new_address = coming_from_autocomplete.getPlace().formatted_address;
                if (new_address !== '') {
                    self.coming_from(coming_from_autocomplete.getPlace().formatted_address);
                    if (self.transport_mode_base() && self.location_address() && self.modal_open()) {
                        self.calulating_travel_time('Calculating...');
                        self.calculate_travel_duration();
                    }
                }
            })
        }

        initAutocomplete();

        var distanceService = new google.maps.DistanceMatrixService();

        self.calculate_travel_duration = function() {

            distanceService.getDistanceMatrix({
                origins: [self.coming_from()],
                destinations: [self.location_address()],
                travelMode: patient_careplan_vm.patient_details_vm.google_transport_codes[patient_careplan_vm.patient_details_vm.transport_modes[self.transport_mode()].base],
                unitSystem: google.maps.UnitSystem.METRIC,
                durationInTraffic: true,
                avoidHighways: false,
                avoidTolls: false,
                drivingOptions: {
                    departureTime: new Date(Date.now()),  // for the time N milliseconds from now.
                    trafficModel: 'pessimistic'
                }
            },
            function (response, status) {
                console.log(response);
                if (status !== google.maps.DistanceMatrixStatus.OK) {
                    console.log('Error:', status);
                    self.duration_travel('');
                } else {
                    var element = response.rows[0].elements[0];
                    if (element.status === 'OK') {
                        self.duration_travel(moment.duration(element.duration.value, 'seconds'));
                    } else {
                        self.duration_travel('');
                    }
                }
                self.calulating_travel_time('');
            });
        };

        self.format_duration = function(moment_duration) {
            var hours = moment_duration.hours(),
                minutes = moment_duration.minutes();

            if (hours > 99) {
                return '9999';
            }
            if (hours <= 9) {
                hours = '0' + hours;
            }
            if (minutes <= 9) {
                minutes = '0' + minutes;
            }
            return '' + hours + '' + minutes;
        };

        function serialize_treatment_course_form() {

            // TODO: fix this part here

            var inputs = [
                'csrfmiddlewaretoken=' + $('input[name=csrfmiddlewaretoken]').val(),
                'patient_careplan_id=' + self.patient_careplan_vm.patient_careplan().id,
                'patient=' + self.patient_careplan_vm.patient_details_vm.patient_id(),
                'date_start=' + self.date_start().format('DD-MM-YYYY'),
                'date_end=' + self.date_end().format('DD-MM-YYYY'),
                'is_prescribed_course_complete=' + self.is_prescribed_course_complete(),
                'explanation=' + self.explanation(),
                'transport_mode=' + self.transport_mode(),
                'location=' + self.location(),
                'coming_from=' + self.coming_from() ? self.coming_from() : '',
                'duration_travel=' + self.duration_travel_verbose(),
                'obj_type=treatment_course'
            ];
            $.each(self.accompanying_people(), function(i, v) {
                inputs.push('accompanying_people=' + v);
            });

            if (self.id()) {
                inputs.push('id=' + self.id());
            }

            return inputs.join('&');
        }

        $treatment_course_form.submit(function() {

            self.form_error('');

            var url = RPP_URLs.object_create_update_ajax,
                data = serialize_treatment_course_form();

            // $('.diagnosis-error').remove();
            $('.has-error').removeClass('has-error');

            $.ajax({
                url: url,
                data: data,
                method: 'POST',
                success: function(res) {

                    if (!res.success) {
                        $.each(res.form_errors, function(k, v) {
                            if (k === 'date_end' || k === 'date_start') {
                                self.date_range_treatment_error(v);
                            } else {
                                self[k + '_error'](v);
                            }
                        });
                    } else {
                        console.log(res);
                        var new_treatment_course = new RPal.models.TreatmentCourse(res.object);
                        self.set_treatment_course(new_treatment_course);
                        patient_careplan_vm.patient_careplan().treatment_course = new_treatment_course;
                        self.$treatment_course_modal.modal('hide');
                    }
                },
                error: function(res) {
                    console.log('failed treatment course creation');
                    console.log(res);
                    self.form_error(res.statusText)
                }
            });
            return false;
        });

        $add_treatment_course_go.click(function() {
            $treatment_course_form.submit();
        });

        self.edit_followup = function(fu) {
            console.log(fu);
        };

        self.date_sort = function (arr, param) {
            return arr.sort(function(l, r) { return moment(l[param]) > moment(r[param]) ? 1 : -1 });
        };

        self.clear = function() {
            self.id(null);

            self.treatment_course(null);

            self.date_range_treatment('');
            self.is_prescribed_course_complete(true);
            self.explanation('');

            self.transport_mode('');
            self.transport_mode_base('');
            self.location('');
            self.location_address('');
            self.coming_from(patient_careplan_vm.patient_details_vm.patient().home_address);
            self.duration_travel('');

            self.accompanying_people([]);
            self.followups([]);

            self.date_range_treatment_error('');
            self.coming_from_error('');
            self.duration_travel_error('');
            self.transport_mode_error('');
            self.location_error('');
            self.accompanying_people_error('');
            self.is_prescribed_course_complete_error('');
            self.explanation_error('');

            self.calulating_travel_time('');
        };

        self.set_treatment_course = function(treatment_course, open) {

            self.modal_open(open);

            self.patient_careplan_vm.treatment_course(treatment_course);

            if (treatment_course) {
                // self.patient_careplan_vm.active_treatment_course(treatment_course);
                self.treatment_course(treatment_course);

                self.id(treatment_course.id);

                self.is_prescribed_course_complete(treatment_course.is_prescribed_course_complete);
                self.explanation(treatment_course.explanation);
                self.date_range_treatment(moment(treatment_course.date_start, 'YYYY-MM-DD').format('MMM Do YYYY') + ' - ' + moment(treatment_course.date_end, 'YYYY-MM-DD').format('MMM Do YYYY'));

                self.coming_from(treatment_course.coming_from);
                if (treatment_course.duration_travel) {
                    self.duration_travel(moment.duration(treatment_course.duration_travel, 'seconds'));
                } else {
                    self.duration_travel('');
                }
                self.transport_mode(treatment_course.transport_mode.get_id());
                self.location(treatment_course.location.get_id());

                self.accompanying_people(_.map(treatment_course.accompanying_people, function (ap) {
                    return ap.id.toString();
                }));

                self.followups(treatment_course.followups);
                patient_careplan_vm.followups(treatment_course.followups);

            } else {
                self.clear();
            }

        }

    }

});