require(['jquery', 'base', 'dateline', 'select2', 'async!https://maps.googleapis.com/maps/api/js?key=' + SiteConfig.google_api_key + '&libraries=places'], function($) {
    $(document).ready(function() {

        console.log('google loaded');
        var home_address = $('#home_address-field'),
            mrn = $('#mrn-field'),
            date_birth = $('#date_birth-field'),
            is_deceased = $('#deceased-field'),
            date_deceased_div = $('#date-deceased-div'),
            date_deceased = $('#date_deceased-field'),
            date_last_contact = $('#date_last_contact-field'),
            allergies = $('#allergies-field'),
            facilities = $('#facilities-field'),

            home_address_autocomplete;

        is_deceased.click(function() {
            $(this).prop('checked') ? date_deceased_div.fadeIn('fast') : date_deceased_div.fadeOut('fast');
        });

        date_birth.dateline({
            mainDivClass: 'row',
            monthDivClass: 'col-xs-6',
            dayDivClass: 'col-xs-3',
            yearDivClass: 'col-xs-3',
            monthClass: 'form-control select2',
            dayClass: 'form-control',
            yearClass: 'form-control',
            locale: {'format': 'DD-MM-YYYY'},
        });

        date_last_contact.dateline({
            mainDivClass: 'row',
            monthDivClass: 'col-xs-6',
            dayDivClass: 'col-xs-3',
            yearDivClass: 'col-xs-3',
            monthClass: 'form-control select2',
            dayClass: 'form-control',
            yearClass: 'form-control',
            locale: {'format': 'DD-MM-YYYY'},
        });

        date_deceased.dateline({
            mainDivClass: 'row',
            monthDivClass: 'col-xs-5',
            dayDivClass: 'col-xs-3',
            yearDivClass: 'col-xs-4',
            monthClass: 'form-control select2',
            dayClass: 'form-control',
            yearClass: 'form-control',
            locale: {'format': 'DD-MM-YYYY'},
        });

        function format_placeholder(option_data) {
            if (option_data.id === '') {
                return $('<span style="opacity: 0.8;">' + option_data.text + '</span>');
            } else {
                return option_data.text;
            }
        }
        $('.select2').select2({
            autoclose: true,
            width: '100%',
            minimumResultsForSearch: 10,
            templateSelection: format_placeholder,
            templateResult: format_placeholder
        });
        $('select.select2').overrideSelect2Keys();
        allergies.select2({
            autoclose: true,
            width: '100%',
            minimumResultsForSearch: 10,
            templateSelection: format_placeholder,
            templateResult: format_placeholder
        });
        facilities.select2({
            autoclose: true,
            width: '100%',
            minimumResultsForSearch: 10,
            templateSelection: format_placeholder,
            templateResult: format_placeholder
        });

        mrn.focus();

        function initAutocomplete() {
            // Create the autocomplete object, restricting the search to geographical
            // location types.
            var center = new google.maps.LatLng(45.4, -75.6);
            var circle = new google.maps.Circle({
                center: center,
                radius: 100000
            });

            home_address_autocomplete = new google.maps.places.Autocomplete(
                home_address[0],
                {
                    types: ['geocode'],
                }
            );
            home_address_autocomplete.setBounds(circle.getBounds());

            // When the user selects an address from the dropdown, populate the address
            // fields in the form.
            // home_address_autocomplete.addListener('place_changed', fillInAddress);
        }
        initAutocomplete();


    });
});