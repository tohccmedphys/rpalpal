require([
    'jquery', 'knockout', 'moment', 'd3', 'base', 'select2', 'daterangepicker',
    'async!https://maps.googleapis.com/maps/api/js?key=' + SiteConfig.google_api_key
], function($, ko, moment, d3) {

    d3.selection.prototype.moveToFront = function () {
        return this.each(function () {
            this.parentNode.appendChild(this);
        });
    };

    $(document).ready(function() {

        var $daterange_period = $('#daterange-period'),
            $dl_poc = $('#dl_poc'),
            $dl_rc = $('#dl_rc'),
            $dl_dc = $('#dl_dc'),
            $dl_p = $('#dl_p'),
            $dl_tc = $('#dl_tc');

        var data_cache = {};

        var ranges = {
            "Last 6 Months": [
                moment().subtract(5, 'months').startOf('month'),
                moment().endOf('month')
            ],
            "Last 12 Months": [
                moment().subtract(11, 'months').startOf('month'),
                moment().endOf('month')
            ],
            "Year To Date": [
                moment().startOf('year'),
                moment().endOf('month')
            ],
            "Previous Year": [
                moment().subtract(1, 'years').startOf('year'),
                moment().subtract(1, 'years').endOf('year')
            ],
            "Last 2 Years": [
                moment().subtract(23, 'months').startOf('month'),
                moment().endOf('month')
            ],
            "All Time": [
                moment().year(oldest_moment.year).month(oldest_moment.month).startOf('month'),
                moment().endOf('month')
            ]
        },
            total_days;

        get_stat_data = function(date_from, date_to) {
            total_days = moment.duration(
                moment(date_from).diff(moment(date_to))
            ).asDays();
            if (date_from && date_to) {
                var cache_string = date_from.format('DD-MM-YYYY') + '_' + date_to.format('DD-MM-YYYY');
                if (cache_string in data_cache) {
                    update_charts(data_cache[cache_string])
                } else {
                    $.ajax({
                        url: RPP_URLs.get_stat_data,
                        data: {
                            date_from: date_from.format('DD-MM-YYYY'),
                            date_to: date_to.format('DD-MM-YYYY')
                        },
                        success: function (res) {
                            data_cache[cache_string] = prep_data(res.data);
                            update_charts(data_cache[cache_string]);
                        }
                    })
                }
            }
        };

        $daterange_period.daterangepicker({
            autoClose: true,
            autoApply: true,
            opens: 'right',
            locale: {'format': 'MMM YYYY'},
            linkedCalendars: false,
            ranges: ranges,
            startDate: moment().subtract(11, 'months').startOf('month'),
            endDate: moment().endOf('month')
        }, get_stat_data);

        get_stat_data(
            $daterange_period.data('daterangepicker').startDate, $daterange_period.data('daterangepicker').endDate
        );
        function get_date_url_string() {
            var date_from = $daterange_period.data('daterangepicker').startDate.format('DD-MM-YYYY');
            var date_to = $daterange_period.data('daterangepicker').endDate.format('DD-MM-YYYY');
            return '?date_from=' + date_from + '&date_to=' + date_to;
        }

        $dl_poc.click(function() {
            window.location = stats_urls.DL_POC + get_date_url_string();
        });
        $dl_tc.click(function() {
            window.location = stats_urls.DL_TC + get_date_url_string();
        });
        $dl_rc.click(function() {
            window.location = stats_urls.DL_RC + get_date_url_string();
        });
        $dl_dc.click(function() {
            window.location = stats_urls.DL_DC + get_date_url_string();
        });
        $dl_p.click(function() {
            window.location = stats_urls.DL_P + get_date_url_string();
        });

        function prep_data(_data) {
            $.each(_data.monthly_data, function(i, v1) {

                // poc durations
                var count = 0;
                _data.monthly_data[i].avg_poc_duration = d3.sum(
                    v1.duration_travel_poc_dict, function(d) {
                        count += d.duration_travel ? 1 : 0;
                        return d.duration_travel;
                    }
                ) / count / 60;

                if (!_data.monthly_data[i].avg_poc_duration) {
                    _data.monthly_data[i].avg_poc_duration = 0;
                }

                // tc durations
                count = 0;
                _data.monthly_data[i].avg_tc_duration = d3.sum(
                    v1.duration_travel_tc_dict, function(d) {
                        count += d.duration_travel ? d.fractions_override : 0;
                        return d.duration_travel * d.fractions_override;
                    }
                ) / count / 60;

                if (!_data.monthly_data[i].avg_tc_duration) {
                    _data.monthly_data[i].avg_tc_duration = 0;
                }
            });

            console.log(_data);
            return _data;
        }

        function update_charts(data) {
            console.log('--- update charts');
            update_dt(data);
            display_markers(data.coming_from_data);
            console.log(data)
            update_demographic_sliders(data.demographics_data);
        }

        var tooltip = d3.select('body').append('div').attr('class', 'chart-tooltip');

        // Demographics ////////////////////////////////////////////////////////////////////////////////////

        // Demographics slider properties
        var dsp = {
            width: 400,
            height: 100,
            bar_space: 5
        };

        dsp.bar_height = dsp.height / 4 - dsp.bar_space;
        dsp.margin = {top: 15, right: 15, bottom: 15, left: 15};
        dsp.x_scale = d3.scaleLinear()
            .domain([0, 1])
            .rangeRound([0, dsp.width]);
        dsp.svg = d3.select('#demographics-sliders-chart').append('svg')
            .attr('width', dsp.width + dsp.margin.left + dsp.margin.right)
            .attr('height', dsp.height + dsp.margin.top + dsp.margin.bottom)
          .append('g')
            .attr('transform', 'translate(' + dsp.margin.left + ',' + dsp.margin.top + ')');


        function update_demographic_sliders(data) {

            var number_male_patients = d3.sum(data.patient_data, function(d) { return d.gender === 'm'; }),
                number_patients = data.patient_data.length,
                ratio_male = number_male_patients / number_patients;

            dsp.gender_bar = dsp.svg.selectAll('.gender-bar')
                .data(
                    [{ratio: ratio_male, chart: 'male'}, {ratio: 1 - ratio_male, chart: 'female'}],
                    function (d) { return d.chart; }
                );
            dsp.gender_bar.select('.bar').transition()
                .attr('width', function(d) { return d.ratio * dsp.width; })
                .attr('x', function(d) { return d.chart === 'male' ? 0 : (1 - d.ratio) * dsp.width; });

            var gender_g = dsp.gender_bar.enter()
                .append('g')
                .attr('class', 'gender-bar');
            gender_g.append('rect')
                .style('fill-opacity', 1e-6)
                .attr('x', function(d) { return d.chart === 'male' ? 0 : (1 - d.ratio) * dsp.width; })
                .attr('class', function(d) { return d.chart === 'male' ? 'bar bar-02' : 'bar bar-01'})
                .attr('width', function(d) { return d.ratio * dsp.width; })
                .attr('height', dsp.bar_height)
              .transition()
                .style('fill-opacity', 1);
            gender_g.append('text')
                .attr('transform', 'translate(5,15)')
                .attr('class', 'chart-label white')
                .attr('x', function(d) { return d.chart === 'male' ? 5 : dsp.width - 95; })
                .text(function(d) {
                    return (d.chart === 'male' ? 'Male ' : 'Female ') + (Math.round(d.ratio * 10000) / 100) + '%'
                })
              .transition()
                .style('fill-opacity', 1);
            dsp.gender_bar.select('text').transition()
                .style('opacity', 1e-6)
              .transition()
                .text(function(d) {
                    return (d.chart === 'male' ? 'Male ' : 'Female ') + (Math.round(d.ratio * 10000) / 100) + '%'
                })
                .style('opacity', 1);

            var number_in_patients = d3.sum(data.visit_data, function(d) { return d.in_out === 'in'; }),
                number_visits = data.visit_data.length,
                ratio_in = number_in_patients / number_visits;

            dsp.inout_bar = dsp.svg.selectAll('.inout-bar')
                .data(
                    [{ratio: ratio_in, chart: 'in'}, {ratio: 1 - ratio_in, chart: 'out'}],
                    function (d) { return d.chart; }
                );
            dsp.inout_bar.select('.bar').transition()
                .attr('width', function(d) { return d.ratio * dsp.width; })
                .attr('x', function(d) { return d.chart === 'in' ? 0 : (1 - d.ratio) * dsp.width; });

            var inout_g = dsp.inout_bar.enter()
                .append('g')
                .attr('transform', 'translate(0,' + (dsp.bar_space + dsp.bar_height) + ')')
                .attr('class', 'inout-bar');
            inout_g.append('rect')
                .style('fill-opacity', 1e-6)
                .attr('x', function(d) { return d.chart === 'in' ? 0 : (1 - d.ratio) * dsp.width; })
                .attr('class', function(d) { return d.chart === 'in' ? 'bar bar-02' : 'bar bar-01'})
                .attr('width', function(d) { return d.ratio * dsp.width; })
                .attr('height', dsp.bar_height)
              .transition()
                .style('fill-opacity', 1);
            inout_g.append('text')
                .attr('transform', 'translate(5,15)')
                .attr('class', 'chart-label white')
                .attr('x', function(d) { return d.chart === 'in' ? 5 : dsp.width - 110; })
                .text(function(d) {
                    return (d.chart === 'in' ? 'Inpatient ' : 'Outpatient ') + (Math.round(d.ratio * 10000) / 100) + '%'
                })
              .transition()
                .style('fill-opacity', 1);
            dsp.inout_bar.select('text').transition()
                .style('opacity', 1e-6)
              .transition()
                .text(function(d) {
                    return (d.chart === 'in' ? 'Inpatient ' : 'Outpatient ') + (Math.round(d.ratio * 10000) / 100) + '%'
                })
                .style('opacity', 1);

        }

        // Duration travel /////////////////////////////////////////////////////////////////////////////////////////////
        var dt_props = {
            margin: {top: 0, right: 0, bottom: 0, left: 0},
            width: 0,
            height: 0,
            x_scale: '',
            old_x_scale: '',
            x_axis: '',
            y_scale: '',
            bar_padding: 3,
            svg: '',
            poc_bars: '',
            tc_bars: '',
            y_max: 0,
            y_buff: 0,
            legend: ''
        };

        dt_props.margin = {top: 10, right: 30, bottom: 30, left: 50};
        dt_props.width = $('#patient-travel-chart').width() - dt_props.margin.left - dt_props.margin.right;
        dt_props.height = 200 - dt_props.margin.top - dt_props.margin.bottom;
        dt_props.x_scale = d3.scaleTime()
            .domain([$daterange_period.data('daterangepicker').startDate, $daterange_period.data('daterangepicker').endDate])
            .rangeRound([0, dt_props.width]);
        dt_props.y_scale = d3.scaleLinear()
            .range([dt_props.height, 0]);
        dt_props.x_axis = d3.axisBottom(dt_props.x_scale);
        dt_props.y_axis = d3.axisLeft(dt_props.y_scale);
        dt_props.svg = d3.select('#patient-travel-chart').append('svg')
                .attr('width', dt_props.width + dt_props.margin.left + dt_props.margin.right)
                .attr('height', dt_props.height + dt_props.margin.top + dt_props.margin.bottom)
              .append('g')
                .attr('transform', 'translate(' + dt_props.margin.left + ',' + dt_props.margin.top + ')');

        dt_props.x_axis_elem = dt_props.svg.append('g')
            .attr('transform', 'translate(0,' + dt_props.height + ')')
            .call(dt_props.x_axis);
        dt_props.y_axis_elem = dt_props.svg.append('g').attr('class', 'y-axis-elem')
            // .attr('transform', 'translate(0,' + dt_props.height + ')')
            .call(dt_props.y_axis);
        dt_props.svg.append('text')
            .attr('class', 'chart-label')
            .attr('transform', 'rotate(-90)')
            .attr('y', 0 - dt_props.margin.left)
            .attr('x', 0 - (dt_props.height / 2))
            .attr('dy', '1em')
            .style('text-anchor', 'middle')
            .text('Duration traveled (min)');
        dt_props.legend = d3.select('#dt_legend')
            .append('svg')
            .attr('class', 'legend dt-legend');
        var tc_legend = dt_props.legend.append('g')
            .attr('transform', 'translate(0,15)')
            .attr('class', 'legend-item');
        var poc_legend = dt_props.legend.append('g')
            .attr('class', 'legend-item');

        poc_legend.append('rect')
            .attr('class', 'legend-icon _01');
        tc_legend.append('rect')
            .attr('class', 'legend-icon _02');
        poc_legend.append('text')
            .text('Point Of Contact Travel Durations')
            .attr('x', 15)
            .attr('y', 10)
            .attr('class', 'legend-label');
        tc_legend.append('text')
            .text('Treatment Course Travel Durations')
            .attr('x', 15)
            .attr('y', 10)
            .attr('class', 'legend-label');

        function update_dt(data) {

            // Monthly
            dt_props.old_x_scale = dt_props.x_scale.copy();
            dt_props.y_max = d3.max([
                d3.max(data.monthly_data, function(d) { return d.avg_poc_duration; }),
                d3.max(data.monthly_data, function(d) { return d.avg_tc_duration; })
            ]);
            dt_props.y_buff = dt_props.y_max * 0.1;

            dt_props.bar_width = dt_props.width / data.monthly_data.length;
            dt_props.x_scale.domain([
                moment($daterange_period.data('daterangepicker').startDate).startOf('month')._d,
                $daterange_period.data('daterangepicker').endDate.endOf('month')._d
            ]);
            dt_props.y_scale.domain([0, dt_props.y_max + dt_props.y_buff]);

            dt_props.poc_bars = dt_props.svg.selectAll('.bar-01')
                .data(data.monthly_data, function(d) { return d.month; });

            dt_props.poc_bars.transition()
                .attr('x', function(d) {
                    return dt_props.x_scale(moment(d.month, 'YYYY-MM')._d);
                })
                .attr('width', function(d) { return get_month_bar_width(d, dt_props.width / 2) + 1; })
                .attr('y', function(d) { return dt_props.y_scale(d.avg_poc_duration); })
                .attr('height', function(d) { return dt_props.height - dt_props.y_scale(d.avg_poc_duration); });

            dt_props.poc_bars.enter().append('rect')
                .style('fill-opacity', 1e-6)
                .attr('class', 'bar bar-01')
                .attr('x', function(d) { return dt_props.old_x_scale(moment(d.month, 'YYYY-MM')._d); })
                .attr('width', function(d) { return get_month_bar_width(d, dt_props.width / 2) + 1; })
                .attr('y', function(d) { return dt_props.y_scale(d.avg_poc_duration); })
                .attr('height', function(d) { return dt_props.height - dt_props.y_scale(d.avg_poc_duration); })
                .on('mousemove', function(d){
                    tooltip
                      .style('left', d3.event.pageX - 50 + 'px')
                      .style('top', d3.event.pageY - 80 + 'px')
                      .style('display', 'inline-block')
                      .html(d.month + '<br>' + Math.round(d.avg_poc_duration) + ' min');
                })
                .on('mouseout', function(d){ tooltip.style('display', 'none'); })
                    .transition()
                    .style('fill-opacity', 1)
                    .attr('x', function(d) { return dt_props.x_scale(moment(d.month, 'YYYY-MM')._d); })
                    .style('fill-opacity', 1);
                // .attr('width', function(d) { return get_bar_width(d, dt_props.width) - 1; });

            dt_props.poc_bars.exit().transition()
                .style('fill-opacity', 1e-6)
                .attr('x', function(d) { return dt_props.x_scale(moment(d.month, 'YYYY-MM')._d); })
                .remove();

            dt_props.tc_bars = dt_props.svg.selectAll('.bar-02')
                .data(data.monthly_data, function(d) { return d.month; });

            dt_props.tc_bars.transition()
                .attr('x', function(d) {
                    return dt_props.x_scale(moment(d.month, 'YYYY-MM')._d) + get_month_bar_width(d, dt_props.width / 2);
                })
                .attr('width', function(d) { return get_month_bar_width(d, dt_props.width / 2) + 1; })
                .attr('y', function(d) { return dt_props.y_scale(d.avg_tc_duration); })
                .attr('height', function(d) { return dt_props.height - dt_props.y_scale(d.avg_tc_duration); });

            dt_props.tc_bars.enter().append('rect')
                .style('fill-opacity', 1e-6)
                .attr('class', 'bar bar-02')
                .attr('x', function(d) {
                    return dt_props.old_x_scale(moment(d.month, 'YYYY-MM')._d) + get_month_bar_width(d, dt_props.width / 2);
                })
                .attr('width', function(d) { return get_month_bar_width(d, dt_props.width / 2) + 1; })
                .attr('y', function(d) { return dt_props.y_scale(d.avg_tc_duration); })
                .attr('height', function(d) { return dt_props.height - dt_props.y_scale(d.avg_tc_duration); })
                .on('mousemove', function(d){
                    tooltip
                      .style('left', d3.event.pageX - 50 + 'px')
                      .style('top', d3.event.pageY - 80 + 'px')
                      .style('display', 'inline-block')
                      .html(d.month + '<br>' + Math.round(d.avg_tc_duration) + ' min');
                })
                .on('mouseout', function(d){ tooltip.style('display', 'none'); })
                .transition()
                .style('fill-opacity', 1)
                .attr('x', function(d) {
                    return dt_props.x_scale(moment(d.month, 'YYYY-MM')._d) + get_month_bar_width(d, dt_props.width / 2); })
                .style('fill-opacity', 1);
                // .attr('width', function(d) { return get_bar_width(d, dt_props.width) - 1; });

            dt_props.tc_bars.exit().transition()
                .style('fill-opacity', 1e-6)
                .attr('x', function(d) {
                    return dt_props.x_scale(moment(d.month, 'YYYY-MM')._d) + get_month_bar_width(d, dt_props.width / 2);
                })
                .remove();

            dt_props.x_axis_elem.transition().call(dt_props.x_axis);
            dt_props.y_axis_elem.transition().call(dt_props.y_axis.tickSize(-dt_props.width));
        }

        function get_month_bar_width(d, width) {
            var start_moment = moment(d.month, 'YYYY-MM').startOf('month'),
                end_moment = moment(d.month, 'YYYY-MM').endOf('month');
            return width * moment.duration(start_moment.diff(end_moment)).asDays() / total_days;
        }

        // Travel Map ////////////////////////////////////////////////////////////////////////////////////////////////
        var map;
        function initMap() {
            map = new google.maps.Map(document.getElementById('patient-travel-map'), {
                center: {lat: 45.401388, lng: -75.646266},
                zoom: 8
            });
        }

        var markers = [],
            bounds = new google.maps.LatLngBounds();

        function display_markers(coming_from_data) {
            console.log('----- display markers');
            $.each(markers, function(i, v) {
                v.setMap(null);
            });
            markers = [];

            $.each(coming_from_data.poc, function(i, v) {
                displayLatLng(v, 'Point of contact');
            });
            $.each(coming_from_data.tc, function(i, v) {
                displayLatLng(v, 'Treatment');
            });
            map.fitBounds(bounds);
        }
        function displayLatLng(data, type) {
            var position = {lat: parseFloat(data.lat_lng.split(',')[0]), lng: parseFloat(data.lat_lng.split(',')[1])},
                contentString = '<div id="content">' +
                    '<div id="siteNotice">' +
                    '</div>' +
                    '<h4 id="firstHeading" class="firstHeading">Patient ' + data.mrn + '</h4>' +
                    '<div id="bodyContent">' +
                    '    <b>Coming from:</b> ' + data.address + '<br><b>Travel time:</b> ' +
                    (data.duration_travel < 60 || !data.duration_travel ? 'Unknown' : moment.duration(data.duration_travel, 'seconds').humanize()) +
                    '</div></div>';

                var infowindow = new google.maps.InfoWindow({
                    content: contentString,
                    maxWidth: 200
                });


            var marker = new google.maps.Marker({
                map: map,
                position: position
            });
            google.maps.event.addListener(marker, 'mouseover', function (e) {
                infowindow.open(map, marker);
            });
            google.maps.event.addListener(marker, 'mouseout', function (e) {
                infowindow.close();
            });
            bounds.extend(marker.getPosition());
            markers.push(marker);
        }
        initMap();


    })

});