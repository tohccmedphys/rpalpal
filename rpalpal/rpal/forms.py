
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, ButtonHolder, Submit, Div, Field
from crispy_forms.utils import render_field
from django.conf import settings
from django.core.exceptions import ValidationError
import django.forms as forms
from django.template.loader import render_to_string
from django.utils.dateparse import parse_duration
from django.utils.encoding import force_str
from django.utils import timezone

from rpal import models as rp_models


def duration_string_hours_mins(duration):

    seconds = duration.seconds
    minutes = seconds // 60

    hours = minutes // 60
    minutes %= 60
    hours %= 60

    if seconds > 0 and minutes < 1 and hours == 0:
        return '00:01'

    return '{:02d}:{:02d}'.format(hours, minutes)


class RPalFormHelper(FormHelper):

    form_tag = False
    field_template = 'field_template.html'


class RPalField(Field):

    template = 'field_template.html'

    def get_rendered_fields(self, form, context, template_pack=None, **kwargs):

        to_return = ''
        for field in self.fields:
            if field in form.fields:
                if 'attrs' not in kwargs:
                    kwargs['attrs'] = {}
                if getattr(form.fields[field], 'required'):
                    kwargs['attrs']['placeholder'] = 'required'
                kwargs['attrs']['title'] = form._meta.model._meta.get_field(field).help_text
                to_return += render_field(field, form, context, template_pack=template_pack, **kwargs)

        return to_return


class HoursMinDurationField(forms.DurationField):

    def __init__(self, *args, **kwargs):
        super(HoursMinDurationField, self).__init__(*args, **kwargs)
        self.widget.attrs.update({'class': 'inputmask'})

    def prepare_value(self, value):
        if isinstance(value, timezone.timedelta):
            return duration_string_hours_mins(value)
        return value

    def to_python(self, value):

        if value is not None:
            if value == '__:__':
                return None
            value = value.replace('_', '0').replace(':', '')
        if value in self.empty_values:
            return None
        if isinstance(value, timezone.timedelta):
            return value
        value = '{:04d}'.format(int(value))
        value = parse_duration(force_str(':'.join([value[:2], value[2:], '00'])))
        if value is None:
            raise ValidationError(self.error_messages['invalid'], code='invalid')

        return value


class ModelChoiceFieldFullQueryset(forms.ModelChoiceField):

    def to_python(self, value):

        if value in self.empty_values:
            return None
        try:
            key = self.to_field_name or 'pk'
            value = self.queryset.model.objects.get(**{key: value})
        except (ValueError, TypeError, self.queryset.model.DoesNotExist):
            raise ValidationError(self.error_messages['invalid_choice'], code='invalid_choice')
        return value


class ExtraContextDiv(Div):

    def render(self, form, context, template_pack=None, **kwargs):
        fields = self.get_rendered_fields(form, context, template_pack, **kwargs)

        template = self.get_template_name(template_pack)
        return render_to_string(template, {'div': self, 'fields': fields, 'form': form})


class PatientForm(forms.ModelForm):

    class Meta:
        model = rp_models.Patient
        exclude = ['datetime_entered']

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        super().__init__(*args, **kwargs)

        is_new = self.instance.pk is None

        for f in ['date_birth', 'date_deceased']:
            self.fields[f].input_formats = settings.INPUT_DATE_FORMATS
            self.fields[f].widget.format = settings.INPUT_DATE_FORMATS[0]
            self.fields[f].widget.attrs.update({'autocomplete': 'off'})

        for f in [
            'first_name', 'middle_name', 'last_name', 'phone_number', 'facilities', 'allergies', 'mrn',
            'date_birth', 'gender', 'home_address', 'date_deceased'
        ]:
            self.fields[f].widget.attrs['class'] = 'form-control'
            self.fields[f].widget.attrs['autocomplete'] = 'new-password'

    def clean_date_deceased(self):
        if self.cleaned_data['deceased'] and self.cleaned_data['date_deceased'] is None:
            raise forms.ValidationError('Must enter date of death')
        elif not self.cleaned_data['deceased']:
            self.cleaned_data['date_deceased'] = None
        return self.cleaned_data['date_deceased']


class ReferralForm(forms.ModelForm):

    class Meta:
        model = rp_models.Referral
        fields = [
            'patient', 'date_referred', 'referring_physician', 'physician_contacted_by', 'referring_communication_mode',
            'referral_reach', 'referral_reasons', 'referral_location'
        ]

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        super().__init__(*args, **kwargs)

        self.helper = RPalFormHelper()
        self.helper.form_id = 'referral_form'
        self.helper.attrs = {'novalidate': 'novalidate'}
        self.helper.layout = Layout(
            Div(
                Div(
                    Div(
                        RPalField('patient', type='hidden', id='id_patient'),
                        RPalField('date_referred', id='id_date_referred', css_class='form-control'),
                        RPalField('referring_physician', id='id_referring_physician', css_class='form-control select2', title=''),
                        RPalField('referring_communication_mode', id='id_referring_communication_mode', css_class='form-control select2'),
                        RPalField('referral_reach', id='id_referral_reach', css_class='form-control select2', title=''),
                        RPalField('referral_reasons', id='id_referral_reasons', css_class='form-control select2', title=''),
                        css_class='form-horizontal',
                    ),
                    css_class='col-sm-12'
                ),
                css_class='row',
            ),
        )

        for f in ['date_referred']:
            self.fields[f].input_formats = settings.INPUT_DATE_FORMATS
            self.fields[f].widget.format = settings.INPUT_DATE_FORMATS[0]
            self.fields[f].widget.attrs['class'] = 'form-control'
            self.fields[f].widget.attrs.update({'autocomplete': 'new-password'})

        # add css here for when form is not used with crispy tag
        for f in ['referring_communication_mode', 'referral_reach', 'referring_physician', 'referral_location', 'physician_contacted_by']:
            self.fields[f].widget.attrs['class'] = 'form-control select2-referral'

        for f in ['referral_reasons']:
            self.fields[f].widget.attrs['class'] = 'form-control select2-referral-multi'

        for f in ['patient']:
            self.fields[f].widget = forms.HiddenInput()


class DiagnosisForm(forms.ModelForm):

    class Meta:
        model = rp_models.Diagnosis
        fields = [
            'patient', 'primary_cancer_site', 'pathologic_diagnosis',  'date_diagnosis',
            'radiographic_diagnosis_explanation', 'primary_cancer_dx_description', 'primary_diagnosis_group'
        ]

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        super().__init__(*args, **kwargs)

        is_new = self.instance.pk is None

        for f in ['date_diagnosis']:
            self.fields[f].input_formats = settings.INPUT_DATE_FORMATS
            self.fields[f].widget.format = settings.INPUT_DATE_FORMATS[0]
            self.fields[f].widget.attrs['class'] = 'form-control'
            self.fields[f].widget.attrs.update({'autocomplete': 'new-password'})

        # add css here for when form is not used with crispy tag
        self.fields['primary_cancer_site'].widget.attrs['class'] = 'form-control select2-referral'
        self.fields['primary_diagnosis_group'].widget.attrs['class'] = 'form-control select2-referral'
        self.fields['radiographic_diagnosis_explanation'].widget.attrs['class'] = 'form-control autosize'
        self.fields['radiographic_diagnosis_explanation'].widget.attrs['rows'] = 3
        self.fields['primary_cancer_dx_description'].widget.attrs['class'] = 'form-control autosize'
        self.fields['primary_cancer_dx_description'].widget.attrs['rows'] = 3
        self.fields['patient'].widget = forms.HiddenInput()


class MetaStaticCancerSiteDiagnosisCollectionForm(forms.ModelForm):

    class Meta:
        model = rp_models.MetaStaticCancerSiteDiagnosisCollection
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # self.fields['date'].widget.attrs['class'] = 'form-control csm-date not-dated'
        self.fields['date'].input_formats = settings.INPUT_DATE_FORMATS
        self.fields['date'].widget.format = settings.INPUT_DATE_FORMATS[0]
        self.fields['date'].widget.attrs.update({'autocomplete': 'new-password'})
        # self.fields['metastatic_cancer_site'].widget.attrs['class'] = 'form-control csm-select2 not-select2'


MetaStaticCancerSiteDiagnosisCollectioFormset = forms.inlineformset_factory(
    rp_models.Diagnosis, rp_models.MetaStaticCancerSiteDiagnosisCollection,
    form=MetaStaticCancerSiteDiagnosisCollectionForm, extra=0
)


class VisitForm(forms.ModelForm):

    duration_travel = HoursMinDurationField(required=False)
    ministry_of_transportation_status = forms.ChoiceField(
        required=False,
        choices=[(None, '--------')] + [(mots.name, mots.name) for mots in rp_models.MinistryOfTransportationStatus.objects.all()]
    )

    class Meta:
        model = rp_models.Visit
        exclude = ['is_first_visit', 'relative_id', 'coming_from_lat_lng']

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        super().__init__(*args, **kwargs)

        is_new = self.instance.pk is None

        for f in ['date']:
            self.fields[f].input_formats = settings.INPUT_DATE_FORMATS
            self.fields[f].widget.format = settings.INPUT_DATE_FORMATS[0]
            self.fields[f].widget.attrs['class'] = 'form-control'
            self.fields[f].widget.attrs.update({'autocomplete': 'new-password'})

        for f in [
            'patient', 'coming_from', 'duration_travel', 'transport_mode', 'person_consent',
            'location', 'stage', 'code_status', 'life_expectancy', 'mental_status', 'medications',
            'medications_modified', 'services_referred', 'accompanying_people', 'ecog', 'pps', 'rad_onc',
            'ministry_of_transportation_status', 'frailty', 'point_of_contact_mode', 'referral', 'in_out'
        ]:
            self.fields[f].widget.attrs['class'] = 'form-control'


class TreatmentCourseForm(forms.ModelForm):

    duration_travel = HoursMinDurationField(required=False)
    visit = ModelChoiceFieldFullQueryset(required=False, queryset=rp_models.Visit.objects.none())

    class Meta:
        model = rp_models.TreatmentCourse
        exclude = ['relative_id', 'coming_from_lat_lng']

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        super().__init__(*args, **kwargs)

        is_new = self.instance.pk is None

        for f in [
            'visit', 'care_plan', 'dose_override', 'fractions_override', 'coming_from', 'duration_travel',
            'explanation', 'ct_date', 'structure_treated'
        ]:
            self.fields[f].widget.attrs['class'] = 'form-control'

        for f in ['date_start', 'date_end', 'ct_date']:
            self.fields[f].input_formats = settings.INPUT_DATE_FORMATS
            self.fields[f].widget.format = settings.INPUT_DATE_FORMATS[0]
            self.fields[f].widget.attrs.update({'autocomplete': 'new-password'})

        self.fields['explanation'].widget.attrs['rows'] = 3

    def save(self, commit=True):
        treatment_course = super().save(commit=False)
        if commit:
            treatment_course.save()
            if 'bone_mets_complexity' in self.changed_data:
                final_bmc = self.cleaned_data.get(
                    'bone_mets_complexity', rp_models.BoneMetsComplexity.objects.none()
                ).all()
                initial_bmc = self.initial.get('bone_mets_complexity', [])

                # create and save new bone mets
                for bmc in final_bmc:
                    if bmc not in initial_bmc:
                        rp_models.TreatmentCourseBoneMetsComplexityCollection.objects.create(
                            bone_mets_complexity=bmc, treatment_course=treatment_course
                        )

                # delete old bone mets that were removed from the form
                for bmc in initial_bmc:
                    if bmc not in final_bmc:
                        rp_models.TreatmentCourseBoneMetsComplexityCollection.objects.filter(
                            bone_mets_complexity=bmc, treatment_course=treatment_course
                        ).delete()

        return treatment_course


class CareplanForm(forms.ModelForm):

    class Meta:
        model = rp_models.CarePlan
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        super().__init__(*args, **kwargs)

        is_new = self.instance.pk is None

        self.helper = RPalFormHelper()
        self.helper.layout = Layout(
            Div(
                Div(
                    RPalField('name', id='id_cp_name', css_class='form-control'),
                    # RPalField('structures_treated', id='id_cp_structures_treated', css_class='form-control'),
                    RPalField('dose', id='id_cp_dose', css_class='form-control'),
                    RPalField('fractions', id='id_cp_fractions', css_class='form-control'),
                    RPalField('type', id='id_cp_type', css_class='form-control select2'),
                    RPalField('complexity', id='id_cp_complexity', css_class='form-control select2'),
                    RPalField('pattern', id='id_cp_pattern', css_class='form-control select2'),
                    ButtonHolder(
                        Submit('submit', 'Enter New Care Plan' if is_new else 'Submit Changes',
                               css_class='btn btn-flat btn-primary float-right'),
                    ),
                    css_class='col-md-12 form-horizontal',
                ),

                css_class='row'
            )
        )


class PhysicianForm(forms.ModelForm):

    class Meta:
        model = rp_models.Physician
        exclude = ['datetime_entered']

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        super().__init__(*args, **kwargs)

        is_new = self.instance.pk is None

        for f in ['first_name', 'last_name', 'email', 'phone_number', 'physician_specialty', 'text_number', 'fax_number']:
            self.fields[f].widget.attrs['class'] = 'form-control'
            self.fields[f].widget.attrs['autocomplete'] = 'new-password'
