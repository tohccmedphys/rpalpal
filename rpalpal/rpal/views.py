
import calendar
import csv

from braces.views import LoginRequiredMixin
from dateutil.rrule import rrule, MONTHLY
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.forms.utils import timezone
from django.http import HttpResponseRedirect, Http404, JsonResponse, HttpResponse
from django.template import Context, Template
from django.template.loader import get_template
from django.utils.encoding import smart_str
from django.utils.translation import gettext as _
from django.urls import reverse, resolve
from django.views.generic import TemplateView
from django.views.generic.detail import SingleObjectTemplateResponseMixin
from django.views.generic.edit import ModelFormMixin, ProcessFormView

from listable.views import (
    BaseListableView, DATE_RANGE, SELECT, SELECT_MULTI, NONEORNULL, TEXT, SELECT_MULTI_FROM_MULTI,
    TODAY, YESTERDAY, TOMORROW, LAST_WEEK, THIS_WEEK, NEXT_WEEK, LAST_14_DAYS, LAST_MONTH, THIS_MONTH, THIS_YEAR
)

from rpal import models as rp_models
from rpal import forms as rp_forms


class UpdateCreatePatient(LoginRequiredMixin, SingleObjectTemplateResponseMixin, ModelFormMixin, ProcessFormView):
    """
    CreateView and UpdateView functionality combined
    """

    model = rp_models.Patient
    template_name = 'rpal/patient_form.html'
    form_class = rp_forms.PatientForm

    def dispatch(self, request, *args, **kwargs):
        self.user = request.user
        return super().dispatch(request, *args, **kwargs)

    def get_object(self, queryset=None):
        """
        Returns the object the view is displaying.

        By default this requires `self.queryset` and a `pk` or `slug` argument
        in the URLconf, but subclasses can override this to return any object.
        """
        # Use a custom queryset if provided; this is required for subclasses
        # like DateDetailView
        if queryset is None:
            queryset = self.get_queryset()

        # Next, try looking up by primary key.
        pk = self.kwargs.get(self.pk_url_kwarg)
        slug = self.kwargs.get(self.slug_url_kwarg)
        if pk is not None:
            # queryset = queryset.filter(pk=pk).select_related(
            #     'test_list_instance_initiated_by',
            # ).prefetch_related(
            #     'qafollowup_set',
            #     'test_list_instance_initiated_by__testinstance_set',
            #     'test_list_instance_initiated_by__testinstance_set__status',
            #     'test_list_instance_initiated_by__unit_test_collection__tests_object'
            # )
            queryset = queryset.filter(pk=pk)

        # If none of those are defined, it's an error.
        if pk is None and slug is None:
            return None

        try:
            # Get the single item from the filtered queryset
            obj = queryset.get()
        except queryset.model.DoesNotExist:
            raise Http404(_("No %(verbose_name)s found matching the query") %
                          {'verbose_name': queryset.model._meta.verbose_name})
        return obj

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().post(request, *args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['user'] = self.user
        return kwargs

    def get_context_data(self, *args, **kwargs):
        context_data = super().get_context_data(**kwargs)
        return context_data

    def form_valid(self, form):
        patient = form.save()
        return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, form):
        return super().form_invalid(form)

    def get_success_url(self):
        next_ = self.request.GET.get("next", None)
        if next_ is not None:
            return next_

        return reverse('p_list')


class CreatePatient(UpdateCreatePatient):

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        return kwargs

    def form_valid(self, form):
        self.instance = form.save(commit=False)
        self.instance.datetime_entered = timezone.now()
        return super().form_valid(form)


class UpdatePatient(UpdateCreatePatient):

    def form_valid(self, form):

        self.instance = form.save(commit=False)
        return super().form_valid(form)


class PatientDetails(LoginRequiredMixin, TemplateView):

    template_name = 'rpal/patient_details.html'

    def get_context_data(self, *args, **kwargs):
        context_data = super().get_context_data(**kwargs)
        if self.request.method == 'GET':
            patient_id = self.request.GET.get('p_id', False)
            if patient_id:
                patient = rp_models.Patient.objects.get(pk=patient_id)
                context_data['patient'] = {
                    'id': patient_id,
                    'mrn': patient.mrn,
                    'first_name': patient.first_name,
                    'last_name': patient.last_name
                }
            else:
                context_data['patient'] = 'false'
            context_data['patient_form'] = rp_forms.PatientForm()
            context_data['referral_form'] = rp_forms.ReferralForm()
            context_data['physician_form'] = rp_forms.PhysicianForm()
            context_data['diagnosis_form'] = rp_forms.DiagnosisForm()
            context_data['visit_form'] = rp_forms.VisitForm()
            context_data['treatment_course_form'] = rp_forms.TreatmentCourseForm(auto_id='id_tc_%s')
            context_data['metastatic_formset'] = rp_forms.MetaStaticCancerSiteDiagnosisCollectioFormset()

            context_data['locations'] = {
                l.id: {'name': l.name, 'address': l.address} for l in rp_models.Location.objects.all()
            }
            context_data['transport_modes'] = {
                tp.id: {'name': tp.name, 'base': tp.base_transport_mode} for tp in rp_models.TransportMode.objects.all()
            }
            context_data['rt_complexity_without_number_of_fields'] = [rp_models.NON_MODULATED, rp_models.PLACED_FIELDS]
        return context_data


class PatientsList(LoginRequiredMixin, BaseListableView):
    model = rp_models.Patient
    template_name = 'rpal/basic_list.html'
    paginate_by = 50

    order_by = ['-datetime_entered']
    kwarg_filters = None

    fields = (
        'actions',
        'datetime_entered',
        'mrn',
        'first_name',
        'recent_visit',
        'phone_number'
    )

    headers = {
        'datetime_entered': _('Date Entered'),
        'mrn': _('MRN'),
        'first_name': _('Name'),
        'phone_number': _('Phone Number'),
        'recent_visit': _('Recent Point Of Contact')
    }

    widgets = {
        'datetime_entered': DATE_RANGE,
        'mrn': TEXT,
        'first_name': TEXT,
        'phone_number': None,
        'recent_visit': DATE_RANGE
    }

    date_ranges = {
        'datetime_entered': [TODAY, YESTERDAY, LAST_WEEK, THIS_WEEK, LAST_MONTH, THIS_MONTH, THIS_YEAR],
        'recent_visit': [TODAY, YESTERDAY, LAST_WEEK, THIS_WEEK, LAST_MONTH, THIS_MONTH, THIS_YEAR]
    }

    search_fields = {
        'actions': False,
        'phone_number': False,
        'first_name': [
            'first_name__icontains',
            'last_name__icontains',
            'middle_name__icontains'
        ],
        'recent_visit': 'visit_set__date'
    }

    order_fields = {
        'actions': False,
        'phone_number': False,
        'recent_visit': 'visit_set__date'
    }

    prefetch_related = ('visit_set',)

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)
        self.templates = {
            'actions': get_template("rpal/patients_table_context_actions.html"),
            'patient': get_template("rpal/treatments_table_context_patient.html"),
            'visit': get_template("rpal/treatments_table_context_visit.html"),
        }

    def get_icon(self):
        return 'fa-users'

    def get_page_title(self, f=None):
        # if not f:
        #     return 'Patients'
        # return to_return
        return 'Patients'

    def get(self, request, *args, **kwargs):
        if self.kwarg_filters is None:
            self.kwarg_filters = kwargs.pop('f', None)
        return super().get(request, *args, **kwargs)

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        current_url = resolve(self.request.path_info).url_name
        context['view_name'] = current_url
        context['icon'] = self.get_icon()
        f = self.request.GET.get('f', False)
        context['kwargs'] = {'f': f} if f else {}
        context['page_title'] = self.get_page_title(f)
        return context

    def get_queryset(self):
        qs = super().get_queryset()

        if self.kwarg_filters is None:
            self.kwarg_filters = self.request.GET.get('f', None)

        return qs

    def datetime_entered(self, p):
        template = Template('<div>{{ datetime }}</div>')
        c = Context({'datetime': p.datetime_entered})
        return template.render(c)

    def actions(self, p):
        template = self.templates['actions']
        next = reverse('p_list') + (('?f=' + self.kwarg_filters) if self.kwarg_filters else '')
        c = {'p': p, 'request': self.request, 'next': next}
        return template.render(c)

    def first_name(self, p):
        template = self.templates['patient']
        c = {'patient': p}
        return template.render(c)

    def recent_visit(self, p):
        visits = p.visit_set.all()
        if visits.count() > 0:
            latest = visits.latest()
            template = self.templates['visit']
            c = {'visit': latest}
            return template.render(c)
        return None


class UpdateCreateCareplan(LoginRequiredMixin, SingleObjectTemplateResponseMixin, ModelFormMixin, ProcessFormView):
    """
    CreateView and UpdateView functionality combined
    """

    model = rp_models.CarePlan
    template_name = 'rpal/careplan_form.html'
    form_class = rp_forms.CareplanForm

    def dispatch(self, request, *args, **kwargs):
        self.user = request.user
        return super().dispatch(request, *args, **kwargs)

    def get_object(self, queryset=None):
        """
        Returns the object the view is displaying.

        By default this requires `self.queryset` and a `pk` or `slug` argument
        in the URLconf, but subclasses can override this to return any object.
        """
        # Use a custom queryset if provided; this is required for subclasses
        # like DateDetailView
        if queryset is None:
            queryset = self.get_queryset()

        # Next, try looking up by primary key.
        pk = self.kwargs.get(self.pk_url_kwarg)
        slug = self.kwargs.get(self.slug_url_kwarg)
        if pk is not None:
            queryset = queryset.filter(pk=pk)

        # If none of those are defined, it's an error.
        if pk is None and slug is None:
            return None

        try:
            # Get the single item from the filtered queryset
            obj = queryset.get()
        except queryset.model.DoesNotExist:
            raise Http404(_("No %(verbose_name)s found matching the query") %
                          {'verbose_name': queryset.model._meta.verbose_name})
        return obj

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().post(request, *args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['user'] = self.user
        return kwargs

    def get_context_data(self, *args, **kwargs):
        context_data = super().get_context_data(**kwargs)
        return context_data

    def form_valid(self, form):
        cp = form.save()
        return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, form):
        return super().form_invalid(form)

    def get_success_url(self):
        next_ = self.request.GET.get("next", None)
        if next_ is not None:
            return next_

        return reverse('cp_list')


class CreateCareplan(UpdateCreateCareplan):

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        return kwargs

    def form_valid(self, form):
        self.instance = form.save(commit=False)
        return super().form_valid(form)


class UpdateCareplan(UpdateCreateCareplan):

    def form_valid(self, form):
        self.instance = form.save(commit=False)
        return super().form_valid(form)


class CareplansList(LoginRequiredMixin, BaseListableView):
    model = rp_models.CarePlan
    template_name = 'rpal/basic_list.html'
    paginate_by = 50

    order_by = ['-pk']
    kwarg_filters = None
    multi_separator = ' | '

    fields = (
        'actions',
        'pk',
        'name',
        'dose',
        'fractions',
        'type',
        'complexity',
        'pattern',
    )

    headers = {
        'actions': _('Actions'),
        'pk': _('ID'),
        'name': _('Name'),
        'dose': _('Dose'),
        'fractions': _('Fractions'),
        'type': _('RT Type'),
        'complexity': _('RT Complexity'),
        'pattern': _('Pattern'),
    }

    widgets = {
        'type': SELECT_MULTI,
        'complexity': SELECT_MULTI,
        'pattern': SELECT_MULTI,
    }

    search_fields = {
        'actions': False,
    }

    order_fields = {
        'actions': False,
    }

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)
        self.templates = {
            'actions': get_template("rpal/careplans_table_context_actions.html"),
        }

    def get_icon(self):
        return 'fa-plus-square'

    def get_page_title(self, f=None):
        return 'Careplans'

    def get(self, request, *args, **kwargs):
        if self.kwarg_filters is None:
            self.kwarg_filters = kwargs.pop('f', None)
        return super().get(request, *args, **kwargs)

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        current_url = resolve(self.request.path_info).url_name
        context['view_name'] = current_url
        context['icon'] = self.get_icon()
        f = self.request.GET.get('f', False)
        context['kwargs'] = {'f': f} if f else {}
        context['page_title'] = self.get_page_title(f)
        return context

    def get_queryset(self):
        qs = super().get_queryset()

        if self.kwarg_filters is None:
            self.kwarg_filters = self.request.GET.get('f', None)

        return qs

    def actions(self, cp):
        template = self.templates['actions']
        next = reverse('cp_list') + (('?f=' + self.kwarg_filters) if self.kwarg_filters else '')
        c = {'p': cp, 'request': self.request, 'next': next}
        return template.render(c)


class VisitsList(LoginRequiredMixin, BaseListableView):

    model = rp_models.Visit
    template_name = 'rpal/basic_list.html'
    paginate_by = 50
    order_by = ['-date']
    kwarg_filters = None
    multi_separator = ' | '
    date_ranges = {
        'date': [
            TODAY, YESTERDAY, LAST_WEEK, THIS_WEEK, LAST_14_DAYS, LAST_MONTH, THIS_MONTH, THIS_YEAR
        ],
        'referral__date_referred': [
            TODAY, YESTERDAY, LAST_WEEK, THIS_WEEK, LAST_14_DAYS, LAST_MONTH, THIS_MONTH, THIS_YEAR
        ],
    }

    fields = (
        'patient__mrn',
        'patient',
        'referral__date_referred',
        'date',
        'point_of_contact_type',
        'location__name',
        'in_out',
        'duration_travel',
        'is_treatment_decided_on_visit',
        # 'ecog',
        'medications_modified__name',
        'is_first_visit'
    )

    headers = {
        'patient': _('Patient Name'),
        'patient__mrn': _('MRN'),
        'referral__date_referred': _('Ref Date'),
        'date': _('Date'),
        'point_of_contact_type': _('Type'),
        'location__name': _('Location'),
        'in_out': _('In/outpatient'),
        'duration_travel': _('Travel Duration'),
        'is_treatment_decided_on_visit': _('Tx Decided'),
        'ecog': _('ECOG'),
        'medications_modified__name': _('Meds Modified'),
        'is_first_visit': _('First Visit')
    }

    widgets = {
        'patient': TEXT,
        'patient__mrn': TEXT,
        'referral__date_referred': DATE_RANGE,
        'date': DATE_RANGE,
        'point_of_contact_type': SELECT_MULTI,
        'location__name': SELECT_MULTI,
        'in_out': SELECT,
        'is_treatment_decided_on_visit': SELECT,
        'ecog': SELECT_MULTI,
        'medications_modified__name': SELECT_MULTI_FROM_MULTI,
        'is_first_visit': SELECT
    }

    search_fields = {
        'duration_travel': False,
    }

    select_related = ('patient', 'location', 'referral')
    prefetch_related = ('medications_modified',)

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)
        self.templates = {
            'datetime': get_template("rpal/treatments_table_context_dates.html"),
            'visit': get_template("rpal/treatments_table_context_visit.html"),
            'patient': get_template("rpal/treatments_table_context_patient.html"),
        }

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        current_url = resolve(self.request.path_info).url_name
        context['view_name'] = current_url
        context['icon'] = self.get_icon()
        f = self.request.GET.get('f', False)
        context['kwargs'] = {'f': f} if f else {}
        context['page_title'] = self.get_page_title(f)
        return context

    def get_icon(self):
        return 'fa-address-card-o'

    def get_page_title(self, f=None):
        return 'All Point Of Contacts'

    def date(self, v):
        template = self.templates['datetime']
        c = {'datetime': v.date if v.date else ''}
        return template.render(c)

    def referral__date_referred(self, v):
        template = self.templates['datetime']
        c = {'datetime': v.referral.date_referred.date if v.referral else ''}
        return template.render(c)

    def patient(self, v):
        template = self.templates['patient']
        c = {'patient': v.patient}
        return template.render(c)


class TreatmentCourseList(LoginRequiredMixin, BaseListableView):

    model = rp_models.TreatmentCourse
    template_name = 'rpal/basic_list.html'
    paginate_by = 50
    order_by = ['patient__mrn', '-ct_date']
    kwarg_filters = None
    multi_separator = ' | '
    date_ranges = {
        'ct_date': [
            TODAY, YESTERDAY, LAST_WEEK, THIS_WEEK, LAST_14_DAYS, LAST_MONTH, THIS_MONTH, THIS_YEAR
        ],
        'visit': [
            TODAY, YESTERDAY, LAST_WEEK, THIS_WEEK, LAST_14_DAYS, LAST_MONTH, THIS_MONTH, THIS_YEAR
        ],
        'visit__referral__date_referred': [
            TODAY, YESTERDAY, LAST_WEEK, THIS_WEEK, LAST_14_DAYS, LAST_MONTH, THIS_MONTH, THIS_YEAR
        ],
        'date_start': [
            TODAY, YESTERDAY, LAST_WEEK, THIS_WEEK, LAST_14_DAYS, LAST_MONTH, THIS_MONTH, THIS_YEAR
        ],
    }

    fields = (
        # 'pk',
        'patient__mrn',
        'patient',
        'structure_treated__name',
        'dose_override',
        'fractions_override',
        'visit__referral__date_referred',
        'visit',
        'ct_date',
        'date_start',
        'bone_mets_complexity__name',
        'is_prescribed_course_complete'
    )

    headers = {
        'pk': _('ID'),
        'patient': _('Patient Name'),
        'patient__mrn': _('MRN'),
        'structure_treated__name': _('Structure'),
        'dose_override': _('Dose'),
        'fractions_override': _('Fraction'),
        'visit': _('POC Initiated'),
        'visit__referral__date_referred': _('Referral Date'),
        'ct_date': _('CT Date'),
        'date_start': _('Tx Start'),
        'bone_mets_complexity__name': _('Bone Mets'),
        'is_prescribed_course_complete': _('Tx Complete'),
    }

    widgets = {
        'pk': TEXT,
        'patient': TEXT,
        'patient__mrn': TEXT,
        'structure_treated__name': SELECT_MULTI,
        'dose_override': TEXT,
        'fractions_override': TEXT,
        'visit__referral__date_referred': DATE_RANGE,
        'visit': DATE_RANGE,
        'ct_date': DATE_RANGE,
        'date_start': DATE_RANGE,
        'bone_mets_complexity__name': SELECT_MULTI_FROM_MULTI,
        'is_prescribed_course_complete': SELECT
    }

    order_fields = {
        'visit': 'visit__date',
        'patient': 'patient__first_name'
    }
    search_fields = {
        'visit': 'visit__date',
        'patient': [
            'patient__first_name__icontains',
            'patient__last_name__icontains',
            'patient__middle_name__icontains'
        ]
    }
    select_related = ('patient', 'visit', 'visit__referral', 'structure_treated')

    prefetch_related = ('bone_mets_complexity',)

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)
        self.templates = {
            'datetime': get_template("rpal/treatments_table_context_dates.html"),
            'visit': get_template("rpal/treatments_table_context_visit.html"),
            'patient': get_template("rpal/treatments_table_context_patient.html"),
        }

    def get_icon(self):
        return 'fa-bullseye'

    def get_page_title(self, f=None):
        return 'Treatments'

    def get(self, request, *args, **kwargs):
        if self.kwarg_filters is None:
            self.kwarg_filters = kwargs.pop('f', None)
        return super().get(request, *args, **kwargs)

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        current_url = resolve(self.request.path_info).url_name
        context['view_name'] = current_url
        context['icon'] = self.get_icon()
        f = self.request.GET.get('f', False)
        context['kwargs'] = {'f': f} if f else {}
        context['page_title'] = self.get_page_title(f)
        return context

    def get_queryset(self):
        qs = super().get_queryset()

        if self.kwarg_filters is None:
            self.kwarg_filters = self.request.GET.get('f', None)

        return qs

    def ct_date(self, t):
        template = self.templates['datetime']
        c = {'datetime': t.ct_date if t.ct_date else ''}
        return template.render(c)

    def date_start(self, t):
        template = self.templates['datetime']
        c = {'datetime': t.date_start if t.date_start else ''}
        return template.render(c)

    def visit__referral__date_referred(self, t):
        template = self.templates['datetime']
        c = {'datetime': t.visit.referral.date_referred.date if t.visit and t.visit.referral else ''}
        return template.render(c)

    def patient(self, t):
        template = self.templates['patient']
        c = {'patient': t.patient}
        return template.render(c)

    def visit(self, t):
        template = self.templates['visit']
        c = {'visit': t.visit}
        return template.render(c)


class Stats(LoginRequiredMixin, TemplateView):

    template_name = 'rpal/stats.html'

    def get_context_data(self, **kwargs):
        context = super(Stats, self).get_context_data(**kwargs)
        oldest_moment = settings.OLDEST_MOMENT
        context['oldest_moment'] = oldest_moment
        return context


def get_stat_data(request):

    date_from = request.GET.get('date_from')
    date_to = request.GET.get('date_to')

    if not date_to or not date_from:
        return JsonResponse('Error with dates', status=400)

    date_from = timezone.datetime.strptime(date_from, '%d-%m-%Y')
    date_to = timezone.datetime.strptime(date_to, '%d-%m-%Y')

    tc_demo_data_qs = rp_models.TreatmentCourse.objects.filter(
        date_start__gt=date_from.replace(day=1, hour=0, minute=0),
        date_start__lt=date_to.replace(
            day=calendar.monthrange(date_to.year, date_to.month)[1], hour=23, minute=59
        )
    )
    tc_ids = [tc.id for tc in tc_demo_data_qs]

    tc_bmc_demo_data_qs = rp_models.TreatmentCourseBoneMetsComplexityCollection.objects.filter(
        treatment_course_id__in=tc_ids
    )

    v_demo_data_qs = rp_models.Visit.objects.filter(
        date__gt=date_from.replace(day=1, hour=0, minute=0),
        date__lt=date_to.replace(
            day=calendar.monthrange(date_to.year, date_to.month)[1], hour=23, minute=59
        )
    )
    v_ids = [v.id for v in v_demo_data_qs]

    monthly_data = [
        {
            'month': dt.strftime('%Y-%m'),
            'duration_travel_poc_dict': list(v_demo_data_qs.filter(
                point_of_contact_mode__in=['telemed', 'in_person'],
                date__gt=dt.replace(day=1, hour=0, minute=0),
                date__lt=dt.replace(day=calendar.monthrange(dt.year, dt.month)[1], hour=23, minute=59)
            ).values('duration_travel', 'patient__mrn')),
            'duration_travel_tc_dict': list(tc_demo_data_qs.filter(
                date_start__gt=dt.replace(day=1, hour=0, minute=0),
                date_start__lt=dt.replace(day=calendar.monthrange(dt.year, dt.month)[1], hour=23, minute=59)
            ).values('duration_travel', 'patient__mrn', 'fractions_override'))
        } for dt in rrule(MONTHLY, dtstart=date_from, until=date_to)
    ]

    for md in monthly_data:
        for dt in md['duration_travel_poc_dict']:
            dt['duration_travel'] = dt['duration_travel'].total_seconds() if dt['duration_travel'] else None
        for dt in md['duration_travel_tc_dict']:
            dt['duration_travel'] = dt['duration_travel'].total_seconds() if dt['duration_travel'] else None

    coming_from_data = {
        'tc': [{
            'lat_lng': tc.coming_from_lat_lng,
            'mrn': tc.patient.mrn,
            'address': tc.coming_from,
            'duration_travel': tc.duration_travel.total_seconds() if tc.duration_travel else ''
        } for tc in tc_demo_data_qs.filter(coming_from_lat_lng__isnull=False)],
        'poc': [{
            'lat_lng': v.coming_from_lat_lng,
            'mrn': v.patient.mrn,
            'address': v.coming_from,
            'duration_travel': v.duration_travel.total_seconds() if v.duration_travel else ''
        } for v in v_demo_data_qs.filter(coming_from_lat_lng__isnull=False)]
    }

    demographics_data = {
        'patient_data': list(
            rp_models.Patient.objects.prefetch_related('treatmentcourse_set', 'visit_set').filter(
                Q(treatmentcourse_set__id__in=tc_ids) | Q(visit_set__id__in=v_ids)
            ).distinct().values('mrn', 'date_birth', 'date_deceased', 'gender')
        ),
        'visit_data': list(v_demo_data_qs.values(
            'id', 'ecog', 'in_out', 'point_of_contact_mode', 'is_treatment_decided_on_visit', 'is_first_visit'
        )),
        'treatment_data': list(tc_demo_data_qs.values('id', 'structure_treated__name')),
        'bone_mets_data': list(tc_bmc_demo_data_qs.values('id', 'bone_mets_complexity__name', 'treatment_course_id'))
    }
    return JsonResponse({'data': {
        'monthly_data': monthly_data,
        'coming_from_data': coming_from_data,
        'demographics_data': demographics_data
    }})


def multi_field_to_string(qs, display_field='name', seperator=' | '):
    return seperator.join([getattr(item, display_field) for item in qs])


def smart_str_with_none(obj):
    if obj is None:
        return ''
    return smart_str(obj)


def format_duration(duration):
    if duration is None:
        return ''
    return duration.total_seconds() // 60


def export_patients_csv(request):

    date_from = request.GET.get('date_from')
    date_to = request.GET.get('date_to')

    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename=patients_{}_{}.csv'.format(date_from, date_to)

    date_from = timezone.datetime.strptime(date_from, '%d-%m-%Y')
    date_to = timezone.datetime.strptime(date_to, '%d-%m-%Y')

    writer = csv.writer(response, csv.excel)
    response.write(u'\ufeff'.encode('utf8'))

    writer.writerow([
        'MRN',
        'Date Entered',
        'Name',
        'Date Birth',
        'Gender',
        'Date Deceased',

        'Referrals:', 'ID', 'Physician', 'Referral Via', 'Reasons', 'Location', 'Physician Contacted By', 'Communication Mode', 'Date Referred',
        'Diagnoses:', 'ID', 'Primary Cancer Site', 'Metastatic Cancer Sites', 'Primary Diagnosis Group', 'Primary Cancer Dx Description', 'Pathologic Diagnosis', 'Date Diagnosis', 'Radiographic Diagnosis Explanation',
        'Point of Contacts:', 'ID', 'Transport Mode', 'Accompanying People', 'Location', 'Coming From', 'Duration Travel', 'Person Consent', 'Medications', 'Medications Modified', 'Treatment Course', 'Referral', 'Point Of Contact Mode', 'Point Of Contact Type', 'Stage', 'Services Referred', 'Code Status', 'Life Expectancy', 'Mental Status', 'Ministry Of Transportation Status', 'Frailty', 'In/Out Patient', 'Is On Chemo', 'Is Treatment Decided On Visit', 'Date', 'ECOG', 'PPS',
        'Treatment Courses:', 'ID', 'Initiated from POC', 'Structure Treated', 'Care Plan', 'Num Fields', 'Bone Mets Complexity', 'Dose', 'Fractions', 'Type', 'Complexity', 'Pattern', 'Start Date', 'End Date', 'CT Date', 'Bone Mets', 'Is Prescribed Course Complete', 'Same Day Start Date', 'Explanation', 'Transport Mode', 'Location', 'Coming From', 'Duration Travel',

    ])

    writer.writerow([
        '',
        '(dd-mm-yyyy)',
        '',
        '(dd-mm-yyyy)',
        '',
        '(dd-mm-yyyy)',

        '|', '', '', '', '', '', '', '', '', '|', '', '', '', '', '', '', '', '', '|', '', '', '', '', '', '', '', '',
        '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '|'

    ])

    qs = rp_models.Patient.objects.prefetch_related(
        'referral_set', 'referral_set__referring_physician', 'referral_set__referring_physician__physician_specialty', 'referral_set__referral_reach', 'referral_set__referral_reasons', 'referral_set__referral_location',
        'diagnosis_set', 'diagnosis_set__primary_cancer_site', 'diagnosis_set__cancer_sites_metastatic',
        'visit_set', 'visit_set__transport_mode', 'visit_set__location', 'visit_set__person_consent', 'visit_set__code_status', 'visit_set__mental_status', 'visit_set__medications', 'visit_set__medications_modified', 'visit_set__services_referred', 'visit_set__accompanying_people',
        'treatmentcourse_set', 'treatmentcourse_set__care_plan', 'treatmentcourse_set__care_plan_num_fields', 'treatmentcourse_set__structure_treated', 'treatmentcourse_set__bone_mets_complexity', 'treatmentcourse_set__location', 'treatmentcourse_set__transport_mode'
    ).filter(
        datetime_entered__gte=date_from, datetime_entered__lte=date_to
    )

    for p in qs:

        r_qs = list(p.referral_set.all())
        d_qs = list(p.diagnosis_set.all())
        v_qs = list(p.visit_set.all())
        t_qs = list(p.treatmentcourse_set.all())

        r_qs_len = len(r_qs)
        d_qs_len = len(d_qs)
        v_qs_len = len(v_qs)
        t_qs_len = len(t_qs)

        max_rows_for_p = max([r_qs_len, d_qs_len, v_qs_len, t_qs_len])

        patient_detail_rows = []
        for i in range(max_rows_for_p):
            row = ['', '', '', '', '', '', '|']
            if r_qs_len > i + 1:
                row.append(r_qs[i].id)
                row.append(smart_str_with_none(r_qs[i].referring_physician))
                row.append(smart_str_with_none(r_qs[i].referral_reach))
                row.append(smart_str_with_none(multi_field_to_string(r_qs[i].referral_reasons.all())))
                row.append(smart_str_with_none(r_qs[i].referral_location))
                row.append(smart_str_with_none(r_qs[i].physician_contacted_by))
                row.append(smart_str_with_none(rp_models.get_choice_text(r_qs[i].referring_communication_mode, rp_models.COMMUNICATION_MODES)))
                row.append(r_qs[i].date_referred.strftime('%d-%m-%Y'))
            else:
                for j in range(8):
                    row.append('')
            row.append('|')

            if d_qs_len > i + 1:
                row.append(d_qs[i].id)
                row.append(smart_str_with_none(d_qs[i].primary_cancer_site))
                row.append(smart_str_with_none(multi_field_to_string(d_qs[i].cancer_sites_metastatic.all())))
                row.append(smart_str_with_none(d_qs[i].primary_diagnosis_group))
                row.append(smart_str_with_none(d_qs[i].primary_cancer_dx_description))
                row.append(smart_str_with_none(d_qs[i].pathologic_diagnosis))
                row.append(d_qs[i].date_diagnosis.strftime('%d-%m-%Y') if d_qs[i].date_diagnosis else '')
                row.append(smart_str_with_none(d_qs[i].radiographic_diagnosis_explanation))
            else:
                for j in range(8):
                    row.append('')
            row.append('|')

            if v_qs_len > i + 1:
                row.append(v_qs[i].id)
                row.append(smart_str_with_none(v_qs[i].transport_mode))
                row.append(smart_str_with_none(multi_field_to_string(v_qs[i].accompanying_people.all())))
                row.append(smart_str_with_none(v_qs[i].location))
                row.append(smart_str_with_none(v_qs[i].coming_from))
                row.append(smart_str_with_none(format_duration(v_qs[i].duration_travel)))
                row.append(smart_str_with_none(v_qs[i].person_consent))
                row.append(smart_str_with_none(multi_field_to_string(v_qs[i].medications.all())))
                row.append(smart_str_with_none(multi_field_to_string(v_qs[i].medications_modified.all())))
                row.append(smart_str_with_none(v_qs[i].treatment_course_id))
                row.append(smart_str_with_none(v_qs[i].referral_id))
                row.append(smart_str_with_none(v_qs[i].point_of_contact_mode))
                row.append(smart_str_with_none(v_qs[i].point_of_contact_type))
                row.append(smart_str_with_none(v_qs[i].stage))
                row.append(smart_str_with_none(multi_field_to_string(v_qs[i].services_referred.all())))
                row.append(smart_str_with_none(v_qs[i].code_status))
                row.append(smart_str_with_none(v_qs[i].life_expectancy))
                row.append(smart_str_with_none(v_qs[i].mental_status))
                row.append(smart_str_with_none(v_qs[i].ministry_of_transportation_status))
                row.append(smart_str_with_none(v_qs[i].frailty))
                row.append(smart_str_with_none(v_qs[i].in_out))
                row.append(v_qs[i].is_on_chemo)
                row.append(v_qs[i].is_treatment_decided_on_visit)
                row.append(smart_str_with_none(v_qs[i].date.strftime('%d-%m-%Y')))
                row.append(smart_str_with_none(v_qs[i].ecog))
                row.append(smart_str_with_none(v_qs[i].pps))

            else:
                for j in range(26):
                    row.append('')
            row.append('|')

            if t_qs_len > i + 1:
                row.append(smart_str_with_none(t_qs[i].id))
                row.append(smart_str_with_none(t_qs[i].visit_id))
                row.append(smart_str_with_none(t_qs[i].structure_treated))
                row.append(smart_str_with_none(t_qs[i].care_plan))
                row.append(smart_str_with_none(t_qs[i].care_plan_num_fields))
                row.append(smart_str_with_none(multi_field_to_string(t_qs[i].bone_mets_complexity.all())))
                row.append(smart_str_with_none(t_qs[i].dose_override))
                row.append(smart_str_with_none(t_qs[i].fractions_override))
                row.append(smart_str_with_none(t_qs[i].type_override))
                row.append(smart_str_with_none(t_qs[i].complexity_override))
                row.append(smart_str_with_none(t_qs[i].pattern_override))
                row.append(smart_str_with_none(t_qs[i].date_start.strftime('%d-%m-%Y') if t_qs[i].date_start else None))
                row.append(smart_str_with_none(t_qs[i].date_end.strftime('%d-%m-%Y') if t_qs[i].date_end else None))
                row.append(smart_str_with_none(t_qs[i].ct_date.strftime('%d-%m-%Y') if t_qs[i].ct_date else None))
                row.append(smart_str_with_none(t_qs[i].bone_mets_involved))
                row.append(smart_str_with_none(t_qs[i].is_prescribed_course_complete))
                row.append(smart_str_with_none(t_qs[i].same_day_start_date))
                row.append(smart_str_with_none(t_qs[i].explanation))
                row.append(smart_str_with_none(t_qs[i].transport_mode))
                row.append(smart_str_with_none(t_qs[i].location))
                row.append(smart_str_with_none(t_qs[i].coming_from))
                row.append(smart_str_with_none(format_duration(t_qs[i].duration_travel)))
            else:
                for j in range(22):
                    row.append('')

            patient_detail_rows.append(row)

        writer.writerow([
            smart_str_with_none(p.mrn),
            smart_str_with_none(p.datetime_entered.strftime('%d-%m-%Y')),
            smart_str_with_none(p.get_full_name()),
            smart_str_with_none(p.date_birth.strftime('%d-%m-%Y') if p.date_birth else None),
            smart_str_with_none(p.gender),
            smart_str_with_none(p.date_deceased.strftime('%d-%m-%Y') if p.date_deceased else None),

            '{} Referrals'.format(r_qs_len),
            '', '', '', '', '', '', '', '',

            '{} Diagnoses'.format(d_qs_len),
            '', '', '', '', '', '', '', '',

            '{} Point of contacts'.format(v_qs_len),
            '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '',

            '{} Treatment Courses'.format(t_qs_len),
            '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''
        ])

        for r in patient_detail_rows:
            writer.writerow(r)

    return response


def export_visit_csv(request):

    date_from = request.GET.get('date_from')
    date_to = request.GET.get('date_to')

    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename=point_of_contacts_{}_{}.csv'.format(date_from, date_to)

    date_from = timezone.datetime.strptime(date_from, '%d-%m-%Y')
    date_to = timezone.datetime.strptime(date_to, '%d-%m-%Y')

    writer = csv.writer(response, csv.excel)
    response.write(u'\ufeff'.encode('utf8'))
    writer.writerow([
        'ID',

        'MRN',
        'Name',
        'Date Birth',
        'Gender',
        'Date Deceased',

        'Transport Mode',
        'Accompanying People',
        'Location',
        'Coming From',
        'Duration Travel',
        'Person Consent',
        'Medications',
        'Medications Modified',
        'Treatment Course',
        'Referral',
        'Point Of Contact Mode',
        'Point Of Contact Type',
        'Stage',
        'Services Referred',
        'Code Status',
        'Life Expectancy',
        'Mental Status',
        'Ministry Of Transportation Status',
        'Frailty',
        'In/Out Patient',
        'Is On Chemo',
        'Is Treatment Decided On Visit',
        'Date',
        'ECOG',
        'PPS',
    ])

    writer.writerow([
        '',
        '',
        '',
        '(dd-mm-yyyy)',
        '',
        '(dd-mm-yyyy)',

        '',
        '',
        '',
        '',
        '(minutes)',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '(days)',
        '',
        '',
        '',
        '',
        '',
        '',
        '(dd-mm-yyyy)',
        '',
        '',
    ])

    qs = rp_models.Visit.objects.get_verbose_queryset().filter(
        date__gte=date_from, date__lte=date_to
    )

    for v in qs:
        writer.writerow([
            smart_str_with_none(v.id),

            smart_str_with_none(v.patient.mrn),
            smart_str_with_none(v.patient.get_full_name()),
            smart_str_with_none(v.patient.date_birth.strftime('%d-%m-%Y') if v.patient.date_birth else None),
            smart_str_with_none(v.patient.gender),
            smart_str_with_none(v.patient.date_deceased.strftime('%d-%m-%Y') if v.patient.date_deceased else None),

            smart_str_with_none(v.transport_mode),
            smart_str_with_none(multi_field_to_string(v.accompanying_people.all())),
            smart_str_with_none(v.location),
            smart_str_with_none(v.coming_from),
            smart_str_with_none(format_duration(v.duration_travel)),
            smart_str_with_none(v.person_consent),
            smart_str_with_none(multi_field_to_string(v.medications.all())),
            smart_str_with_none(multi_field_to_string(v.medications_modified.all())),
            smart_str_with_none(v.treatment_course_id),
            smart_str_with_none(v.referral_id),
            smart_str_with_none(v.point_of_contact_mode),
            smart_str_with_none(v.point_of_contact_type),
            smart_str_with_none(v.stage),
            smart_str_with_none(multi_field_to_string(v.services_referred.all())),
            smart_str_with_none(v.code_status),
            smart_str_with_none(v.life_expectancy),
            smart_str_with_none(v.mental_status),
            smart_str_with_none(v.ministry_of_transportation_status),
            smart_str_with_none(v.frailty),
            smart_str_with_none(v.in_out),
            smart_str_with_none(v.is_on_chemo),
            smart_str_with_none(v.is_treatment_decided_on_visit),
            smart_str_with_none(v.date.strftime('%d-%m-%Y')),
            smart_str_with_none(v.ecog),
            smart_str_with_none(v.pps),
        ])
    return response


def export_treatment_course_csv(request):

    date_from = request.GET.get('date_from')
    date_to = request.GET.get('date_to')

    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename=treatment_courses_{}_{}.csv'.format(date_from, date_to)

    date_from = timezone.datetime.strptime(date_from, '%d-%m-%Y')
    date_to = timezone.datetime.strptime(date_to, '%d-%m-%Y')

    writer = csv.writer(response, csv.excel)
    response.write(u'\ufeff'.encode('utf8'))  # BOM (optional...Excel needs it to open UTF-8 file properly)
    writer.writerow([
        'ID',

        'MRN',
        'Name',
        'Date Birth',
        'Gender',
        'Date Deceased',

        'Initiated from POC',
        'Structure Treated',
        'Care Plan',
        'Num Fields',
        'Bone Mets Complexity',
        'Dose',
        'Fractions',
        'Type',
        'Complexity',
        'Pattern',
        'Start Date',
        'End Date',
        'CT Date',
        'Bone Mets',
        'Is Prescribed Course Complete',
        'Same Day Start Date',
        'Explanation',
        'Transport Mode',
        'Location',
        'Coming From',
        'Duration Travel',
    ])

    writer.writerow([
        '',
        '',
        '',
        '(dd-mm-yyyy)',
        '',
        '(dd-mm-yyyy)',

        '',
        '',
        '',
        '',
        '',
        '(cGy)',
        '',
        '',
        '',
        '',
        '(dd-mm-yyyy)',
        '(dd-mm-yyyy)',
        '(dd-mm-yyyy)',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '(minutes)',
    ])

    qs = rp_models.TreatmentCourse.objects.get_verbose_queryset().filter(
        date_start__gte=date_from, date_start__lte=date_to
    )

    for tc in qs:
        writer.writerow([
            smart_str_with_none(tc.id),

            smart_str_with_none(tc.patient.mrn),
            smart_str_with_none(tc.patient.get_full_name()),
            smart_str_with_none(tc.patient.date_birth.strftime('%d-%m-%Y') if tc.patient.date_birth else None),
            smart_str_with_none(tc.patient.gender),
            smart_str_with_none(tc.patient.date_deceased.strftime('%d-%m-%Y') if tc.patient.date_deceased else None),

            smart_str_with_none(tc.visit_id),
            smart_str_with_none(tc.structure_treated),
            smart_str_with_none(tc.care_plan),
            smart_str_with_none(tc.care_plan_num_fields),
            smart_str_with_none(multi_field_to_string(tc.bone_mets_complexity.all())),
            smart_str_with_none(tc.dose_override),
            smart_str_with_none(tc.fractions_override),
            smart_str_with_none(tc.type_override),
            smart_str_with_none(tc.complexity_override),
            smart_str_with_none(tc.pattern_override),
            smart_str_with_none(tc.date_start.strftime('%d-%m-%Y') if tc.date_start else None),
            smart_str_with_none(tc.date_end.strftime('%d-%m-%Y') if tc.date_end else None),
            smart_str_with_none(tc.ct_date.strftime('%d-%m-%Y') if tc.ct_date else None),
            smart_str_with_none(tc.bone_mets_involved),
            smart_str_with_none(tc.is_prescribed_course_complete),
            smart_str_with_none(tc.same_day_start_date),
            smart_str_with_none(tc.explanation),
            smart_str_with_none(tc.transport_mode),
            smart_str_with_none(tc.location),
            smart_str_with_none(tc.coming_from),
            smart_str_with_none(format_duration(tc.duration_travel)),
        ])
    return response


def export_referral_csv(request):

    date_from = request.GET.get('date_from')
    date_to = request.GET.get('date_to')

    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename=referrals_{}_{}.csv'.format(date_from, date_to)

    date_from = timezone.datetime.strptime(date_from, '%d-%m-%Y')
    date_to = timezone.datetime.strptime(date_to, '%d-%m-%Y')

    writer = csv.writer(response, csv.excel)
    response.write(u'\ufeff'.encode('utf8'))  # BOM (optional...Excel needs it to open UTF-8 file properly)
    writer.writerow([
        'ID',

        'MRN',
        'Name',
        'Date Birth',
        'Gender',
        'Date Deceased',

        'Physician',
        'Referral Via',
        'Reasons',
        'Location',
        'Physician Contacted By',
        'Communication Mode',
        'Date Referred',
    ])

    writer.writerow([
        '',

        '',
        '',
        '(dd-mm-yyyy)',
        '',
        '(dd-mm-yyyy)',

        '',
        '',
        '',
        '',
        '',
        '',
        '(dd-mm-yyyy)',
    ])

    qs = rp_models.Referral.objects.get_verbose_queryset().filter(
        date_referred__gte=date_from, date_referred__lte=date_to
    )

    for r in qs:
        writer.writerow([
            smart_str_with_none(r.id),

            smart_str_with_none(r.patient.mrn),
            smart_str_with_none(r.patient.get_full_name()),
            smart_str_with_none(r.patient.date_birth.strftime('%d-%m-%Y') if r.patient.date_birth else None),
            smart_str_with_none(r.patient.gender),
            smart_str_with_none(r.patient.date_deceased.strftime('%d-%m-%Y') if r.patient.date_deceased else None),

            smart_str_with_none(r.referring_physician),
            smart_str_with_none(r.referral_reach),
            smart_str_with_none(multi_field_to_string(r.referral_reasons.all())),
            smart_str_with_none(r.referral_location),
            smart_str_with_none(r.physician_contacted_by),
            smart_str_with_none(
                rp_models.get_choice_text(r.referring_communication_mode, rp_models.COMMUNICATION_MODES)
            ),
            smart_str_with_none(r.date_referred.strftime('%d-%m-%Y')),
        ])
    return response


def export_diagnoses_csv(request):

    date_from = request.GET.get('date_from')
    date_to = request.GET.get('date_to')

    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename=diagnoses_{}_{}.csv'.format(date_from, date_to)

    date_from = timezone.datetime.strptime(date_from, '%d-%m-%Y')
    date_to = timezone.datetime.strptime(date_to, '%d-%m-%Y')

    writer = csv.writer(response, csv.excel)
    response.write(u'\ufeff'.encode('utf8'))  # BOM (optional...Excel needs it to open UTF-8 file properly)
    writer.writerow([
        'ID',

        'MRN',
        'Name',
        'Date Birth',
        'Gender',
        'Date Deceased',

        'Primary Cancer Site',
        'Metastatic Cancer Sites',
        'Primary Diagnosis Group',
        'Primary Cancer Dx Description',
        'Pathologic Diagnosis',
        'Date Diagnosis',
        'Radiographic Diagnosis Explanation',
    ])

    writer.writerow([
        '',

        '',
        '',
        '(dd-mm-yyyy)',
        '',
        '(dd-mm-yyyy)',

        '',
        '',
        '',
        '',
        '',
        '(dd-mm-yyyy)',
        '',
    ])

    qs = rp_models.Diagnosis.objects.get_verbose_queryset().filter(
        date_diagnosis__gte=date_from, date_diagnosis__lte=date_to
    )

    for d in qs:
        writer.writerow([
            smart_str_with_none(d.id),

            smart_str_with_none(d.patient.mrn),
            smart_str_with_none(d.patient.get_full_name()),
            smart_str_with_none(d.patient.date_birth.strftime('%d-%m-%Y') if d.patient.date_birth else None),
            smart_str_with_none(d.patient.gender),
            smart_str_with_none(d.patient.date_deceased.strftime('%d-%m-%Y') if d.patient.date_deceased else None),

            smart_str_with_none(d.primary_cancer_site),
            smart_str_with_none(multi_field_to_string(d.cancer_sites_metastatic.all())),
            smart_str_with_none(d.primary_diagnosis_group),
            smart_str_with_none(d.primary_cancer_dx_description),
            smart_str_with_none(d.pathologic_diagnosis),
            smart_str_with_none(d.date_diagnosis.strftime('%d-%m-%Y')),
            smart_str_with_none(d.radiographic_diagnosis_explanation),
        ])
    return response
