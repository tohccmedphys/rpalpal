"""rpalpal URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import include
from django.contrib import admin
from django.urls import reverse_lazy, path, re_path
from django.views.generic import RedirectView

from rpalpal_core import views

urlpatterns = [
    re_path('^$', RedirectView.as_view(url=reverse_lazy("home"))),
    path('home/', views.Home.as_view(), name="home"),
    path('admin/', admin.site.urls),
    path('accounts/', include('accounts.urls')),
    path('rpal/', include('rpal.urls')),
    # url(r'^plans/', include('rpal.urls')),
    # path('', include('django.contrib.auth.urls')),
]

if settings.DEBUG:
    from django.conf.urls import include
    import debug_toolbar
    urlpatterns.append(path('__debug__/', include(debug_toolbar.urls)))
