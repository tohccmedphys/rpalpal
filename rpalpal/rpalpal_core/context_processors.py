
from django.conf import settings
from random import Random

from rpal.admin import ADMIN_FIELDS


def site(request):

    return {
        'VERSION': settings.VERSION,
        'FORCE_SCRIPT_NAME': settings.FORCE_SCRIPT_NAME,
        'DEBUG': settings.DEBUG,
        'USE_LDAP': settings.USE_LDAP,
        'GOOGLE_API_KEY': settings.GOOGLE_API_KEY,
        'ADMIN_FIELDS': ADMIN_FIELDS,
        'CSS_VERSION': Random().randint(1, 1000) if settings.DEBUG else settings.VERSION,
        'MIN': '' if settings.DEBUG else '.min',
    }
