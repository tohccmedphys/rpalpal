/*!
 * Dateline 0.0.1
 *
 */
(function (factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(['jquery'], factory);
    } else if (typeof exports === 'object') {
        // Node/CommonJS
        factory(require('jquery'));
    } else {
        // Browser globals
        factory(jQuery);
    }
}(function (jQuery) {
    // This is needed so we can catch the AMD loader configuration and use it
    // The inner file should be wrapped (by `banner.start.js`) in a function that
    // returns the AMD loader references.
    var DALI =
        (function () {
            // Restore the Select2 AMD loader so it can be used
            // Needed mostly in the language files, where the loader is not inserted
            if (jQuery && jQuery.fn && jQuery.fn.felter && jQuery.fn.felter.amd) {
                var DALI = jQuery.fn.felter.amd;
            }
            var DALI;
            (function () {
                if (!DALI || !DALI.requirejs) {
                    if (!DALI) {
                        DALI = {};
                    } else {
                        require = DALI;
                    }
                    var requirejs, require, define;
                    (function (undef) {
                        var main, req, makeMap, handlers,
                            defined = {},
                            waiting = {},
                            config = {},
                            defining = {},
                            hasOwn = Object.prototype.hasOwnProperty,
                            aps = [].slice,
                            jsSuffixRegExp = /\.js$/;

                        function hasProp(obj, prop) {
                            return hasOwn.call(obj, prop);
                        }

                        /**
                         * Given a relative module name, like ./something, normalize it to
                         * a real name that can be mapped to a path.
                         * @param {String} name the relative name
                         * @param {String} baseName a real name that the name arg is relative
                         * to.
                         * @returns {String} normalized name
                         */
                        function normalize(name, baseName) {
                            var nameParts, nameSegment, mapValue, foundMap, lastIndex,
                                foundI, foundStarMap, starI, i, j, part,
                                baseParts = baseName && baseName.split("/"),
                                map = config.map,
                                starMap = (map && map['*']) || {};

                            //Adjust any relative paths.
                            if (name && name.charAt(0) === ".") {
                                //If have a base name, try to normalize against it,
                                //otherwise, assume it is a top-level require that will
                                //be relative to baseUrl in the end.
                                if (baseName) {
                                    name = name.split('/');
                                    lastIndex = name.length - 1;

                                    // Node .js allowance:
                                    if (config.nodeIdCompat && jsSuffixRegExp.test(name[lastIndex])) {
                                        name[lastIndex] = name[lastIndex].replace(jsSuffixRegExp, '');
                                    }

                                    //Lop off the last part of baseParts, so that . matches the
                                    //"directory" and not name of the baseName's module. For instance,
                                    //baseName of "one/two/three", maps to "one/two/three.js", but we
                                    //want the directory, "one/two" for this normalization.
                                    name = baseParts.slice(0, baseParts.length - 1).concat(name);

                                    //start trimDots
                                    for (i = 0; i < name.length; i += 1) {
                                        part = name[i];
                                        if (part === ".") {
                                            name.splice(i, 1);
                                            i -= 1;
                                        } else if (part === "..") {
                                            if (i === 1 && (name[2] === '..' || name[0] === '..')) {
                                                //End of the line. Keep at least one non-dot
                                                //path segment at the front so it can be mapped
                                                //correctly to disk. Otherwise, there is likely
                                                //no path mapping for a path starting with '..'.
                                                //This can still fail, but catches the most reasonable
                                                //uses of ..
                                                break;
                                            } else if (i > 0) {
                                                name.splice(i - 1, 2);
                                                i -= 2;
                                            }
                                        }
                                    }
                                    //end trimDots

                                    name = name.join("/");
                                } else if (name.indexOf('./') === 0) {
                                    // No baseName, so this is ID is resolved relative
                                    // to baseUrl, pull off the leading dot.
                                    name = name.substring(2);
                                }
                            }

                            //Apply map config if available.
                            if ((baseParts || starMap) && map) {
                                nameParts = name.split('/');

                                for (i = nameParts.length; i > 0; i -= 1) {
                                    nameSegment = nameParts.slice(0, i).join("/");

                                    if (baseParts) {
                                        //Find the longest baseName segment match in the config.
                                        //So, do joins on the biggest to smallest lengths of baseParts.
                                        for (j = baseParts.length; j > 0; j -= 1) {
                                            mapValue = map[baseParts.slice(0, j).join('/')];

                                            //baseName segment has  config, find if it has one for
                                            //this name.
                                            if (mapValue) {
                                                mapValue = mapValue[nameSegment];
                                                if (mapValue) {
                                                    //Match, update name to the new value.
                                                    foundMap = mapValue;
                                                    foundI = i;
                                                    break;
                                                }
                                            }
                                        }
                                    }

                                    if (foundMap) {
                                        break;
                                    }

                                    //Check for a star map match, but just hold on to it,
                                    //if there is a shorter segment match later in a matching
                                    //config, then favor over this star map.
                                    if (!foundStarMap && starMap && starMap[nameSegment]) {
                                        foundStarMap = starMap[nameSegment];
                                        starI = i;
                                    }
                                }

                                if (!foundMap && foundStarMap) {
                                    foundMap = foundStarMap;
                                    foundI = starI;
                                }

                                if (foundMap) {
                                    nameParts.splice(0, foundI, foundMap);
                                    name = nameParts.join('/');
                                }
                            }

                            return name;
                        }

                        function makeRequire(relName, forceSync) {
                            return function () {
                                //A version of a require function that passes a moduleName
                                //value for items that may need to
                                //look up paths relative to the moduleName
                                var args = aps.call(arguments, 0);

                                //If first arg is not require('string'), and there is only
                                //one arg, it is the array form without a callback. Insert
                                //a null so that the following concat is correct.
                                if (typeof args[0] !== 'string' && args.length === 1) {
                                    args.push(null);
                                }
                                return req.apply(undef, args.concat([relName, forceSync]));
                            };
                        }

                        function makeNormalize(relName) {
                            return function (name) {
                                return normalize(name, relName);
                            };
                        }

                        function makeLoad(depName) {
                            return function (value) {
                                defined[depName] = value;
                            };
                        }

                        function callDep(name) {
                            if (hasProp(waiting, name)) {
                                var args = waiting[name];
                                delete waiting[name];
                                defining[name] = true;
                                main.apply(undef, args);
                            }

                            if (!hasProp(defined, name) && !hasProp(defining, name)) {
                                throw new Error('No ' + name);
                            }
                            return defined[name];
                        }

                        //Turns a plugin!resource to [plugin, resource]
                        //with the plugin being undefined if the name
                        //did not have a plugin prefix.
                        function splitPrefix(name) {
                            var prefix,
                                index = name ? name.indexOf('!') : -1;
                            if (index > -1) {
                                prefix = name.substring(0, index);
                                name = name.substring(index + 1, name.length);
                            }
                            return [prefix, name];
                        }

                        /**
                         * Makes a name map, normalizing the name, and using a plugin
                         * for normalization if necessary. Grabs a ref to plugin
                         * too, as an optimization.
                         */
                        makeMap = function (name, relName) {
                            var plugin,
                                parts = splitPrefix(name),
                                prefix = parts[0];

                            name = parts[1];

                            if (prefix) {
                                prefix = normalize(prefix, relName);
                                plugin = callDep(prefix);
                            }

                            //Normalize according
                            if (prefix) {
                                if (plugin && plugin.normalize) {
                                    name = plugin.normalize(name, makeNormalize(relName));
                                } else {
                                    name = normalize(name, relName);
                                }
                            } else {
                                name = normalize(name, relName);
                                parts = splitPrefix(name);
                                prefix = parts[0];
                                name = parts[1];
                                if (prefix) {
                                    plugin = callDep(prefix);
                                }
                            }

                            //Using ridiculous property names for space reasons
                            return {
                                f: prefix ? prefix + '!' + name : name, //fullName
                                n: name,
                                pr: prefix,
                                p: plugin
                            };
                        };

                        function makeConfig(name) {
                            return function () {
                                return (config && config.config && config.config[name]) || {};
                            };
                        }

                        handlers = {
                            require: function (name) {
                                return makeRequire(name);
                            },
                            exports: function (name) {
                                var e = defined[name];
                                if (typeof e !== 'undefined') {
                                    return e;
                                } else {
                                    return (defined[name] = {});
                                }
                            },
                            module: function (name) {
                                return {
                                    id: name,
                                    uri: '',
                                    exports: defined[name],
                                    config: makeConfig(name)
                                };
                            }
                        };

                        main = function (name, deps, callback, relName) {
                            var cjsModule, depName, ret, map, i,
                                args = [],
                                callbackType = typeof callback,
                                usingExports;

                            //Use name if no relName
                            relName = relName || name;

                            //Call the callback to define the module, if necessary.
                            if (callbackType === 'undefined' || callbackType === 'function') {
                                //Pull out the defined dependencies and pass the ordered
                                //values to the callback.
                                //Default to [require, exports, module] if no deps
                                deps = !deps.length && callback.length ? ['require', 'exports', 'module'] : deps;
                                for (i = 0; i < deps.length; i += 1) {
                                    map = makeMap(deps[i], relName);
                                    depName = map.f;

                                    //Fast path CommonJS standard dependencies.
                                    if (depName === "require") {
                                        args[i] = handlers.require(name);
                                    } else if (depName === "exports") {
                                        //CommonJS module spec 1.1
                                        args[i] = handlers.exports(name);
                                        usingExports = true;
                                    } else if (depName === "module") {
                                        //CommonJS module spec 1.1
                                        cjsModule = args[i] = handlers.module(name);
                                    } else if (hasProp(defined, depName) ||
                                        hasProp(waiting, depName) ||
                                        hasProp(defining, depName)) {
                                        args[i] = callDep(depName);
                                    } else if (map.p) {
                                        map.p.load(map.n, makeRequire(relName, true), makeLoad(depName), {});
                                        args[i] = defined[depName];
                                    } else {
                                        throw new Error(name + ' missing ' + depName);
                                    }
                                }

                                ret = callback ? callback.apply(defined[name], args) : undefined;

                                if (name) {
                                    //If setting exports via "module" is in play,
                                    //favor that over return value and exports. After that,
                                    //favor a non-undefined return value over exports use.
                                    if (cjsModule && cjsModule.exports !== undef &&
                                        cjsModule.exports !== defined[name]) {
                                        defined[name] = cjsModule.exports;
                                    } else if (ret !== undef || !usingExports) {
                                        //Use the return value from the function.
                                        defined[name] = ret;
                                    }
                                }
                            } else if (name) {
                                //May just be an object definition for the module. Only
                                //worry about defining if have a module name.
                                defined[name] = callback;
                            }
                        };

                        requirejs = require = req = function (deps, callback, relName, forceSync, alt) {
                            if (typeof deps === "string") {
                                if (handlers[deps]) {
                                    //callback in this case is really relName
                                    return handlers[deps](callback);
                                }
                                //Just return the module wanted. In this scenario, the
                                //deps arg is the module name, and second arg (if passed)
                                //is just the relName.
                                //Normalize module name, if it contains . or ..
                                return callDep(makeMap(deps, callback).f);
                            } else if (!deps.splice) {
                                //deps is a config object, not an array.
                                config = deps;
                                if (config.deps) {
                                    req(config.deps, config.callback);
                                }
                                if (!callback) {
                                    return;
                                }

                                if (callback.splice) {
                                    //callback is an array, which means it is a dependency list.
                                    //Adjust args if there are dependencies
                                    deps = callback;
                                    callback = relName;
                                    relName = null;
                                } else {
                                    deps = undef;
                                }
                            }

                            //Support require(['a'])
                            callback = callback || function () {
                                };

                            //If relName is a function, it is an errback handler,
                            //so remove it.
                            if (typeof relName === 'function') {
                                relName = forceSync;
                                forceSync = alt;
                            }

                            //Simulate async callback;
                            if (forceSync) {
                                main(undef, deps, callback, relName);
                            } else {
                                //Using a non-zero value because of concern for what old browsers
                                //do, and latest browsers "upgrade" to 4 if lower value is used:
                                //http://www.whatwg.org/specs/web-apps/current-work/multipage/timers.html#dom-windowtimers-settimeout:
                                //If want a value immediately, use require('id') instead -- something
                                //that works in almond on the global level, but not guaranteed and
                                //unlikely to work in other AMD implementations.
                                setTimeout(function () {
                                    main(undef, deps, callback, relName);
                                }, 4);
                            }

                            return req;
                        };

                        /**
                         * Just drops the config on the floor, but returns req in case
                         * the config return value is used.
                         */
                        req.config = function (cfg) {
                            return req(cfg);
                        };

                        /**
                         * Expose module registry for debugging and tooling
                         */
                        requirejs._defined = defined;

                        define = function (name, deps, callback) {
                            if (typeof name !== 'string') {
                                throw new Error('See almond README: incorrect module build, no module name');
                            }

                            //This module may not have dependencies
                            if (!deps.splice) {
                                //deps is not an array, so probably means
                                //an object literal or factory function for
                                //the value. Adjust args.
                                callback = deps;
                                deps = [];
                            }

                            if (!hasProp(defined, name) && !hasProp(waiting, name)) {
                                waiting[name] = [name, deps, callback];
                            }
                        };

                        define.amd = {
                            jQuery: true
                        };
                    }());

                    DALI.requirejs = requirejs;
                    DALI.require = require;
                    DALI.define = define;
                }
            }());
            DALI.define("almond", function () {
            });

            /* global jQuery:false, $:false */
            DALI.define('jquery', [], function () {
                var _$ = jQuery || $;

                if (_$ == null && console && console.error) {
                    console.error(
                        'Dateline: An instance of jQuery or a jQuery-compatible library was not ' +
                        'found. Make sure that you are including jQuery before Dateline on your ' +
                        'web page.'
                    );
                }

                return _$;
            });

            DALI.define('dateline/utils', ['jquery'], function ($) {
                var Utils = {};

                function getMethods(theClass) {
                    var proto = theClass.prototype;

                    var methods = [];

                    for (var methodName in proto) {
                        var m = proto[methodName];

                        if (typeof m !== 'function') {
                            continue;
                        }

                        if (methodName === 'constructor') {
                            continue;
                        }

                        methods.push(methodName);
                    }

                    return methods;
                }

                Utils.Extend = function (ChildClass, SuperClass) {
                    var __hasProp = {}.hasOwnProperty;

                    function BaseConstructor() {
                        this.constructor = ChildClass;
                    }

                    for (var key in SuperClass) {
                        if (__hasProp.call(SuperClass, key)) {
                            ChildClass[key] = SuperClass[key];
                        }
                    }

                    BaseConstructor.prototype = SuperClass.prototype;
                    ChildClass.prototype = new BaseConstructor();
                    ChildClass.__super__ = SuperClass.prototype;

                    return ChildClass;
                };

                var Observable = function () {
                    this.listeners = {};
                };

                Observable.prototype.on = function (event, callback) {
                    this.listeners = this.listeners || {};

                    if (event in this.listeners) {
                        this.listeners[event].push(callback);
                    } else {
                        this.listeners[event] = [callback];
                    }
                };

                Observable.prototype.trigger = function (event) {
                    var slice = Array.prototype.slice;
                    var params = slice.call(arguments, 1);

                    this.listeners = this.listeners || {};

                    // Params should always come in as an array
                    if (params == null) {
                        params = [];
                    }

                    // If there are no arguments to the event, use a temporary object
                    if (params.length === 0) {
                        params.push({});
                    }

                    // Set the `_type` of the first object to the event
                    params[0]._type = event;

                    if (event in this.listeners) {
                        this.invoke(this.listeners[event], slice.call(arguments, 1));
                    }

                    if ('*' in this.listeners) {
                        this.invoke(this.listeners['*'], arguments);
                    }
                };

                Observable.prototype.invoke = function (listeners, params) {
                    for (var i = 0, len = listeners.length; i < len; i++) {
                        listeners[i].apply(this, params);
                    }
                };

                Utils.Observable = Observable;

                return Utils;

            });

            DALI.define('dateline/defaults', ['jquery', 'require', './utils'], function ($, require, Utils) {

            });

            DALI.define('dateline/core',['jquery', './utils'/*, './options', './keys'*/], function ($, Utils/*, Options, KEYS*/) {

                var Dateline = function($element, options) {
                    if ($element.data('dateline') != null) {
                        $element.data('dateline').destroy();
                    }

                    var self = this;
                    self.$element = $element;

                    if (self.$element.prop('tagName') !== 'INPUT') {
                        throw new Error('Dateline element must be an input');
                    }

                    self.initial_val = $element.val();

                    self.id = self._generateId($element);

                    self.options = options || {};

                    Dateline.__super__.constructor.call(self);

                    var $container = self.render();
                    self._placeContainer($container);

                    $element.data('dateline', self);

                };

                Utils.Extend(Dateline, Utils.Observable);

                Dateline.prototype._generateId = function ($element) {
                    var id = '';

                    if ($element.attr('id') != null) {
                        id = $element.attr('id');
                    } else {
                        throw new Error('Dateline element must have id');
                    }

                    id = id.replace(/(:|\.|\[|\]|,)/g, '');
                    id = 'dateline-' + id;

                    return id;
                };

                Dateline.prototype._placeContainer = function ($container) {
                    $container.insertAfter(this.$element);
                    this.$element.hide();
                    $container.css('left', '0');
                    var width = this.options.width || this.$element.width;

                    if (width != null) {
                        $container.css('width', width);
                    }
                };

                Dateline.prototype.render = function () {

                    var self = this;

                    var id = self.$element.attr('id');

                    var $container = $(
                        '<div id="dateline-' + id + '" class="dateline dateline-container ' + (self.options.mainDivClass || '') + '"></div>'
                    );

                    var disabled = self.$element.prop('disabled');

                    self.$monthDiv = $('<div id="dateline-monthDiv-' + id + '" class="' + self.options.monthDivClass + '">');
                    self.$dayDiv = $('<div id="dateline-dayDiv-' + id + '" class="' + self.options.dayDivClass + '">');
                    self.$yearDiv = $('<div id="dateline-yearDiv-' + id + '" class="' + self.options.yearDivClass + '">');

                    self.$month = $(
                        '<select id="dateline-month-' + id + '" class="' + self.options.monthClass + ' dateline-month" placeholder="Month" ' + (disabled ? 'disabled' : '') + '>' +
                        '    <option class="placeholder" value="">select month</option>' +
                        '    <option value="01">January</option>' +
                        '    <option value="02">February</option>' +
                        '    <option value="03">March</option>' +
                        '    <option value="04">April</option>' +
                        '    <option value="05">May</option>' +
                        '    <option value="06">June</option>' +
                        '    <option value="07">July</option>' +
                        '    <option value="08">August</option>' +
                        '    <option value="09">September</option>' +
                        '    <option value="10">October</option>' +
                        '    <option value="11">November</option>' +
                        '    <option value="12">December</option>' +
                        '</select>'
                    );
                    self.$day = $('<input id="dateline-day-' + id + '" class="' + self.options.dayClass + ' dateline-day" placeholder="Day" ' + (disabled ? 'disabled' : '') + ' autocomplete="off">');
                    self.$year = $('<input id="dateline-year-' + id + '" class="' + self.options.yearClass + ' dateline-year" placeholder="Year" ' + (disabled ? 'disabled' : '') + ' autocomplete="off">');

                    self.$monthDiv.append(self.$month);
                    self.$dayDiv.append(self.$day);
                    self.$yearDiv.append(self.$year);

                    $container.append(self.$monthDiv);
                    $container.append(self.$dayDiv);
                    $container.append(self.$yearDiv);

                    if (self.initial_val) {
                        self.setInputValues(self.initial_val);
                    }

                    self.$month.change(function() {
                        self.set_val();
                    });

                    self.$day.on('keyup', function() {
                        self.set_val();
                    });

                    self.$year.on('keyup', function() {
                        self.set_val();
                    });

                    self.$element.change(function() {
                        self.elementChange()
                    });

                    return $container;
                };

                Dateline.prototype.set_val = function () {
                    var self = this,
                        val = self.$day.val() + '-' + self.$month.val() + '-' + self.$year.val();

                    val = val === '--' ? '' : val;
                    self.$element.val(val);
                    self.$element.change();
                };

                Dateline.prototype.elementChange = function() {
                    var self = this;
                    self.setInputValues(self.$element.val())
                };

                Dateline.prototype.setInputValues = function(dateString) {

                    var self = this;
                    var year_pos, month_pos, day_pos;
                    if (self.options.locale && self.options.locale.format === 'YYYY-MM-DD') {
                        year_pos = 0;
                        month_pos = 1;
                        day_pos = 2;
                    } else {
                        year_pos = 2;
                        month_pos = 1;
                        day_pos = 0;
                    }
                    var initial_day = dateString.split('-')[day_pos];
                    var initial_month = dateString.split('-')[month_pos];
                    var initial_year = dateString.split('-')[year_pos];
                    self.$day.val(initial_day);
                    self.$month.val(initial_month);
                    self.$year.val(initial_year);
                };

                Dateline.prototype.val = function() {
                    return this.$element.val();
                };

                Dateline.prototype._bindAdapters = function () {
                    this.dataAdapter.bind(this, this.$container);
                    this.selection.bind(this, this.$container);

                    this.dropdown.bind(this, this.$container);
                    this.results.bind(this, this.$container);
                };

                return Dateline;

            });

            DALI.define('jquery-mousewheel', ['jquery'], function ($) {
                // Used to shim jQuery.mousewheel for non-full builds.
                return $;
            });

            DALI.define('jquery.dateline', ['jquery', 'jquery-mousewheel', './dateline/core', './dateline/defaults'], function ($, _, Dateline, Defaults) {
                if ($.fn.dateline == null) {
                    // All methods that should return the element
                    var thisMethods = ['open', 'close', 'destroy'];

                    $.fn.dateline = function (options) {
                        options = options || {};

                        if (typeof options === 'object') {
                            this.each(function () {
                                var instanceOptions = $.extend(true, {}, options);

                                var instance = new Dateline($(this), instanceOptions);
                            });

                            return this;
                        } else if (typeof options === 'string') {
                            var ret;
                            var args = Array.prototype.slice.call(arguments, 1);

                            this.each(function () {
                                var instance = $(this).data('dateline');

                                if (instance == null && window.console && console.error) {
                                    console.error(
                                        'The dateline(\'' + options + '\') method was called on an ' +
                                        'element that is not using Select2.'
                                    );
                                }

                                ret = instance[options].apply(instance, args);
                            });

                            // Check if we should be returning `this`
                            if ($.inArray(options, thisMethods) > -1) {
                                return this;
                            }

                            return ret;
                        } else {
                            throw new Error('Invalid arguments for Dateline: ' + options);
                        }
                    };
                }

                if ($.fn.dateline.defaults == null) {
                    $.fn.dateline.defaults = Defaults;
                }

                return Dateline;
            });


            return {
                define: DALI.define,
                require: DALI.require
            };
        }());
    // Autoload the jQuery bindings
    // We know that all of the modules exist above this, so we're safe
    var dateline = DALI.require('jquery.dateline');

    // Hold the AMD module references on the jQuery function that was just loaded
    // This allows Dateline to use the internal loader outside of this file, such
    // as in the language files.
    jQuery.fn.dateline.amd = DALI;

    // Return the Dateline instance for anyone who is importing it.
    return dateline;
}));