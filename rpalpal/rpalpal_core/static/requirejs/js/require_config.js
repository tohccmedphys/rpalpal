
var google_api_key = SiteConfig.google_api_key;

require.config({
    urlArgs: (function () {
        if (SiteConfig.debug === 'True') {
            return 'v=' + Math.random();
        }
        return 'v=' + SiteConfig.version;
    }()),
    baseUrl: SiteConfig.baseStaticUrl,
    paths: {

        adminlte: SiteConfig.baseStaticUrl + 'adminlte/js/app' + SiteConfig.min,
        adminlte_config: SiteConfig.baseStaticUrl + 'adminlte/js/adminlte.config',
        autosize: SiteConfig.baseStaticUrl + 'autosize/js/autosize' + SiteConfig.min,
        bootstrap: SiteConfig.baseStaticUrl + 'bootstrap/js/bootstrap.bundle' + SiteConfig.min,
        d3: SiteConfig.baseStaticUrl + 'd3/js/d3' + SiteConfig.min,
        datepicker: SiteConfig.baseStaticUrl + 'datepicker/js/bootstrap-datepicker',
        daterangepicker: SiteConfig.baseStaticUrl + 'daterangepicker/js/daterangepicker',
        dateline: SiteConfig.baseStaticUrl + 'dateline/js/dateline',
        flatpickr: SiteConfig.baseStaticUrl + 'flatpickr/js/flatpickr',
        inputmask: SiteConfig.baseStaticUrl + 'inputmask/js/jquery.inputmask.bundle',
        json2: SiteConfig.baseStaticUrl + 'json2/js/json2',
        jquery: SiteConfig.baseStaticUrl + 'jquery/js/jquery' + SiteConfig.min,
        knockout: SiteConfig.baseStaticUrl + 'knockoutjs/js/knockout',
        lodash: SiteConfig.baseStaticUrl + 'lodash/js/lodash',
        moment: SiteConfig.baseStaticUrl + 'moment/js/moment',
        multiselect: SiteConfig.baseStaticUrl + 'multiselect/js/bootstrap.multiselect',
        popper: SiteConfig.baseStaticUrl + 'popper/js/popper',
        select2: SiteConfig.baseStaticUrl + 'select2/js/select2.full' + SiteConfig.min,
        slimscroll: SiteConfig.baseStaticUrl + 'slimscroll/js/jquery.slimscroll.min',

        datatables: SiteConfig.baseStaticUrl + 'listable/js/jquery.dataTables.min',
        'datatables.bootstrap': SiteConfig.baseStaticUrl + 'listable/js/jquery.dataTables.bootstrap',
        'datatables.columnFilter': SiteConfig.baseStaticUrl + 'listable/js/jquery.dataTables.columnFilter',
        'datatables.searchPlugins': SiteConfig.baseStaticUrl + 'listable/js/jquery.dataTables.searchPlugins',
        'datatables.sort': SiteConfig.baseStaticUrl + 'listable/js/jquery.dataTables.sort',
        toggler: SiteConfig.baseStaticUrl + 'listable/js/jquery.toggler',
        listable: SiteConfig.baseStaticUrl + 'listable/js/listable',

        async: SiteConfig.baseStaticUrl + 'requirejs/js/plugins/async',

        base: SiteConfig.baseStaticUrl + 'rpalpal_core/js/base',
        patient_form: SiteConfig.baseStaticUrl + 'rpal/js/patient_form',
        patient_details: SiteConfig.baseStaticUrl + 'rpal/js/patient_details',
        patient_details_vm: SiteConfig.baseStaticUrl + 'rpal/js/patient_details_vm',
        rpal_models: SiteConfig.baseStaticUrl + 'rpal/js/rpal_models',
        referral_vm: SiteConfig.baseStaticUrl + 'rpal/js/referral_vm',
        diagnosis_vm: SiteConfig.baseStaticUrl + 'rpal/js/diagnosis_vm',
        visit_vm: SiteConfig.baseStaticUrl + 'rpal/js/visit_vm',
        // patient_careplan_vm: SiteConfig.baseStaticUrl + 'rpal/js/patient_careplan_vm',
        treatment_course_vm: SiteConfig.baseStaticUrl + 'rpal/js/treatment_course_vm',
        // followup_vm: SiteConfig.baseStaticUrl + 'rpal/js/followup_vm',
        patient_vm: SiteConfig.baseStaticUrl + 'rpal/js/patient_vm',
        physician_vm: SiteConfig.baseStaticUrl + 'rpal/js/physician_vm',
        stats: SiteConfig.baseStaticUrl + 'rpal/js/stats',

        careplan_form: SiteConfig.baseStaticUrl + 'rpal/js/careplan_form',

    },
    shim: {
        adminlte: {
            deps: ['jquery', 'bootstrap', 'slimscroll', 'adminlte_config']
        },
        bootstrap: {
            deps: ['jquery', 'popper']
        },
        datatables: {
            deps: ['jquery'],
            exports: 'dataTable'
        },
        'datatables.bootstrap': {
            deps: ['datatables']
        },
        'datatables.columnFilter': {
            deps: ['datatables']
        },
        'datatables.searchPlugins': {
            deps: ['datatables']
        },
        'datatables.sort': {
            deps: ['datatables']
        },
        datepicker: {
            deps: ['jquery', 'bootstrap']
        },
        daterangepicker: {
            exports: 'DateRangePicker',
            deps: ['jquery', 'moment']
        },
        jquery: {
            exports: '$'
        },
        knockout: {
            deps: ['jquery'],
            exports: 'ko'
        },
        listable: {
            deps: ['jquery', 'moment', 'datatables', 'datatables.columnFilter', 'datatables.searchPlugins', 'datatables.sort', 'datatables.bootstrap', 'multiselect', 'datepicker', 'daterangepicker', 'popper', 'toggler']
        },
        lodash: {
            deps: ['jquery'],
            exports: '_'
        },
        multiselect: {
            deps: ['bootstrap', 'toggler']
        },
        slimscroll: {
            deps: ['jquery']
        },
        select2: {
            deps: ['jquery']
        },
        toggler: {
            deps: ['jquery']
        },
    }
});

require(['adminlte']);


