
import re

from django import template

from rpal import models as rp_models

register = template.Library()


@register.filter(name='addcss')
def addcss(field, css):
    return field.as_widget(attrs={"class": css})


@register.filter(name='addplaceholder')
def addplaceholder(field, placeholder=None):
    if placeholder is None:
        if hasattr(field, 'verbose_name'):
            return field.as_widget(attrs={'placeholder': field.verbose_name})
        v_name = field.name.replace('_', ' ').title()
        return field.as_widget(attrs={'placeholder': v_name})
    else:
        return field.as_widget(attrs={'placeholder': placeholder})


@register.filter(name='addcss_addplaceholder')
def addcss_addplaceholder(field, css):
    if hasattr(field, 'verbose_name'):
        return field.as_widget(attrs={'placeholder': field.verbose_name, 'class': css})
    v_name = re.sub(r'\d', '', field.name.replace('_', ' ').title())
    return field.as_widget(attrs={'placeholder': v_name, 'class': css})


@register.filter(name='hideinput')
def hideinput(field):
    return field.as_widget(attrs={'type': 'hidden'})


@register.filter(name='disableinput')
def disableinput(field):
    return field.as_widget(attrs={'disabled': 'disabled'})


@register.filter
def lookup(d, key):
    return d[key]


@register.filter(name='add_data_bind')
def add_data_bind(field, data_bind):
    return field.as_widget(attrs={'data-bind': data_bind})


@register.filter
def addstr(arg1, arg2):
    return str(arg1) + str(arg2)


@register.filter
def get_model(models, model_name):
    for m in models:
        if m['object_name'] == model_name:
            m['help_text'] = getattr(getattr(rp_models, model_name), 'help_text', False)
            return m
    return None
