Django==3.0.10
django-braces==1.14.0
django-crispy-forms==1.9.2
django-cities==0.6
python-dateutil==2.8.1
-e git+https://github.com/randlet/django-listable.git@1307027166e7d2e86b6a469db5de328c8686d376#egg=django_listable

django-debug-toolbar==3.1.1

CherryPy
django-mssql-backend

#pywin32
#pyldap